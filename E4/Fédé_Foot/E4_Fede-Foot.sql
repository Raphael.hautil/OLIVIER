-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `AgentFédé`;
CREATE TABLE `AgentFédé` (
  `idL` varchar(5) NOT NULL,
  `hierarchie` varchar(30) NOT NULL,
  PRIMARY KEY (`idL`),
  CONSTRAINT `AgentFédé_ibfk_1` FOREIGN KEY (`idL`) REFERENCES `Licencié` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `AgentFédé` (`idL`, `hierarchie`) VALUES
('LUCFR',	'Vice-Président'),
('ZOUAB',	'Présidente');

DROP TABLE IF EXISTS `Club`;
CREATE TABLE `Club` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) NOT NULL,
  `dateCreation` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Club` (`id`, `libelle`, `dateCreation`) VALUES
(1,	'AS Hautil',	'2018-11-28'),
(2,	'Olympique de Marseille',	'1899-08-31'),
(3,	'Paris Saint Germain',	'1970-08-12');

DROP TABLE IF EXISTS `Competition`;
CREATE TABLE `Competition` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Competition` (`id`, `libelle`) VALUES
(1,	'Ligue des Champions'),
(2,	'Europa League'),
(3,	'Ligue 1'),
(4,	'Ligue 2'),
(5,	'Coupe de la Ligue'),
(6,	'Coupe de France'),
(7,	'Trophée des Champions'),
(101,	'Coupe du Val d\'Oise');

DROP TABLE IF EXISTS `Demande`;
CREATE TABLE `Demande` (
  `id` int(11) NOT NULL,
  `idC` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `categorie` varchar(50) NOT NULL,
  `dateCreation` date NOT NULL,
  `status` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idC` (`idC`),
  CONSTRAINT `Demande_ibfk_1` FOREIGN KEY (`idC`) REFERENCES `Club` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Demande` (`id`, `idC`, `nom`, `categorie`, `dateCreation`, `status`) VALUES
(1,	1,	'Test',	'U21',	'2019-03-15',	'Accepté'),
(2,	3,	'Test',	'U21',	'2019-03-15',	'Refusé'),
(3,	1,	'czegic',	'vo',	'2019-03-15',	'Accepté'),
(4,	1,	'OUI',	'U15',	'2019-03-20',	'Accepté'),
(5,	1,	'non',	'U19',	'2019-03-20',	'Refusé'),
(6,	3,	'PSG',	'U13',	'2019-03-20',	'Refusé'),
(7,	3,	'PSG 3',	'Débutant',	'2019-03-20',	'Accepté'),
(8,	2,	'OM',	'Benjamin',	'2019-03-20',	'Refusé'),
(9,	2,	'OM 2',	'Séniors',	'2019-03-20',	'Accepté');

DROP TABLE IF EXISTS `DemandeTraitee`;
CREATE TABLE `DemandeTraitee` (
  `idD` int(11) NOT NULL,
  `idA` varchar(5) NOT NULL,
  `dateTraitement` date NOT NULL,
  `status` varchar(7) NOT NULL,
  PRIMARY KEY (`idD`),
  KEY `idP` (`idA`),
  CONSTRAINT `DemandeTraitee_ibfk_1` FOREIGN KEY (`idD`) REFERENCES `Demande` (`id`),
  CONSTRAINT `DemandeTraitee_ibfk_2` FOREIGN KEY (`idA`) REFERENCES `AgentFédé` (`idL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `DemandeTraitee` (`idD`, `idA`, `dateTraitement`, `status`) VALUES
(1,	'ZOUAB',	'2019-03-20',	'Accepté'),
(2,	'ZOUAB',	'2019-03-20',	'Refusé'),
(3,	'ZOUAB',	'2019-03-20',	'Accepté'),
(4,	'ZOUAB',	'2019-03-20',	'Accepté'),
(5,	'ZOUAB',	'2019-03-20',	'Refusé'),
(6,	'ZOUAB',	'2019-03-20',	'Refusé'),
(7,	'ZOUAB',	'2019-03-20',	'Accepté'),
(8,	'ZOUAB',	'2019-03-20',	'Refusé'),
(9,	'ZOUAB',	'2019-03-20',	'Accepté');

DELIMITER ;;

CREATE TRIGGER `updateDemande` AFTER INSERT ON `DemandeTraitee` FOR EACH ROW
BEGIN
UPDATE Demande SET Demande.status = NEW.status WHERE Demande.id = NEW.idD;
END;;

DELIMITER ;

DROP TABLE IF EXISTS `Entraineur`;
CREATE TABLE `Entraineur` (
  `idL` varchar(5) NOT NULL,
  `equipe` int(11) NOT NULL,
  PRIMARY KEY (`idL`),
  KEY `equipe` (`equipe`),
  CONSTRAINT `Entraineur_ibfk_2` FOREIGN KEY (`equipe`) REFERENCES `Equipe` (`id`),
  CONSTRAINT `Entraineur_ibfk_3` FOREIGN KEY (`idL`) REFERENCES `Licencié` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Entraineur` (`idL`, `equipe`) VALUES
('ANDMA',	1),
('RUDGA',	2),
('THOTU',	3);

DROP TABLE IF EXISTS `Equipe`;
CREATE TABLE `Equipe` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) NOT NULL,
  `categorie` varchar(10) NOT NULL,
  `dateCreation` date NOT NULL,
  `club` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `club` (`club`),
  CONSTRAINT `Equipe_ibfk_1` FOREIGN KEY (`club`) REFERENCES `Club` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Equipe` (`id`, `libelle`, `categorie`, `dateCreation`, `club`) VALUES
(1,	'FC Hautil',	'U21',	'2018-11-28',	1),
(2,	'Olympique de Marseille',	'Séniors',	'1899-08-31',	2),
(3,	'Paris Saint Germain',	'Séniors',	'1970-08-12',	3),
(4,	'OM 2',	'Séniors',	'2019-03-20',	2);

DROP TABLE IF EXISTS `Joueur`;
CREATE TABLE `Joueur` (
  `idL` varchar(5) NOT NULL,
  `categorie` varchar(10) NOT NULL,
  `estPro` tinyint(4) NOT NULL,
  `poste` varchar(20) NOT NULL,
  `equipe` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  PRIMARY KEY (`idL`),
  KEY `equipe` (`equipe`),
  CONSTRAINT `Joueur_ibfk_2` FOREIGN KEY (`equipe`) REFERENCES `Equipe` (`id`),
  CONSTRAINT `Joueur_ibfk_3` FOREIGN KEY (`idL`) REFERENCES `Licencié` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Joueur` (`idL`, `categorie`, `estPro`, `poste`, `equipe`, `numero`) VALUES
('ADIRA',	'Séniors',	1,	'Défenseur',	2,	23),
('ADRLE',	'U21',	0,	'Défenseur',	1,	83),
('ALEMO',	'U21',	0,	'Défenseur',	1,	69),
('ANGDI',	'Séniors',	1,	'Milieu',	3,	11),
('BASGR',	'U21',	0,	'Défenseur',	1,	5),
('BOUSA',	'Séniors',	1,	'Défenseur',	2,	17),
('CLINJ',	'Séniors',	1,	'Attaquant',	2,	14),
('DIMPA',	'Séniors',	1,	'Attaquant',	2,	10),
('EDICA',	'Séniors',	1,	'Attaquant',	3,	9),
('FLORE',	'U21',	0,	'Attaquant',	1,	9),
('FLOTH',	'Séniors',	1,	'Attaquant',	2,	26),
('GIABU',	'Séniors',	1,	'Gardien',	3,	1),
('HIRSA',	'Séniors',	1,	'Défenseur',	2,	2),
('JUABE',	'Séniors',	1,	'Défenseur',	3,	14),
('KILBO',	'U21',	0,	'Attaquant',	1,	8),
('KYLMB',	'Séniors',	1,	'Attaquant',	3,	7),
('LUCOC',	'Séniors',	1,	'Milieu',	2,	5),
('MARAO',	'Séniors',	1,	'Milieu',	3,	5),
('MARVE',	'Séniors',	1,	'Milieu',	3,	6),
('MATCA',	'U21',	0,	'Attaquant',	1,	10),
('MAXLO',	'Séniors',	1,	'Milieu',	2,	27),
('MOHAL',	'U21',	0,	'Milieu',	1,	95),
('MORSA',	'Séniors',	1,	'Milieu',	2,	8),
('NEYDA',	'Séniors',	1,	'Attaquant',	3,	10),
('PREKI',	'Séniors',	1,	'Défenseur',	3,	3),
('RAPOL',	'U21',	0,	'Milieu',	1,	12),
('ROLVI',	'Séniors',	1,	'Défenseur',	2,	6),
('STEMA',	'Séniors',	1,	'Gardien',	2,	30),
('THISI',	'Séniors',	1,	'Défenseur',	3,	2),
('THOFE',	'U21',	0,	'Milieu',	1,	99),
('THOHE',	'U21',	0,	'Gardien',	1,	46),
('THOME',	'Séniors',	1,	'Défenseur',	3,	12),
('VALHE',	'U21',	0,	'Défenseur',	1,	23);

DROP TABLE IF EXISTS `LgePalmares`;
CREATE TABLE `LgePalmares` (
  `id` int(11) NOT NULL,
  `idClub` int(11) NOT NULL,
  `idCompetition` int(11) NOT NULL,
  `nombre` int(11) NOT NULL,
  PRIMARY KEY (`id`,`idClub`),
  KEY `idClub` (`idClub`),
  KEY `idCompetition` (`idCompetition`),
  CONSTRAINT `LgePalmares_ibfk_1` FOREIGN KEY (`idClub`) REFERENCES `Club` (`id`),
  CONSTRAINT `LgePalmares_ibfk_2` FOREIGN KEY (`idCompetition`) REFERENCES `Competition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `LgePalmares` (`id`, `idClub`, `idCompetition`, `nombre`) VALUES
(1,	1,	101,	1),
(1,	2,	1,	1),
(1,	3,	3,	7),
(2,	2,	3,	11),
(2,	3,	4,	1),
(3,	2,	4,	1),
(3,	3,	7,	8),
(4,	2,	6,	10),
(4,	3,	5,	8),
(5,	2,	5,	3),
(5,	3,	6,	12),
(6,	2,	7,	3);

DROP TABLE IF EXISTS `Licencié`;
CREATE TABLE `Licencié` (
  `id` varchar(5) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `nom` varchar(70) NOT NULL,
  `dateNaissance` date NOT NULL,
  `dateInscription` date NOT NULL,
  `type` varchar(20) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Licencié` (`id`, `prenom`, `nom`, `dateNaissance`, `dateInscription`, `type`, `password`) VALUES
('ADIRA',	'Adil',	'Rami',	'1985-12-27',	'1992-08-05',	'Joueur',	'adira'),
('ADRLE',	'Adrien',	'Le Mignon',	'1999-03-10',	'2004-10-01',	'Joueur',	'adrle'),
('ALEMO',	'Alexandre',	'Mouzon',	'1999-04-30',	'2007-11-11',	'Joueur',	'alemo'),
('ANDMA',	'Andy',	'Martin',	'1999-11-18',	'2015-11-03',	'Entraîneur',	'andma'),
('ANGDI',	'Ángel',	'Di María',	'1988-02-14',	'1995-01-23',	'Joueur',	'angdi'),
('AURLA',	'Aurélien',	'Larivierre',	'1999-11-05',	'2017-09-05',	'Président de Club',	'aurla'),
('BASGR',	'Bastien',	'Gregori',	'1997-06-16',	'2003-05-11',	'Joueur',	'basgr'),
('BOUSA',	'Bouna',	'Sarr',	'1992-01-31',	'1996-12-29',	'Joueur',	'bousa'),
('CLINJ',	'Clinton',	'Njie',	'1993-08-15',	'1999-05-30',	'Joueur',	'clinj'),
('DIMPA',	'Dimitri',	'Payet',	'1987-03-29',	'1993-09-23',	'Joueur',	'dimpa'),
('EDICA',	'Edinson',	'Cavani',	'1987-02-14',	'1994-12-18',	'Joueur',	'edica'),
('FLORE',	'Florent',	'Regnier',	'1999-05-23',	'2004-10-01',	'Joueur',	'flore'),
('FLOTH',	'Florian',	'Thauvin',	'1993-01-26',	'1999-09-12',	'Joueur',	'floth'),
('FRAMC',	'Frank',	'McCourt',	'1953-08-14',	'1972-10-13',	'Président de Club',	'framc'),
('GIABU',	'Gianluigi',	'Buffon',	'1978-01-28',	'1984-06-21',	'Joueur',	'giabu'),
('HIRSA',	'Hiroki',	'Sakai',	'1990-04-12',	'2000-04-12',	'Joueur',	'hirsa'),
('JUABE',	'Juan',	'Bernat',	'1993-03-01',	'2003-05-19',	'Joueur',	'juabe'),
('KILBO',	'Kiliann',	'Boimard',	'2000-03-15',	'2005-09-12',	'Joueur',	'kilbo'),
('KYLMB',	'Kylian',	'Mbappé',	'1998-12-20',	'2004-03-26',	'Joueur',	'neymar'),
('LUCFR',	'Luc',	'Frébourg',	'1976-05-22',	'2017-09-23',	'Agent de Fédération',	'lucfr'),
('LUCOC',	'Lucas',	'Ocampos',	'1994-07-11',	'2000-11-16',	'Joueur',	'lucoc'),
('MARAO',	'Marquinhos',	'Aoás Corrêa',	'1994-05-14',	'2002-06-21',	'Joueur',	'marao'),
('MARVE',	'Marco',	'Verratti',	'1992-11-05',	'2000-09-29',	'Joueur',	'marve'),
('MATCA',	'Matis',	'Cataldi',	'1999-05-12',	'2008-12-19',	'Joueur',	'matca'),
('MAXLO',	'Maxime',	'Lopez',	'1997-12-04',	'2002-10-02',	'Joueur',	'maxlo'),
('MOHAL',	'Mohamed-Ali',	'Aljane',	'1997-11-26',	'2001-12-11',	'Joueur',	'bite'),
('MORSA',	'Morgan',	'Sanson',	'1994-08-18',	'2002-01-22',	'Joueur',	'morsa'),
('NASAL',	'Nasser',	'Al-Khelaïfi',	'1973-11-12',	'2011-11-04',	'Président de Club',	'nasal'),
('NEYDA',	'Neymar',	'da Silva Santos',	'1992-02-05',	'1998-10-29',	'Joueur',	'neyda'),
('PREKI',	'Presnel',	'Kimpembe',	'1995-08-15',	'2001-04-27',	'Joueur',	'preki'),
('RAPOL',	'Raphaël',	'Olivier',	'1999-09-12',	'2004-09-03',	'Joueur',	'rapol'),
('ROLVI',	'Rolando',	'Victor Finn Hogan',	'1985-08-31',	'1997-03-09',	'Joueur',	'rolvi'),
('RUDGA',	'Rudi',	'Garcia',	'1964-02-20',	'1977-03-16',	'Entraîneur',	'rudga'),
('STEMA',	'Steve',	'Mandanda',	'1985-03-28',	'1992-12-12',	'Joueur',	'stema'),
('THISI',	'Thiago',	'Silva',	'1984-09-22',	'1990-12-31',	'Joueur',	'thisi'),
('THOFE',	'Thomas',	'Fernandez',	'1999-06-12',	'2011-08-24',	'Joueur',	'thofe'),
('THOHE',	'Thomas',	'Heron',	'1998-01-23',	'2007-11-22',	'Joueur',	'thohe'),
('THOME',	'Thomas',	'Meunier',	'1991-09-12',	'1998-09-16',	'Joueur',	'thome'),
('THOTU',	'Thomas',	'Tuchel',	'1973-08-29',	'1980-02-28',	'Entraîneur',	'thotu'),
('VALHE',	'Valentin',	'Heurtin',	'1999-02-03',	'2006-04-11',	'Joueur',	'valhe'),
('ZOUAB',	'Zoubeida',	'Abdelmoula',	'1982-07-26',	'2017-11-09',	'Agent de Fédération',	'zouab');

DROP TABLE IF EXISTS `PresidentClub`;
CREATE TABLE `PresidentClub` (
  `idL` varchar(5) NOT NULL,
  `club` int(11) NOT NULL,
  PRIMARY KEY (`idL`),
  KEY `club` (`club`),
  CONSTRAINT `PresidentClub_ibfk_1` FOREIGN KEY (`idL`) REFERENCES `Licencié` (`id`),
  CONSTRAINT `PresidentClub_ibfk_2` FOREIGN KEY (`club`) REFERENCES `Club` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `PresidentClub` (`idL`, `club`) VALUES
('AURLA',	1),
('FRAMC',	2),
('NASAL',	3);

DROP TABLE IF EXISTS `StatsJoueurs`;
CREATE TABLE `StatsJoueurs` (
  `idJ` varchar(5) NOT NULL,
  `dateArrivee` date NOT NULL,
  `nbMatchsJoues` int(11) NOT NULL,
  `nbButsMarques` int(11) NOT NULL,
  `nbPassesDecisives` int(11) NOT NULL,
  `nbCartonsJaune` int(11) NOT NULL,
  `nbCartonsRouge` int(11) NOT NULL,
  PRIMARY KEY (`idJ`),
  CONSTRAINT `StatsJoueurs_ibfk_1` FOREIGN KEY (`idJ`) REFERENCES `Joueur` (`idL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `StatsJoueurs` (`idJ`, `dateArrivee`, `nbMatchsJoues`, `nbButsMarques`, `nbPassesDecisives`, `nbCartonsJaune`, `nbCartonsRouge`) VALUES
('ADIRA',	'2017-07-13',	530,	28,	11,	19,	4),
('ADRLE',	'2018-11-28',	12,	1,	0,	0,	0),
('ALEMO',	'2018-11-28',	12,	0,	2,	2,	0),
('ANGDI',	'2015-02-11',	476,	120,	34,	23,	6),
('BASGR',	'2018-11-28',	12,	0,	0,	6,	2),
('BOUSA',	'2014-06-21',	209,	45,	23,	8,	2),
('CLINJ',	'2015-12-30',	442,	123,	43,	34,	8),
('DIMPA',	'2013-03-04',	567,	203,	86,	34,	5),
('EDICA',	'2012-02-10',	487,	368,	126,	43,	3),
('FLORE',	'2018-11-28',	12,	4,	1,	3,	2),
('FLOTH',	'2015-11-23',	256,	132,	47,	8,	1),
('GIABU',	'2018-07-04',	759,	3,	1,	6,	1),
('HIRSA',	'2014-09-26',	301,	12,	7,	9,	3),
('JUABE',	'2016-08-12',	269,	8,	19,	18,	4),
('KILBO',	'2018-11-28',	12,	2,	2,	0,	0),
('KYLMB',	'2017-08-02',	153,	115,	34,	9,	2),
('LUCOC',	'2015-11-19',	376,	68,	20,	7,	1),
('MARAO',	'2013-07-15',	634,	142,	56,	18,	4),
('MARVE',	'2009-01-23',	419,	93,	48,	31,	8),
('MATCA',	'2018-11-28',	12,	7,	3,	0,	0),
('MAXLO',	'2014-10-21',	79,	11,	8,	2,	0),
('MOHAL',	'2018-11-28',	12,	3,	0,	1,	0),
('MORSA',	'2015-10-10',	163,	23,	10,	0,	0),
('NEYDA',	'2016-07-11',	372,	208,	63,	13,	3),
('PREKI',	'2017-11-29',	132,	6,	2,	4,	0),
('RAPOL',	'2018-11-28',	12,	4,	1,	0,	0),
('ROLVI',	'2015-02-14',	394,	24,	4,	5,	1),
('STEMA',	'2007-12-21',	783,	0,	2,	8,	0),
('THISI',	'2016-09-12',	127,	31,	12,	3,	0),
('THOFE',	'2018-11-28',	12,	0,	1,	0,	0),
('THOHE',	'2018-11-28',	12,	0,	0,	0,	0),
('THOME',	'2014-10-23',	221,	34,	19,	2,	0),
('VALHE',	'2018-11-28',	12,	0,	0,	0,	0);

-- 2019-03-20 11:07:11
