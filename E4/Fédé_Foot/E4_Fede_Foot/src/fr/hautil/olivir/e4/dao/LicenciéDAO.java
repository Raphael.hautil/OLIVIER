/**
 * Classe: LicenciéDAO
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package fr.hautil.olivir.e4.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class LicenciéDAO extends DAO{
    /**
     * Méthode qui change le mot de passe d'un licencié.
     *
     * @param pwd nouveau mot de passe du licencié
     * @param id id du licencié
     */

    public void updateMdp(String pwd, String id){
        Connection conn;
        try{
            conn = super.getConnection();
            String req = "UPDATE Licencié SET password = ? WHERE id = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,pwd);
            pstmt.setString(2,id);
            pstmt.executeUpdate();
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Méthode qui change le prénom d'un licencié
     *
     * @param prenom nouveau prénom du licencié
     * @param id id du licencié
     */

    public void updatePrenom(String prenom, String id){
        Connection conn;
        try{
            conn = super.getConnection();
            String req = "UPDATE Licencié SET prenom = ? WHERE id = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,prenom);
            pstmt.setString(2,id);
            pstmt.executeUpdate();
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Méthode qui change le nom d'un licencié.
     *
     * @param nom nouveau nom du licencié
     * @param id id du licencié
     */

    public void updateNom(String nom, String id){
        Connection conn;
        try{
            conn = super.getConnection();
            String req = "UPDATE Licencié SET nom = ? WHERE id = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,nom);
            pstmt.setString(2,id);
            pstmt.executeUpdate();
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Méthode qui change la date de naissance d'un licencié
     *
     * @param dateNaissance nouvelle date de naissance du licencié
     * @param id id du licencié
     */

    public void updateDateNaissance(String dateNaissance, String id){
        Connection conn;
        try{
            conn = super.getConnection();
            String req = "UPDATE Licencié SET dateNaissance = ? WHERE id = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,dateNaissance);
            pstmt.setString(2,id);
            pstmt.executeUpdate();
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
