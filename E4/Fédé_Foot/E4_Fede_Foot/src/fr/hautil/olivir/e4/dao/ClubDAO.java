/**
 * Classe: ClubDAO
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package fr.hautil.olivir.e4.dao;

import fr.hautil.olivir.e4.classes.Club;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class ClubDAO extends DAO {

    /**
     * Méthode qui retourne tous les clubs de la vues de données dans une liste
     *
     * @return list
     */

    public ArrayList<Club> getClubs(){
        ArrayList<Club> list = new ArrayList<>();
        Connection conn;
        Club c;
        try{
            conn = super.getConnection();
            Statement stmt = conn.createStatement();
            String req = "SELECT * FROM Club;";
            ResultSet result = stmt.executeQuery(req);
            while (result.next()){
                c = new Club();
                c.setId(result.getInt(1));
                c.setLibelle(result.getString(2));
                c.setDateCreation(result.getDate(3));
                list.add(c);
            }
            stmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Méthode qui retourne un club en fonction de son id
     *
     * @param id id du club
     * @return c
     */

    public Club getClubById(String id){
        Club c = new Club();
        Connection conn;
        try {
            conn = super.getConnection();
            String req = "SELECT * FROM Club WHERE id = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,id);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                c.setId(result.getInt(1));
                c.setLibelle(result.getString(2));
                c.setDateCreation(result.getDate(3));
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return c;
    }
}
