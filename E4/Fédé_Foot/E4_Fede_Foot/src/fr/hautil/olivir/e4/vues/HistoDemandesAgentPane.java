package fr.hautil.olivir.e4.vues;

import fr.hautil.olivir.e4.Main;
import fr.hautil.olivir.e4.classes.AgentFédé;
import fr.hautil.olivir.e4.classes.DemandeTraitee;
import fr.hautil.olivir.e4.dao.AgentFédéDAO;
import fr.hautil.olivir.e4.dao.DemandeDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.util.Callback;

import java.sql.Date;
import java.util.ArrayList;

public class HistoDemandesAgentPane extends GridPane{

    private AgentFédé agent = new AgentFédé();
    private TableView<DemandeTraitee> table = new TableView<>();
    private DemandeDAO dDao = new DemandeDAO();
    private Button btnRet = new Button("Retour");
    private Text txtInfos = new Text("");

    /**
     * Méthode qui récupère et retourne l'agent de la fédération connecté
     *
     * @return agent
     */

    public AgentFédé getAgent() {
        return agent;
    }

    /**
     * Méthode qui change l'agent de la fédération connecté
     *
     * @param agent agent de la fédération connecté
     */

    public void setAgent(AgentFédé agent) {
        this.agent = agent;
        init();
    }

    /**
     * Méthode qui affiche les informations de la demande quand on clique dessus
     */

    private void clickItem(){
        table.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                DemandeDAO dDao = new DemandeDAO();
                AgentFédéDAO aDao = new AgentFédéDAO();
                int id = table.getSelectionModel().getSelectedItem().getId();
                DemandeTraitee dt = dDao.getDemandeTraiteeById(String.valueOf(id));
                txtInfos.setText("Demande traitée et " + dt.getStatus().toLowerCase() + "e" +
                        " par " + aDao.getAgentById(dt.getIdA()).getPrenom() +
                        " le " + dt.getDateTraitement() +
                        " du club " + dt.getClub().getLibelle() +
                        " pour créer l'équipe " + dt.getNom());
            }
        });
    }

    /**
     * Méthode qui initialise la page d'historique des demandes traitées d'un agent
     */

    private void init(){
        txtInfos.setText("");
        ArrayList<DemandeTraitee> list = dDao.getHistoriqueDemandeByAgent(agent.getId());
        ObservableList list2 = FXCollections.observableArrayList(list);
        table.setItems(list2);

        table.setColumnResizePolicy(new Callback<TableView.ResizeFeatures, Boolean>() {
            @Override
            public Boolean call(TableView.ResizeFeatures param) {
                return true;
            }
        });

        clickItem();
    }

    /**
     * Méthode qui créé une page d'historique des demandes traitées d'un agent
     */

    public HistoDemandesAgentPane(){
        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        TableColumn<DemandeTraitee,String> id = new TableColumn<>("ID");
        TableColumn<DemandeTraitee,String> club = new TableColumn<>("Club");
        TableColumn<DemandeTraitee,String> nom = new TableColumn<>("Nom");
        TableColumn<DemandeTraitee,Date> dateTraitement = new TableColumn<>("Date de traitement");
        TableColumn<DemandeTraitee,String> status = new TableColumn<>("Status");

        table.getColumns().addAll(id,club,nom,dateTraitement,status);

        table.resizeColumn(club,10);

        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        club.setCellValueFactory(new PropertyValueFactory<>("club"));
        nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        dateTraitement.setCellValueFactory(new PropertyValueFactory<>("dateTraitement"));
        status.setCellValueFactory(new PropertyValueFactory<>("status"));

        id.setSortType(TableColumn.SortType.DESCENDING);

        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("agentPane"));
            }
        });

        this.add(table,1,1);
        this.add(txtInfos, 2,1);
        this.add(btnRet, 2,2);
    }
}
