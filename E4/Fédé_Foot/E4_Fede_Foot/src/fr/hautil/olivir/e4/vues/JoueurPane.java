/**
 * Classe: JoueurPane
 * @author: Raphaël Olivier
 * @version: 1.2
 */

package fr.hautil.olivir.e4.vues;

import fr.hautil.olivir.e4.Main;
import fr.hautil.olivir.e4.classes.Joueur;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.io.File;


public class JoueurPane extends GridPane{

    private Joueur joueur = null;

    private Text idJoueur = new Text("");
    private Text nomJoueur = new Text("");
    private Text prenomJoueur = new Text("");
    private Text posteJoueur = new Text("");
    private Text equipeJoueur = new Text("");
    private Text dateNaissanceJoueur = new Text("");
    private Text dateInscriptionJoueur = new Text("");
    private Text motDePasseJoueur = new Text("");
    private Text categorieJoueur = new Text("");
    private Text numeroJoueur = new Text("");
    private Text titre = new Text("");
    private Region photo = new Region();

    /**
     * Méthode qui récupère et retourne le joueur connecté
     *
     * @return joueur
     */

    public Joueur getJoueur() {
        return joueur;
    }

    /**
     * Méthode qui change le joueur connecté
     *
     * @param joueur joueur connecté
     */

    public void setJoueur(Joueur joueur){
        this.joueur = joueur;
        init();
    }

    /**
     * Méthode qui initialise la page de joueurs
     */

    private void init(){
        File f = new File("C:\\Users\\rapho\\IdeaProjects\\E4_Fede_Foot\\src\\fr\\hautil\\olivir\\e4\\img\\joueurs\\" + joueur.getId() + ".jpg");
        titre.setText("Bienvenue sur votre page de joueur !");
        idJoueur.setText("Id: " + joueur.getId());
        prenomJoueur.setText("Prénom: " + joueur.getPrenom());
        nomJoueur.setText("Nom: " + joueur.getNom());
        dateNaissanceJoueur.setText("Date de naissance: " + joueur.getDateNaissance());
        dateInscriptionJoueur.setText("Date d'inscription: " + joueur.getDateInscription());
        categorieJoueur.setText("Catégorie: " + joueur.getCategorie());
        posteJoueur.setText("Poste: " + joueur.getPoste());
        equipeJoueur.setText("Equipe: " + joueur.getEquipe());
        numeroJoueur.setText("Numéro: " + joueur.getNumero());
        motDePasseJoueur.setText("Mot de passe: " + joueur.getPassword());
        if (f.exists()){
            String img = Main.class.getResource("img/joueurs/" + joueur.getId() + ".jpg").toExternalForm();
            photo.setStyle("-fx-background-image: url('" + img + "');" + "-fx-background-size: 300;");
            photo.setVisible(true);
        }else{
            String img = Main.class.getResource("img/joueurs/inconnu.jpg").toExternalForm();
            photo.setStyle("-fx-background-image: url('" + img + "');" + "-fx-background-size: 300;");
            photo.setVisible(true);
        }

        Hyperlink linkMdp = new Hyperlink();
        Hyperlink linkInfo = new Hyperlink();
        Hyperlink linkStats = new Hyperlink();
        VBox vbox = new VBox();
        VBox vBoxLinks = new VBox();
        Button btnDeco = new Button("Déconnexion");

        photo.setId("avatar");

        this.add(vbox, 1, 2);
        this.add(vBoxLinks,2,2);
        this.add(btnDeco,7,8);

        vbox.getChildren().add(idJoueur);
        vbox.getChildren().add(prenomJoueur);
        vbox.getChildren().add(nomJoueur);
        vbox.getChildren().add(dateNaissanceJoueur);
        vbox.getChildren().add(dateInscriptionJoueur);
        vbox.getChildren().add(categorieJoueur);
        vbox.getChildren().add(posteJoueur);
        vbox.getChildren().add(equipeJoueur);
        vbox.getChildren().add(numeroJoueur);
        vbox.getChildren().add(motDePasseJoueur);

        vBoxLinks.getChildren().addAll(linkInfo, linkMdp, linkStats);

        linkMdp.setText("Modfier le mot de passe");
        linkMdp.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ModifMdpPane modifMdpPane = (ModifMdpPane) Main.getPanel("modifMdpPane");
                modifMdpPane.setJoueur(joueur);
                Main.getScene().setRoot(modifMdpPane);
            }
        });

        linkInfo.setText("Modifier mes informations");
        linkInfo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ModifInfoPane modifInfoPane = (ModifInfoPane) Main.getPanel("modifInfoPane");
                modifInfoPane.setJoueur(joueur);
                Main.getScene().setRoot(modifInfoPane);
            }
        });

        linkStats.setText("Voir mes statistiques");
        linkStats.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                StatsJoueurPane statsJoueurPane = (StatsJoueurPane) Main.getPanel("statsJoueurPane");
                statsJoueurPane.setJoueur(joueur);
                Main.getScene().setRoot(statsJoueurPane);
            }
        });

        btnDeco.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                LoginPane.deco();
            }
        });
    }

    /**
     * Création d'une page joueur.
     */

    public JoueurPane() {
        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        idJoueur.setText("");
        nomJoueur.setText("");
        prenomJoueur.setText("");
        dateNaissanceJoueur.setText("");
        dateInscriptionJoueur.setText("");
        motDePasseJoueur.setText("");
        categorieJoueur.setText("");
        posteJoueur.setText("");
        equipeJoueur.setText("");
        numeroJoueur.setText("");
        titre.setText("");
        photo.setVisible(false);

        titre.setId("title");

        this.add(photo,1,1);
        this.add(titre,2,0);
    }
}
