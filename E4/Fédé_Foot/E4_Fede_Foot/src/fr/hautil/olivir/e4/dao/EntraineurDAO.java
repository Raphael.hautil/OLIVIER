/**
 * Classe: EntraineurDAO
 * @author: Raphaël Olivier
 * @version: 1.1
 */

package fr.hautil.olivir.e4.dao;

import fr.hautil.olivir.e4.classes.Entraineur;
import fr.hautil.olivir.e4.classes.Equipe;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class EntraineurDAO extends DAO{

    /**
     * Méthode qui retourne tous les entraîneurs de la vues de données dans une liste
     *
     * @return list
     */

    public ArrayList<Entraineur> getEntraineurs(){
        ArrayList<Entraineur> list = new ArrayList<>();
        Connection conn;
        Equipe eq;
        Entraineur res;
        try {
            conn = super.getConnection();
            Statement stmt = conn.createStatement();
            String req = "SELECT * FROM Entraineur, Licencié, Equipe WHERE Entraineur.idL = Licencié.id AND Entraineur.equipe = Equipe.id";
            ResultSet result = stmt.executeQuery(req);
            while (result.next()){
                res = new Entraineur();
                eq = new Equipe();
                res.setId(result.getString(1));
                res.setPrenom(result.getString(4));
                res.setNom(result.getString(5));
                res.setDateNaissance(result.getDate(6));
                res.setDateInscription(result.getDate(7));
                res.setPassword(result.getString(9));
                eq.setId(result.getInt(10));
                eq.setLibelle(result.getString(11));
                eq.setCategorie(result.getString(12));
                eq.setDateCreation(result.getDate(13));
                eq.setClub(result.getInt(14));
                res.setEquipe(eq);
                list.add(res);
            }
            stmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Méthode qui retourne un entraîneur en fonction de son équipe
     *
     * @param equipe équipe de l'entraîneur
     * @return res
     */

    public Entraineur getEntraineurByEquipe(String equipe){
        Entraineur res = new Entraineur();
        Equipe eq = new Equipe();
        Connection conn;
        try {
            conn = super.getConnection();
            String req = "SELECT * FROM Entraineur, Licencié, Equipe WHERE Entraineur.idL = Licencié.id AND Entraineur.equipe = Equipe.id AND equipe = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1, equipe);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                res.setId(result.getString(1));
                res.setPrenom(result.getString(4));
                res.setNom(result.getString(5));
                res.setDateNaissance(result.getDate(6));
                res.setDateInscription(result.getDate(7));
                res.setPassword(result.getString(9));
                eq.setId(result.getInt(10));
                eq.setLibelle(result.getString(11));
                eq.setCategorie(result.getString(12));
                eq.setDateCreation(result.getDate(13));
                eq.setClub(result.getInt(14));
                res.setEquipe(eq);
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Méthode qui retourne un entraîneur en fonction de son id
     *
     * @param id id de l'entraîneur
     * @return res2
     */

    public Entraineur getEntraineurById(String id){
        Entraineur res2 = new Entraineur();
        Equipe eq = new Equipe();
        Connection conn;
        try{
            conn = super.getConnection();
            String req = "SELECT * FROM Entraineur, Licencié, Equipe WHERE Entraineur.idL = Licencié.id AND Entraineur.equipe = Equipe.id AND Licencié.id = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,id);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                res2.setId(result.getString(1));
                res2.setNom(result.getString(5));
                res2.setPrenom(result.getString(4));
                res2.setDateNaissance(result.getDate(6));
                res2.setDateInscription(result.getDate(7));
                res2.setPassword(result.getString(9));
                eq.setId(result.getInt(10));
                eq.setLibelle(result.getString(11));
                eq.setCategorie(result.getString(12));
                eq.setDateCreation(result.getDate(13));
                eq.setClub(result.getInt(14));
                res2.setEquipe(eq);
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return res2;
    }
}
