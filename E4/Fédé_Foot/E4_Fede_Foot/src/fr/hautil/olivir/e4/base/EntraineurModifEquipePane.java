/**
 * Classe: EntraineurModifEquipePane
 * @author: Raphaël Olivier
 * @version: 1.2
 */

package fr.hautil.olivir.e4.base;

import fr.hautil.olivir.e4.classes.Equipe;
import fr.hautil.olivir.e4.classes.Joueur;
import fr.hautil.olivir.e4.classes.JoueurCircle;
import fr.hautil.olivir.e4.classes.StatsJoueurs;
import fr.hautil.olivir.e4.dao.JoueurDAO;
import fr.hautil.olivir.e4.dao.StatsJoueursDAO;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import java.util.ArrayList;

public class EntraineurModifEquipePane extends GridPane{

    private Equipe equipe;
    private JoueurDAO jDao = new JoueurDAO();
    private AnchorPane terrain = new AnchorPane();
    private Text statsJ = new Text("");
    private Circle circleCopy = new Circle();

    /**
     * Méthode qui récupère et retourne l'équipe
     *
     * @return equipe
     */

    public Equipe getEquipe() {
        return equipe;
    }

    /**
     * Méthode qui change l'équipe
     *
     * @param equipe équipe
     */

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
        init();
    }

    /**
     * Méthode qui initialise la page de modification de l'équipe
     */

    private void init(){
        String img = Main.class.getResource("../img/equipe/terrain.png").toExternalForm();
        terrain.setStyle("-fx-background-image: url('" + img + "');" + "-fx-background-size: 700;");

        ArrayList<Joueur> listJoueur = jDao.getJoueursByEquipe(equipe);

        circleCopy.setFill(null);
        statsJ.setText("");

        String j1 = Main.class.getResource("../img/joueurs/" + listJoueur.get(0).getId() + ".jpg").toExternalForm();
        String j2 = Main.class.getResource("../img/joueurs/" + listJoueur.get(1).getId() + ".jpg").toExternalForm();
        String j3 = Main.class.getResource("../img/joueurs/" + listJoueur.get(2).getId() + ".jpg").toExternalForm();
        String j4 = Main.class.getResource("../img/joueurs/" + listJoueur.get(3).getId() + ".jpg").toExternalForm();
        String j5 = Main.class.getResource("../img/joueurs/" + listJoueur.get(4).getId() + ".jpg").toExternalForm();
        String j6 = Main.class.getResource("../img/joueurs/" + listJoueur.get(5).getId() + ".jpg").toExternalForm();
        String j7 = Main.class.getResource("../img/joueurs/" + listJoueur.get(6).getId() + ".jpg").toExternalForm();
        String j8 = Main.class.getResource("../img/joueurs/" + listJoueur.get(7).getId() + ".jpg").toExternalForm();
        String j9 = Main.class.getResource("../img/joueurs/" + listJoueur.get(8).getId() + ".jpg").toExternalForm();
        String j10 = Main.class.getResource("../img/joueurs/" + listJoueur.get(9).getId() + ".jpg").toExternalForm();
        String j11 = Main.class.getResource("../img/joueurs/" + listJoueur.get(10).getId() + ".jpg").toExternalForm();

        Image image = new Image(j1);
        Image image2 = new Image(j2);
        Image image3 = new Image(j3);
        Image image4 = new Image(j4);
        Image image5 = new Image(j5);
        Image image6 = new Image(j6);
        Image image7 = new Image(j7);
        Image image8 = new Image(j8);
        Image image9 = new Image(j9);
        Image image10 = new Image(j10);
        Image image11 = new Image(j11);

        //ATTAQUANTS

        JoueurCircle c1 = new JoueurCircle(40,listJoueur.get(0));
        c1.setFill(new ImagePattern(image));
        c1.setLayoutX(500);
        c1.setLayoutY(70);

        JoueurCircle c2 = new JoueurCircle(40,listJoueur.get(1));
        c2.setFill(new ImagePattern(image2));
        c2.setLayoutX(550);
        c2.setLayoutY(220);

        JoueurCircle c3 = new JoueurCircle(40,listJoueur.get(2));
        c3.setFill(new ImagePattern(image3));
        c3.setLayoutX(500);
        c3.setLayoutY(357);

        //DEFENSEURS

        JoueurCircle c4 = new JoueurCircle(40,listJoueur.get(3));
        c4.setFill(new ImagePattern(image4));
        c4.setLayoutX(185);
        c4.setLayoutY(70);

        JoueurCircle c5 = new JoueurCircle(40,listJoueur.get(4));
        c5.setFill(new ImagePattern(image5));
        c5.setLayoutX(150);
        c5.setLayoutY(160);

        JoueurCircle c6 = new JoueurCircle(40,listJoueur.get(5));
        c6.setFill(new ImagePattern(image6));
        c6.setLayoutX(150);
        c6.setLayoutY(270);

        JoueurCircle c7 = new JoueurCircle(40,listJoueur.get(6));
        c7.setFill(new ImagePattern(image7));
        c7.setLayoutX(185);
        c7.setLayoutY(357);

        //GARDIEN

        JoueurCircle c8 = new JoueurCircle(40,listJoueur.get(7));
        c8.setFill(new ImagePattern(image8));
        c8.setLayoutX(50);
        c8.setLayoutY(222);

        //MILIEUX

        JoueurCircle c9 = new JoueurCircle(40,listJoueur.get(8));
        c9.setFill(new ImagePattern(image9));
        c9.setLayoutX(350);
        c9.setLayoutY(70);

        JoueurCircle c10 = new JoueurCircle(40, listJoueur.get(9));
        c10.setFill(new ImagePattern(image10));
        c10.setLayoutX(350);
        c10.setLayoutY(220);

        JoueurCircle c11 = new JoueurCircle(40,listJoueur.get(10));
        c11.setFill(new ImagePattern(image11));
        c11.setLayoutX(350);
        c11.setLayoutY(357);

        circleCopy.setVisible(true);

        c1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                circleCopy.setFill(new ImagePattern(image));
                StatsJoueursDAO sDao = new StatsJoueursDAO();
                StatsJoueurs stats = sDao.getStatsByJoueur(c1.getJoueur().getId());
                statsJ.setText(stats.toString());
                circleCopy.setRadius(150);
                circleCopy.setLayoutX(700);
                circleCopy.setLayoutY(300);
            }
        });

        c2.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                circleCopy.setFill(new ImagePattern(image2));
                StatsJoueursDAO sDao = new StatsJoueursDAO();
                StatsJoueurs stats = sDao.getStatsByJoueur(c2.getJoueur().getId());
                statsJ.setText(stats.toString());
                circleCopy.setRadius(150);
                circleCopy.setLayoutY(300);
                circleCopy.setLayoutX(700);
            }
        });

        c3.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                circleCopy.setFill(new ImagePattern(image3));
                StatsJoueursDAO sDao = new StatsJoueursDAO();
                StatsJoueurs stats = sDao.getStatsByJoueur(c3.getJoueur().getId());
                statsJ.setText(stats.toString());
                circleCopy.setLayoutX(700);
                circleCopy.setRadius(150);
                circleCopy.setLayoutY(300);
            }
        });

        c4.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                circleCopy.setFill(new ImagePattern(image4));
                StatsJoueursDAO sDao = new StatsJoueursDAO();
                StatsJoueurs stats = sDao.getStatsByJoueur(c4.getJoueur().getId());
                circleCopy.setRadius(150);
                statsJ.setText(stats.toString());
                circleCopy.setLayoutX(700);
                circleCopy.setLayoutY(300);
            }
        });

        c5.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                circleCopy.setFill(new ImagePattern(image5));
                StatsJoueursDAO sDao = new StatsJoueursDAO();
                StatsJoueurs stats = sDao.getStatsByJoueur(c5.getJoueur().getId());
                statsJ.setText(stats.toString());
                circleCopy.setLayoutY(300);
                circleCopy.setRadius(150);
                circleCopy.setLayoutX(700);
            }
        });

        c6.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                circleCopy.setFill(new ImagePattern(image6));
                StatsJoueursDAO sDao = new StatsJoueursDAO();
                StatsJoueurs stats = sDao.getStatsByJoueur(c6.getJoueur().getId());
                circleCopy.setLayoutX(700);
                statsJ.setText(stats.toString());
                circleCopy.setRadius(150);
                circleCopy.setLayoutY(300);
            }
        });

        c7.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                circleCopy.setFill(new ImagePattern(image7));
                StatsJoueursDAO sDao = new StatsJoueursDAO();
                circleCopy.setRadius(150);
                StatsJoueurs stats = sDao.getStatsByJoueur(c7.getJoueur().getId());
                statsJ.setText(stats.toString());
                circleCopy.setLayoutX(700);
                circleCopy.setLayoutY(300);
            }
        });

        c8.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                circleCopy.setFill(new ImagePattern(image8));
                circleCopy.setRadius(150);
                StatsJoueursDAO sDao = new StatsJoueursDAO();
                StatsJoueurs stats = sDao.getStatsByJoueur(c8.getJoueur().getId());
                statsJ.setText(stats.toString());
                circleCopy.setLayoutX(700);
                circleCopy.setLayoutY(300);
            }
        });

        c9.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                circleCopy.setFill(new ImagePattern(image9));
                circleCopy.setRadius(150);
                circleCopy.setLayoutX(700);
                StatsJoueursDAO sDao = new StatsJoueursDAO();
                StatsJoueurs stats = sDao.getStatsByJoueur(c9.getJoueur().getId());
                statsJ.setText(stats.toString());
                circleCopy.setLayoutY(300);
            }
        });

        c10.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                circleCopy.setFill(new ImagePattern(image10));
                circleCopy.setRadius(150);
                circleCopy.setLayoutX(700);
                circleCopy.setLayoutY(300);
                StatsJoueursDAO sDao = new StatsJoueursDAO();
                StatsJoueurs stats = sDao.getStatsByJoueur(c10.getJoueur().getId());
                statsJ.setText(stats.toString());
            }
        });

        c11.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                circleCopy.setFill(new ImagePattern(image11));
                circleCopy.setRadius(150);
                circleCopy.setLayoutY(300);
                circleCopy.setLayoutX(700);
                StatsJoueursDAO sDao = new StatsJoueursDAO();
                StatsJoueurs stats = sDao.getStatsByJoueur(c11.getJoueur().getId());
                statsJ.setText(stats.toString());
            }
        });
        terrain.getChildren().addAll(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11);
    }

    /**
     * Créé une page de modification d'équipe pour les entraîneurs
     */

    public EntraineurModifEquipePane(){
        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        Button btnRet = new Button("Retour");
        Region joueur1 = new Region();

        terrain.setId("terrain");
        joueur1.setId("joueur1");

        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("entraineurPane"));
            }
        });

        VBox vBoxStats = new VBox();
        vBoxStats.getChildren().addAll(circleCopy,statsJ);

        this.add(terrain,1,1);
        this.add(joueur1,1,2);
        this.add(btnRet,1,3);
        this.add(vBoxStats,2,1);
    }
}