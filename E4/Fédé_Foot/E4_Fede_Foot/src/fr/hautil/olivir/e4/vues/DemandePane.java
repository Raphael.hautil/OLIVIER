/**
 * Classe: DemandePane
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package fr.hautil.olivir.e4.vues;

import fr.hautil.olivir.e4.Main;
import fr.hautil.olivir.e4.classes.PresidentClub;
import fr.hautil.olivir.e4.dao.DemandeDAO;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class DemandePane extends GridPane {

    private PresidentClub presidentClub;
    private Text textTrouNom = new Text("  ");
    private Text textTrouCategorie = new Text("  ");
    private Text titre = new Text("");
    private TextField txtFieldNom = new TextField("");
    private TextField txtFieldCategorie = new TextField("");
    private Text textNom = new Text("Saisir le nom de l'équipe");
    private Text textCategorie = new Text("Saisir la catégorie de l'équipe");
    private Text textResult = new Text("");

    /**
     * Méthode qui récupère et retourne le président de club connecté
     *
     * @return presidentClub
     */

    public PresidentClub getPresidentClub(){
        return presidentClub;
    }

    /**
     * Méthode qui change le président de club connecté
     *
     * @param presidentClub président de club
     */

    public void setPresidentClub(PresidentClub presidentClub) {
        this.presidentClub = presidentClub;
        init();
    }

    /**
     * Méthode qui initialise la page de demande de création d'équipe
     */

    private void init(){
        titre.setText("Nouvelle demande de créarion d'équipe par " + presidentClub.getPrenom() + " " + presidentClub.getNom() + " pour le club " + presidentClub.getClub().getLibelle());
        txtFieldCategorie.setText("");
        txtFieldNom.setText("");
        textResult.setText("");

        VBox vBox = new VBox();
        HBox hBoxNom = new HBox();
        HBox hBoxCategorie = new HBox();

        Button btnRet = new Button("Retour");
        Button btnVal = new Button("Valider");

        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("presidentPane"));
            }
        });

        btnVal.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DemandeDAO dDao = new DemandeDAO();
                dDao.createDemande(String.valueOf(presidentClub.getClub().getId()), txtFieldNom.getText(), txtFieldCategorie.getText());
                textResult.setText("Demande envoyée avec succès");
            }
        });

        hBoxNom.getChildren().addAll(textNom,textTrouNom,txtFieldNom);
        hBoxCategorie.getChildren().addAll(textCategorie,textTrouCategorie,txtFieldCategorie);
        vBox.getChildren().addAll(titre, hBoxNom, hBoxCategorie);

        this.add(vBox,1,1);
        this.add(btnVal,2,3);
        this.add(btnRet, 3,3);
    }

    public DemandePane(){
        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        titre.setText("");
        titre.setId("title");
        textResult.setText("");

        this.add(textResult, 1,2);
    }
}
