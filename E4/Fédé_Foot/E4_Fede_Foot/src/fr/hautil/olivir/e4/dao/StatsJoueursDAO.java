/**
 * Classe: StatsJoueursDAO
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package fr.hautil.olivir.e4.dao;

import fr.hautil.olivir.e4.classes.StatsJoueurs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class StatsJoueursDAO extends DAO {

    /**
     * Méthode qui récupère les statistiques d'un joueur en fonction de son identifiant.
     *
     * @param id id du joueur
     * @return stats
     */

    public StatsJoueurs getStatsByJoueur(String id){
        Connection conn;
        StatsJoueurs stats = new StatsJoueurs();
        try{
            conn = super.getConnection();
            String req = "SELECT * FROM StatsJoueurs WHERE idJ = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,id);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                stats.setIdJ(result.getString(1));
                stats.setDateArrivee(result.getDate(2));
                stats.setNbMatchsJoues(result.getInt(3));
                stats.setNbButsMarques(result.getInt(4));
                stats.setNbPassesDecisives(result.getInt(5));
                stats.setNbCartonsJaune(result.getInt(6));
                stats.setNbCartonsRouge(result.getInt(7));
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return stats;
    }
}
