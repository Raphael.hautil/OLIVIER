/**
 * Classe: PalmaresDAO
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package fr.hautil.olivir.e4.dao;

import fr.hautil.olivir.e4.classes.Competition;
import fr.hautil.olivir.e4.classes.LgePalmares;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class PalmaresDAO extends DAO{

    /**
     * Méthode qui récupère le palmarès d'un club
     *
     * @param id id du club
     * @return palmares
     */

    public String getPalmaresByClub(String id){
        Connection conn;
        ArrayList<LgePalmares> palmares = new ArrayList<>();
        LgePalmares ligne;
        Competition compet;
        try {
            conn = super.getConnection();
            String req = "SELECT * FROM LgePalmares, Competition WHERE idClub = ? AND LgePalmares.idCompetition = Competition.id;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,id);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                ligne = new LgePalmares();
                compet = new Competition();
                compet.setId(result.getInt(5));
                compet.setLibelle(result.getString(6));
                ligne.setId(result.getInt(1));
                ligne.setIdClub(result.getInt(2));
                ligne.setCompetition(compet);
                ligne.setNombre(result.getInt(4));
                palmares.add(ligne);
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return "Palmarès de l'équipe: " + palmares;
    }
}
