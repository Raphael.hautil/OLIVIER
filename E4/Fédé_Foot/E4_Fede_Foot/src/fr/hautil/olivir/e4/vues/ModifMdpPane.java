/**
 * Classe: ModifMdpPane
 * @author: Raphaël Olivier
 * @version: 1.2
 */

package fr.hautil.olivir.e4.vues;

import fr.hautil.olivir.e4.Main;
import fr.hautil.olivir.e4.classes.Joueur;
import fr.hautil.olivir.e4.dao.LicenciéDAO;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class ModifMdpPane extends GridPane {

    private Joueur joueur = null;

    private Text info = new Text("");
    private LicenciéDAO lDao = new LicenciéDAO();
    private Text actualMdp = new Text("");

    /**
     * Méthode qui récupère et retourne le joueur connecté
     *
     * @return joueur
     */

    public Joueur getJoueur() {
        return joueur;
    }

    /**
     * Méthode qui change le joueur connecté
     *
     * @param joueur joueur connecté
     */

    public void setJoueur(Joueur joueur) {
        this.joueur = joueur;
        init();
    }

    private void init(){
        actualMdp.setText("Mot de passe actuel: " + joueur.getPassword());
        info.setText("");
        Text saisie1 = new Text("Saisir le nouveau mot de passe");
        TextField newMdp1 = new TextField();
        Text saisie2 = new Text("Confirmer le mot de passe");
        TextField newMdp2 = new TextField();
        Button btnVal = new Button("Valider");
        Button btnRet = new Button("Retour");

        this.add(saisie1,1,2);
        this.add(newMdp1,2,2);
        this.add(saisie2,1,3);
        this.add(newMdp2,2,3);
        this.add(btnVal,3,3);
        this.add(btnRet,7,7);

        btnVal.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (newMdp1.getText().equals(newMdp2.getText())) {
                    lDao.updateMdp(newMdp1.getText(), joueur.getId());
                    info.setText("Mot de passe actualisé avec succès.");
                    joueur.setPassword(newMdp1.getText());
                    actualMdp.setText("Mot de passe actuel: " + joueur.getPassword());
                    newMdp1.setText("");
                    newMdp2.setText("");
                }else{
                    info.setText("Les mots de passe ne correspondent pas.");
                    newMdp1.setText("");
                    newMdp2.setText("");
                }
            }
        });

        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                JoueurPane jPane = (JoueurPane) Main.getPanel("joueurPane");
                jPane.setJoueur(joueur);
                Main.getScene().setRoot(jPane);
            }
        });
    }

    /**
     * Création d'une page de modification de mot de passe.
     */

    public ModifMdpPane(){
        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25,25,25,25));

        actualMdp.setText("");
        info.setText("");

        this.add(actualMdp, 1,1);
        this.add(info,1,4);
    }
}
