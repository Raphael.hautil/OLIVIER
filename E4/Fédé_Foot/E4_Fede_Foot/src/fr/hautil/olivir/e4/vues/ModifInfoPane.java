/**
 * Classe: ModifInfoPane
 * @author: Raphaël Olivier
 * @version: 1.1
 */

package fr.hautil.olivir.e4.vues;

import fr.hautil.olivir.e4.Main;
import fr.hautil.olivir.e4.classes.Joueur;
import fr.hautil.olivir.e4.dao.JoueurDAO;
import fr.hautil.olivir.e4.dao.LicenciéDAO;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ModifInfoPane extends GridPane {

    private Joueur joueur = null;

    private static LicenciéDAO lDao = new LicenciéDAO();
    private static JoueurDAO jDao = new JoueurDAO();

    private static Text textPrenom = new Text();
    private static Text textNom = new Text();
    private static Text textDateNaissance = new Text();
    private static TextField txtFieldPrenom = new TextField();
    private static TextField txtFieldNom = new TextField();
    private static TextField txtFieldDateNaissance = new TextField();
    private static Text messageP = new Text("");
    private static Text messageN = new Text("");
    private static Text messageD = new Text("");
    private static Text message = new Text("");

    /**
     * Méthode qui change le joueur connecté
     *
     * @param joueur joueur connecté
     */

    public void setJoueur(Joueur joueur) {
        this.joueur = joueur;
        init();
    }

    /**
     * Méthode qui récupère et retourne le joueur connecté
     *
     * @return joueur
     */

    public Joueur getJoueur() {
        return joueur;
    }

    private void init(){
        textPrenom.setText("Prénom: " + joueur.getPrenom());
        textNom.setText("Nom: " + joueur.getNom());
        textDateNaissance.setText("Date de naissance: " + joueur.getDateNaissance().toString());
        txtFieldPrenom.setText("");
        txtFieldNom.setText("");
        txtFieldDateNaissance.setText("");

        VBox vBox = new VBox();
        VBox vBox2 = new VBox();
        VBox vBox3 = new VBox();
        Button btnRet = new Button("Retour");
        Button btnVal = new Button("Valider");

        vBox.getChildren().add(textPrenom);
        vBox.getChildren().add(textNom);
        vBox.getChildren().add(textDateNaissance);

        vBox2.getChildren().add(txtFieldPrenom);
        vBox2.getChildren().add(txtFieldNom);
        vBox2.getChildren().add(txtFieldDateNaissance);

        vBox3.getChildren().add(messageP);
        vBox3.getChildren().add(messageN);
        vBox3.getChildren().add(messageD);

        this.add(vBox,1,1);
        this.add(vBox2, 2,1);
        this.add(vBox3,3,1);
        this.add(btnVal,2,2);
        this.add(btnRet,7,7);

        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                JoueurPane jPane = (JoueurPane) Main.getPanel("joueurPane");
                jPane.setJoueur(joueur);
                Main.getScene().setRoot(jPane);
            }
        });

        btnVal.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (txtFieldPrenom.getText().equals("")){}
                else {
                    lDao.updatePrenom(txtFieldPrenom.getText(), joueur.getId());
                    joueur.setPrenom(txtFieldPrenom.getText());
                    textPrenom.setText("Prénom: " + joueur.getPrenom());
                    messageP.setText("Prénom modifié avec succès.");
                }if (txtFieldNom.getText().equals("")){}
                else {
                    lDao.updateNom(txtFieldNom.getText(), joueur.getId());
                    joueur.setNom(txtFieldNom.getText());
                    textNom.setText("Nom: " + joueur.getNom());
                    messageN.setText("Nom modifié avec succès.");
                }if (txtFieldDateNaissance.getText().equals("")){}
                else {
                    lDao.updateDateNaissance(txtFieldDateNaissance.getText(), joueur.getId());
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    java.util.Date dateN = null;
                    try {
                        dateN = sdf.parse(txtFieldDateNaissance.getText());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    joueur.setDateNaissance(dateN);
                    textDateNaissance.setText("Date de naissance: " + sdf.format(joueur.getDateNaissance()));
                    messageD.setText("Date de naissance modifiée avec succès.");
                }if (txtFieldPrenom.getText().equals("") && txtFieldNom.getText().equals("") && txtFieldDateNaissance.getText().equals("")){
                    message.setText("Tous les champs sont vides.");
                }
                txtFieldPrenom.setText("");
                txtFieldNom.setText("");
                txtFieldDateNaissance.setText("");
            }
        });
    }

    /**
     * Création d'une page de modification des informations d'un joueur.
     */

    public ModifInfoPane(){
        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        message.setText("");
        messageD.setText("");
        messageN.setText("");
        messageP.setText("");

        this.add(message,1,3);
    }
}