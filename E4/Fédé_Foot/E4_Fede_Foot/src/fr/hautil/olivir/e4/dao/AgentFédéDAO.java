/**
 * Classe : AgentFédéDAO
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package fr.hautil.olivir.e4.dao;

import fr.hautil.olivir.e4.classes.AgentFédé;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class AgentFédéDAO extends DAO {

    /**
     * Méthode qui retourne tous les agents de la fédération de la vues de données dans une liste
     *
     * @return list
     */

    public ArrayList<AgentFédé> getAgents(){
        ArrayList<AgentFédé> list = new ArrayList<>();
        AgentFédé af;
        Connection conn;
        try {
            conn = super.getConnection();
            Statement stmt = conn.createStatement();
            String req = "SELECT * FROM AgentFédé, Licencié WHERE AgentFédé.idL = Licencié.id;";
            ResultSet result = stmt.executeQuery(req);
            while (result.next()){
                af = new AgentFédé();
                af.setId(result.getString(1));
                af.setHierarchie(result.getString(2));
                af.setPrenom(result.getString(4));
                af.setNom(result.getString(5));
                af.setDateNaissance(result.getDate(6));
                af.setDateInscription(result.getDate(7));
                list.add(af);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Méthode qui retourne un agent de la fédération en fonction de son id
     *
     * @param id id de l'agent de la fédération
     * @return af
     */

    public AgentFédé getAgentById(String id){
        AgentFédé af = new AgentFédé();
        Connection conn;
        try{
            conn = super.getConnection();
            String req = "SELECT * FROM AgentFédé, Licencié WHERE AgentFédé.idL = Licencié.id AND idL = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,id);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                af.setId(result.getString(1));
                af.setHierarchie(result.getString(2));
                af.setPrenom(result.getString(4));
                af.setNom(result.getString(5));
                af.setDateNaissance(result.getDate(6));
                af.setDateInscription(result.getDate(7));
                af.setPassword(result.getString(9));
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return af;
    }
}