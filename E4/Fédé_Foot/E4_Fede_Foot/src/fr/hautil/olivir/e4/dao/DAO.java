/**
 * Classe: DAO
 * @author: Raphaël Olivier
 * @version; 1.1
 */

package fr.hautil.olivir.e4.dao;

import fr.hautil.olivir.e4.classes.Licencié;

import java.sql.*;
import java.text.SimpleDateFormat;

public class DAO {
    private String driver = "com.mysql.cj.jdbc.Driver";
    private String url = "jdbc:mysql://mysql-olvraphael.alwaysdata.net/olvraphael_e4_fede-foot?useSSL=false";
    private String user="181861";
    private String pwd = "uthaephee7pi";

    /**
     * Méthode qui permet de se connecter à la vues de données
     *
     * @return conn
     */

    public Connection getConnection(){
        Connection conn = null;
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url,user,pwd);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * Méthode qui permet de s'identifier en utilisant les identifiants de la vues de données
     *
     * @param idL id du licencié
     * @param pwdL mot de passe du licencié
     * @return id, pwd, type
     */

    public Licencié login(String idL, String pwdL){
        Connection conn;
        Licencié l = new Licencié();
        try{
            conn = getConnection();
            String req = "SELECT id, password, type FROM Licencié WHERE id = ? AND password = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,idL);
            pstmt.setString(2,pwdL);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                l.setId(result.getString(1));
                l.setPassword(result.getString(2));
                l.setType(result.getString(3));
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return l;
    }

    /**
     * Méthode qui permet l'inscription d'un licencié
     *
     * @param prenom prénom du licencié
     * @param nom nom du licencié
     * @param dateNaissance date de naisance du licencié
     * @param mdp mot de passe du licencié
     * @param type type du licencié
     */

    public void insciption(String prenom, String nom, String dateNaissance, String mdp, String type){
        Connection conn;
        String format = "yyyy-MM-dd";
        try{
            conn = getConnection();
            SimpleDateFormat formater = new SimpleDateFormat(format);
            java.util.Date date = new java.util.Date();
            String req = "INSERT INTO Licencié VALUES(?,?,?,?,?,?,?);";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,(prenom.substring(0,3) + nom.substring(0,2)).toUpperCase());
            pstmt.setString(2,prenom);
            pstmt.setString(3,nom);
            pstmt.setString(4,dateNaissance);
            pstmt.setString(5,formater.format(date));
            pstmt.setString(6,type);
            pstmt.setString(7,mdp);
            pstmt.executeUpdate();
            pstmt.close();
            if (type.equals("Joueur")){
                String req2 = "INSERT INTO Joueur(idL, categorie, equipe) VALUES (?,?,?);";
                PreparedStatement pstmt2 = conn.prepareStatement(req2);
                pstmt2.setString(1,(prenom.substring(0,3) + nom.substring(0,2)).toUpperCase());
                pstmt2.setString(2,getCategorie(Date.valueOf(dateNaissance)));
                pstmt2.setString(3,"0");
                pstmt2.executeUpdate();
                pstmt2.close();
            }else if (type.equals("Entraîneur")){
                String req2 = "INSERT INTO Entraineur(idL, equipe) VALUES (?,?);";
                PreparedStatement pstmt2 = conn.prepareStatement(req2);
                pstmt2.setString(1,(prenom.substring(0,3) + nom.substring(0,2)).toUpperCase());
                pstmt2.setString(2,"0");
                pstmt2.executeUpdate();
                pstmt2.close();
            }
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Méthode qui retourne la catégorie d'un joueur en fonction de son année de naissance
     *
     * @param dateNaissance date de naissance du joueur
     * @return categorie
     */

    private String getCategorie(Date dateNaissance){
        int age;
        String categorie = null;
        age = 2019 - (dateNaissance.getYear() + 1900);
        if (age >= 5 && age < 9){
            categorie = "Débutants";
        }else if (age >= 9 && age < 11){
            categorie = "Poussins";
        }else if (age >= 11 && age < 13){
            categorie = "Benjamins";
        }else if (age >= 13 && age < 15){
            categorie = "U15";
        }else if (age >= 15 && age < 17){
            categorie = "U17";
        }else if (age >= 17 && age < 19){
            categorie = "U19";
        }else if (age >= 19 && age < 21){
            categorie = "U21";
        }else if (age >= 21 && age < 35){
            categorie = "Séniors";
        }else if (age >= 35 && age < 45){
            categorie = "Anciens";
        }else if (age >= 45){
            categorie = "Vétérans";
        }
        return categorie;
    }
}