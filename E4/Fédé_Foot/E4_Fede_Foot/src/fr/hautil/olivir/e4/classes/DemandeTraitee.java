/**
 * Classe: DemandeTraitee
 * @author: Raphaël Olivier
 * @version: 1.1
 */

package fr.hautil.olivir.e4.classes;

import java.sql.Date;

public class DemandeTraitee extends Demande {
    private String idA;
    private Date dateTraitement;
    private String status;

    /**
     * Méthode qui créé une demande traitée vide
     */

    public DemandeTraitee(){
    }

    /**
     * Méthode qui créé une demande traitée avec ses attributs
     *
     * @param id           id de la demande
     * @param club         club de la demande
     * @param nom          nom du club demandé
     * @param categorie    catégorie de l'équipe demandé
     * @param dateCreation date de création de la demande
     * @param status       status de la demande
     * @param idA          id de l'agent de la fédération qui a traité la demande
     * @param dateTraitement date de traitement de la demande
     */

    public DemandeTraitee(int id, Club club, String nom, String categorie, java.util.Date dateCreation, String status, String idA, Date dateTraitement, String statusT) {
        super(id, club, nom, categorie, dateCreation, status);
        this.idA = idA;
        this.dateTraitement = dateTraitement;
        this.status = statusT;
    }

    /**
     * Méthode qui récupère et retourne l'id de l'agent de la fédération qui a traité la demande
     *
     * @return idA
     */

    public String getIdA() {
        return idA;
    }

    /**
     * Méthode qui change l'id de l'agent de la fédération qui a traité la demande
     *
     * @param idA id de l'agent de la fédération qui a traité la demande
     */

    public void setIdA(String idA) {
        this.idA = idA;
    }

    /**
     * Méthode qui récupère et retourne la date de traitement de la demande
     *
     * @return dateTraitement
     */

    public Date getDateTraitement() {
        return dateTraitement;
    }

    /**
     * Méthode qui change la date de traitement de la demande
     *
     * @param dateTraitement date de traitement de la demande
     */

    public void setDateTraitement(Date dateTraitement) {
        this.dateTraitement = dateTraitement;
    }

    /**
     * Méthode qui récupère et retourne le status de la demande traitée
     *
     * @return status
     */

    @Override
    public String getStatus() {
        return status;
    }

    /**
     * Méthode qui change le status de la demande traitée
     *
     * @param status status de la demande traitée
     */

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Méthode qui affiche la demande traitée avec ses attributs
     *
     * @return id, idP, nom, dateCreation, status, idA, dateTraitement
     */

    @Override
    public String toString() {
        return "DemandeTraitee{" +
                "Id=" + id + '\'' +
                ", club='" + club + '\'' +
                ", nom='" + nom + '\'' +
                ", categorie='" + categorie + '\'' +
                ", dateCreation=" + dateCreation +
                ", status='" + status + '\'' +
                ", idA='" + idA + '\'' +
                ", dateTraitement=" + dateTraitement +
                '}';
    }
}
