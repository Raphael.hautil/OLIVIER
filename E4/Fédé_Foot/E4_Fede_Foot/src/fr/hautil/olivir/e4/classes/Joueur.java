/**
 * Classe: Joueur
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package fr.hautil.olivir.e4.classes;

import java.util.Date;

public class Joueur extends Licencié{
    private String categorie;
    private boolean estPro;
    private String poste;
    private int numero;
    private Equipe equipe;

    /**
     * Méthode qui créé un joueur vide
     */

    public Joueur() {
    }

    /**
     * Méthode qui créé un joueur avec ses attributs
     *
     * @param id id du joueur
     * @param prenom prénom du joueur
     * @param nom nom du joueur
     * @param dateNaissance date de naissance du joueur
     * @param dateInscription date d'inscription du joueur
     * @param type type du licencié
     * @param password mot de passe du joueur
     * @param categorie catégorie du joueur
     * @param estPro niveau du joueur
     * @param poste poste du joueur
     * @param numero numéro du joueur
     * @param equipe équipe du joueur
     */

    public Joueur(String id, String prenom, String nom, Date dateNaissance, Date dateInscription, String type, String password, String categorie, boolean estPro, String poste, int numero, Equipe equipe){
        super(id, prenom, nom, dateNaissance, dateInscription, type, password);
        this.categorie = categorie;
        this.estPro = estPro;
        this.poste = poste;
        this.numero = numero;
        this.equipe = equipe;
    }

    /**
     * Méthode qui récupère et retourne la catégorie du joueur
     *
     * @return categorie
     */

    public String getCategorie() {
        return categorie;
    }

    /**
     * Méthode qui change la catégorie du joueur
     *
     * @param categorie catégorie du joueur
     */

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    /**
     * Méthode qui récupère et retourne le niveau du joueur
     *
     * @return estPro
     */

    public boolean getEstPro() {
        return estPro;
    }

    /**
     * Méthode qui change le niveau du joueur
     *
     * @param estPro niveau du joueur
     */

    public void setEstPro(boolean estPro) {
        this.estPro = estPro;
    }

    /**
     * Méthode qui récupère et retourne le poste du joueur
     *
     * @return poste
     */

    public String getPoste() {
        return poste;
    }

    /**
     * Méthode qui change le poste du joueur
     *
     * @param poste poste du joueur
     */

    public void setPoste(String poste) {
        this.poste = poste;
    }

    /**
     * Méthode qui récupère et retourne le numéro du joueur
     *
     * @return numero
     */

    public int getNumero() {
        return numero;
    }

    /**
     * Méthode qui change le numéro du joueur
     *
     * @param numero numéro du joueur
     */

    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * Méthode qui récupère et retourne l'équipe du joueur
     *
     * @return equipe
     */

    public Equipe getEquipe() {
        return equipe;
    }

    /**
     * Méthode qui change l'équipe du joueur
     *
     * @param equipe équipe du joueur
     */

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }

    /**
     * Méthode qui affiche le joueur
     *
     * @return id, prenom, nom, dateNaissance, dateInscription, categorie, estPro, poste, numero, equipe
     */

    @Override
    public String toString() {
        return "Joueur{" +
                "Id='" + id + '\'' +
                ", Prénom='" + prenom + '\'' +
                ", Nom='" + nom + '\'' +
                ", Date de naissance=" + dateNaissance + '\'' +
                ", Date d'inscription=" + dateInscription + '\'' +
                ", Catégorie='" + categorie + '\'' +
                ", Niveau=" + estPro + '\'' +
                ", Poste='" + poste + '\'' +
                ", Numéro=" + numero + '\'' +
                ", Equipe=" + equipe + '\'' +
                '}';
    }
}
