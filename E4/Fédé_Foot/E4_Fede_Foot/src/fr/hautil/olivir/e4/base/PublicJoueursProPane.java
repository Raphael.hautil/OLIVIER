/**
 * Classe: PublicJoueursProPane
 * @author: Raphaël Olivier
 * @version: 1.2
 */

package fr.hautil.olivir.e4.base;

import fr.hautil.olivir.e4.classes.Joueur;
import fr.hautil.olivir.e4.classes.StatsJoueurs;
import fr.hautil.olivir.e4.dao.JoueurDAO;
import fr.hautil.olivir.e4.dao.StatsJoueursDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.sql.Date;
import java.util.ArrayList;

public class PublicJoueursProPane extends GridPane {

    private TableView<Joueur> table = new TableView<>();
    private StatsJoueursDAO stats = new StatsJoueursDAO();
    private StatsJoueurs statsJoueurs = new StatsJoueurs();
    private Text textStats = new Text("");
    private Region photo = new Region();

    /**
     * Méthode qui affiche les statistiques d'un joueur quand on clique dessus.
     */

    private void clickItem(){
        table.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                String id = table.getSelectionModel().getSelectedItem().getId();
                String img = Main.class.getResource("../img/joueurs/" + id + ".jpg").toExternalForm();
                photo.setStyle("-fx-background-image: url('" + img + "');" + "-fx-background-size: 300;");
                statsJoueurs = stats.getStatsByJoueur(id);
                textStats.setText(statsJoueurs.toString());
            }
        });
    }

    /**
     * Création d'une nouvelle page de consultation des joueurs professionnels.
     */

    public PublicJoueursProPane(){
        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        JoueurDAO jDao = new JoueurDAO();

        VBox vBoxStats = new VBox();
        Button btnRet = new Button("Retour");

        photo.setId("photo");

        TableColumn<Joueur, String> id = new TableColumn<>("ID");
        TableColumn<Joueur, String> categorie = new TableColumn<>("Catégorie");
        TableColumn<Joueur, String> poste = new TableColumn<>("Poste");
        TableColumn<Joueur, String> equipe = new TableColumn<>("Equipe");
        TableColumn<Joueur, String> numero = new TableColumn<>("Numéro");
        TableColumn<Joueur, String> nomEntier = new TableColumn<>("Nom entier");
        TableColumn<Joueur, String> prenom = new TableColumn<>("Prénom");
        TableColumn<Joueur, String> nom = new TableColumn<>("Nom");
        TableColumn<Joueur, Date> dateNaissance = new TableColumn<>("Date de naissance");
        TableColumn<Joueur, Date> dateInscription = new TableColumn<>("Date d'inscription");

        nomEntier.getColumns().addAll(prenom,nom);
        table.getColumns().addAll(id,nomEntier,dateNaissance,dateInscription,categorie,equipe,poste,numero);

        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        prenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));
        nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        categorie.setCellValueFactory(new PropertyValueFactory<>("categorie"));
        poste.setCellValueFactory(new PropertyValueFactory<>("poste"));
        equipe.setCellValueFactory(new PropertyValueFactory<>("equipe"));
        numero.setCellValueFactory(new PropertyValueFactory<>("numero"));
        dateNaissance.setCellValueFactory(new PropertyValueFactory<>("dateNaissance"));
        dateInscription.setCellValueFactory(new PropertyValueFactory<>("dateInscription"));

        id.setSortType(TableColumn.SortType.DESCENDING);
        prenom.setSortable(false);

        ArrayList<Joueur> list = jDao.getJoueursPro();
        ObservableList list2 = FXCollections.observableArrayList(list);
        table.setItems(list2);

        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("publicPane"));
            }
        });

        vBoxStats.getChildren().addAll(photo,textStats);

        this.add(table,1,1);
        this.add(btnRet,1,2);
        this.add(vBoxStats,2,1);

        clickItem();
    }
}
