/**
 * Classe: StatsJoueurs
 * @author: Raphaël Olivier
 * @version: 1.1
 */

package fr.hautil.olivir.e4.classes;

import java.sql.Date;

public class StatsJoueurs {
    private String idJ;
    private Date dateArrivee;
    private int nbMatchsJoues;
    private int nbButsMarques;
    private int nbPassesDecisives;
    private int nbCartonsJaune;
    private int nbCartonsRouge;

    /**
     * Méthode qui créé des stats vides.
     */

    public StatsJoueurs(){

    }

    /**
     * Méthode qui créé des stats avec tous ses paramètres.
     *
     * @param idJ id du joueur
     * @param dateArrivee date d'arrivée dans le club actuel du joueur
     * @param nbMatchsJoues nombre total de matchs joués par le joueur dans sa carrière
     * @param nbButsMarques nombre roral de buts marqués par le joueur dans sa carrière
     * @param nbPassesDecisives nombre total de passes décisives effectuées par le joueur dans sa carrière
     * @param nbCartonsJaune nombre total de cartons jaune pris par le joueur dans sa carrière
     * @param nbCartonsRouge nombre total de cartons rouge pris par le joueur dans sa carrière
     */

    public StatsJoueurs(String idJ, Date dateArrivee, int nbMatchsJoues, int nbButsMarques, int nbPassesDecisives, int nbCartonsJaune, int nbCartonsRouge) {
        this.idJ = idJ;
        this.dateArrivee = dateArrivee;
        this.nbMatchsJoues = nbMatchsJoues;
        this.nbButsMarques = nbButsMarques;
        this.nbPassesDecisives = nbPassesDecisives;
        this.nbCartonsJaune = nbCartonsJaune;
        this.nbCartonsRouge = nbCartonsRouge;
    }

    /**
     * Méthode qui récupère et retourne l'id du joueur
     *
     * @return idJ
     */

    public String getIdJ() {
        return idJ;
    }

    /**
     * Méthode qui change l'id du joueur
     *
     * @param idJ id du joueur
     */

    public void setIdJ(String idJ) {
        this.idJ = idJ;
    }

    /**
     * Méthode qui récupère et retourne la date d'arrivée dans son club actuel du joueur
     *
     * @return dateArrivee
     */

    public Date getDateArrivee() {
        return dateArrivee;
    }

    /**
     * Méthode qui change la date d'arrivée du joueur dans son club actuel
     *
     * @param dateArrivee date d'arrivée du joueur
     */

    public void setDateArrivee(Date dateArrivee) {
        this.dateArrivee = dateArrivee;
    }

    /**
     * Méthode qui récupère et retourne le nombre total de matchs joués par un joueur dans sa carrière
     *
     * @return nbMatchsJoues
     */

    public int getNbMatchsJoues() {
        return nbMatchsJoues;
    }

    /**
     * Méthode qui change le nombre total de matchs joués par un joueur dans sa carrière
     *
     * @param nbMatchsJoues nombre total de matchs joués par un joueur
     */

    public void setNbMatchsJoues(int nbMatchsJoues) {
        this.nbMatchsJoues = nbMatchsJoues;
    }

    /**
     * Méthode qui récupère et retourne le nombre total de buts marqués par un joueur dans sa carrière
     *
     * @return nbButsMarques
     */

    public int getNbButsMarques() {
        return nbButsMarques;
    }

    /**
     * Méthode qui change le nombre total de buts marqués par un joueur dans sa carrière
     *
     * @param nbButsMarques nombre total de buts marqués par un joueur dans sa carrière
     */

    public void setNbButsMarques(int nbButsMarques) {
        this.nbButsMarques = nbButsMarques;
    }

    /**
     * Méthode qui récupère et retourne le nombre total de passes décisives effectuées par un joueur dans sa carrière
     *
     * @return nbPassesDecisives
     */

    public int getNbPassesDecisives() {
        return nbPassesDecisives;
    }

    /**
     * Méthode qui change le nombre total de passes décisives effectuées par un joueur dans sa carrière
     *
     * @param nbPassesDecisives nombre total de passes décisives effectuées par un joueur dans sa carrière
     */

    public void setNbPassesDecisives(int nbPassesDecisives) {
        this.nbPassesDecisives = nbPassesDecisives;
    }

    /**
     * Méthode qui récupère et retourne le nombre total de cartons jaune pris par un joueur dans sa carrière
     *
     * @return nbCartonsJaune
     */

    public int getNbCartonsJaune() {
        return nbCartonsJaune;
    }

    /**
     * Méthode qui change le nombre total de cartons jaune pris par un joueur dans sa carrière
     *
     * @param nbCartonsJaune nombre total de cartons jaune pris par un joueur dans sa carrière
     */

    public void setNbCartonsJaune(int nbCartonsJaune) {
        this.nbCartonsJaune = nbCartonsJaune;
    }

    /**
     * Méthode qui récupère et retourne le nombre total de cartons rouge pris par un joueur dans sa carrière
     *
     * @return nbCartonsRouge
     */

    public int getNbCartonsRouge() {
        return nbCartonsRouge;
    }

    /**
     * Méthode qui change le nombre total de cartons rouge pris par un joueur dans sa carrière
     *
     * @param nbCartonsRouge nombre total de cartons rouge pris par un joueur dans sa carrière
     */

    public void setNbCartonsRouge(int nbCartonsRouge) {
        this.nbCartonsRouge = nbCartonsRouge;
    }

    /**
     * Méthode qui affiche toutes les statistiques d'un joueur
     *
     * @return idJ, dateArrivee, nbMatchsJoues, nbButsMarques, nbPassesDecisives, nbCartonsJaune, nbCartonsRouge
     */

    @Override
    public String toString() {
        return "Statistiques du joueur:" + '\n' +
                "Id du joueur: " + idJ + '\n' +
                "Date d'arrivée: " + dateArrivee + '\n' +
                "Nombre de matchs joués: " + nbMatchsJoues + '\n' +
                "Nombre de buts marqués: " + nbButsMarques + '\n' +
                "Nombre de passes décisives effectuées: " + nbPassesDecisives + '\n' +
                "Nombre de cartons jaune pris: " + nbCartonsJaune + '\n' +
                "Nombre de cartons rouge pris: " + nbCartonsRouge;
    }
}