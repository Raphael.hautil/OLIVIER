/**
 * Classe: JoueurDAO
 * @author: Raphaël Olivier
 * @version: 1.3.1
 */

package fr.hautil.olivir.e4.dao;

import fr.hautil.olivir.e4.classes.Equipe;
import fr.hautil.olivir.e4.classes.Joueur;
import java.util.ArrayList;

import java.sql.*;

public class JoueurDAO extends DAO{

    /**
     * Méthode qui retourne tous les joueurs de la vues de données dans une liste
     *
     * @return list
     */

    public ArrayList<Joueur> getJoueurs(){
        ArrayList<Joueur> list = new ArrayList<>();
        Joueur res;
        Equipe eq;
        Connection conn;
        try{
            conn = super.getConnection();
            Statement stmt = conn.createStatement();
            String req = "SELECT * FROM Joueur, Licencié, Equipe WHERE Joueur.idL = Licencié.id AND Joueur.equipe = Equipe.id ORDER BY idL;";
            ResultSet result = stmt.executeQuery(req);
            while (result.next()){
                res = new Joueur();
                eq = new Equipe();
                eq.setId(result.getInt(14));
                eq.setLibelle(result.getString(15));
                eq.setCategorie(result.getString(16));
                eq.setDateCreation(result.getDate(17));
                eq.setClub(result.getInt(18));
                res.setId(result.getString(1));
                res.setCategorie(result.getString(2));
                res.setEstPro(result.getBoolean(3));
                res.setPoste(result.getString(4));
                res.setNumero(result.getInt(6));
                res.setPrenom(result.getString(8));
                res.setNom(result.getString(9));
                res.setDateNaissance(result.getDate(10));
                res.setDateInscription(result.getDate(11));
                res.setEquipe(eq);
                list.add(res);
            }
            result.close();
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Méthode qui retourne tous les joueurs pro dans une liste
     *
     * @return list
     */

    public ArrayList<Joueur> getJoueursPro(){
        Joueur res;
        Equipe eq;
        Connection conn;
        ArrayList<Joueur> list = new ArrayList<>();
        try{
            conn = super.getConnection();
            Statement stmt = conn.createStatement();
            String req = "SELECT * FROM Joueur, Licencié, Equipe WHERE Joueur.equipe = Equipe.id AND Joueur.idL = Licencié.id AND estPro = true;";
            ResultSet result = stmt.executeQuery(req);
            while (result.next()){
                res = new Joueur();
                eq = new Equipe();
                eq.setId(result.getInt(14));
                eq.setLibelle(result.getString(15));
                eq.setCategorie(result.getString(16));
                eq.setDateCreation(result.getDate(17));
                eq.setClub(result.getInt(18));
                res.setId(result.getString(1));
                res.setCategorie(result.getString(2));
                res.setPoste(result.getString(4));
                res.setNumero(result.getInt(6));
                res.setPrenom(result.getString(8));
                res.setNom(result.getString(9));
                res.setDateNaissance(result.getDate(10));
                res.setDateInscription(result.getDate(11));
                res.setEquipe(eq);
                list.add(res);
            }
            result.close();
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Méthode qui renvoie tous les joueurs d'une équipe dans une liste
     *
     * @param equipe équipe des joueurs
     * @return list
     */

    public ArrayList<Joueur> getJoueursByEquipe(Equipe equipe){
        ArrayList<Joueur> list = new ArrayList<>();
        Joueur res;
        Connection conn;
        try {
            conn = super.getConnection();
            String req = "SELECT * FROM Joueur, Licencié WHERE Joueur.idL = Licencié.id AND equipe = ? ORDER BY poste;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1, String.valueOf(equipe.getId()));
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                res = new Joueur();
                res.setId(result.getString(1));
                res.setCategorie(result.getString(2));
                res.setEstPro(result.getBoolean(3));
                res.setPoste(result.getString(4));
                res.setNumero(result.getInt(6));
                res.setPrenom(result.getString(8));
                res.setNom(result.getString(9));
                res.setDateNaissance(result.getDate(10));
                res.setDateInscription(result.getDate(11));
                list.add(res);
            }
            result.close();
            pstmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Méthode qui renvoie toutes les informations d'un joueur en fonction de son id.
     *
     * @param id id du joueur
     * @return j Le joueur demandé
     */

    public Joueur getJoueurById(String id){
        Joueur j = new Joueur();
        Equipe eq = new Equipe();
        Connection conn;
        try{
            conn = super.getConnection();
            String req = "SELECT * FROM Licencié, Joueur, Equipe WHERE Joueur.idL = Licencié.id AND Joueur.equipe = Equipe.id AND idL = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,id);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                eq.setId(result.getInt(14));
                eq.setLibelle(result.getString(15));
                eq.setCategorie(result.getString(16));
                eq.setDateCreation(result.getDate(17));
                eq.setClub(result.getInt(18));
                j.setId(result.getString(1));
                j.setPrenom(result.getString(2));
                j.setNom(result.getString(3));
                j.setDateNaissance(result.getDate(4));
                j.setDateInscription(result.getDate(5));
                j.setPassword(result.getString(7));
                j.setCategorie(result.getString(9));
                j.setPoste(result.getString(11));
                j.setNumero(result.getInt(13));
                j.setEquipe(eq);
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return j;
    }

    /**
     * Méthode qui retourne une liste de joueurs qui n'ont pas d'équipes par catégorie
     *
     * @param categorie catégorie des joueurs
     * @return list
     */

    public ArrayList<Joueur> recherchJoueursLibres(String categorie){
        Joueur j;
        ArrayList<Joueur> list = new ArrayList<>();
        Connection conn;
        try{
            conn = super.getConnection();
            String req = "SELECT * FROM Licencié, Joueur WHERE categorie = ? AND equipe = 0 AND Licencié.id = Joueur.idL;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,categorie);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                j = new Joueur();
                j.setId(result.getString(1));
                j.setPrenom(result.getString(2));
                j.setNom(result.getString(3));
                j.setDateNaissance(result.getDate(4));
                j.setDateInscription(result.getDate(5));
                list.add(j);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }
}