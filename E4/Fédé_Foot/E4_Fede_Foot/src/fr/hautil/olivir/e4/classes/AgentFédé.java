/**
 * Classe: AgentFédé
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package fr.hautil.olivir.e4.classes;

import java.util.Date;

public class AgentFédé extends Licencié {
    private String hierarchie;

    /**
     * Méthode qui créé un agent de fédération vide
     */

    public AgentFédé() {
    }

    /**
     * Méthode qui créé un agent de fédération avec ses attributs
     *
     * @param id              id de l'agent de fédération
     * @param prenom          prénom de l'agent de fédération
     * @param nom             nom de l'agent de fédération
     * @param dateNaissance   date de naissance de l'agent de fédération
     * @param dateInscription date d'inscription de l'agent de fédération
     * @param type            type du licencié
     * @param password        mot de passe de l'agent de fédération
     * @param hierarchie      hiérarchie de l'agent de fédération
     */

    public AgentFédé(String id, String prenom, String nom, Date dateNaissance, Date dateInscription, String type, String password, String hierarchie) {
        super(id, prenom, nom, dateNaissance, dateInscription, type, password);
        this.hierarchie = hierarchie;
    }

    /**
     * Méthode qui récupère et retourne la hiérarchie de l'agent de fédération
     *
     * @return hierarchie
     */

    public String getHierarchie() {
        return hierarchie;
    }

    /**
     * Méthode qui change la hiérarchie de l'agent de fédération
     *
     * @param hierarchie hiérarchie de l'agent de fédération
     */

    public void setHierarchie(String hierarchie) {
        this.hierarchie = hierarchie;
    }

    /**
     * Méthode qui affiche l'agent de fédération
     *
     * @return id, prenom, nom, dateNaissance, dateInscription, hierarchie
     */

    @Override
    public String toString() {
        return "AgentFédé{" +
                "Id='" + id + '\'' +
                ", prenom='" + prenom + '\'' +
                ", nom='" + nom + '\'' +
                ", dateNaissance=" + dateNaissance + '\'' +
                ", dateInscription=" + dateInscription + '\'' +
                ", hierarchie='" + hierarchie + '\'' +
                '}';
    }
}
