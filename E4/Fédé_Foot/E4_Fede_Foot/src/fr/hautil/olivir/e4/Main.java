/**
 * Classe: Main
 * @author: Raphaël Olivier
 * @version: 1.1.2
 */

package fr.hautil.olivir.e4;

import fr.hautil.olivir.e4.vues.*;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import jfxtras.styles.jmetro8.JMetro;

import java.util.Hashtable;

public class Main extends Application {
    private static Scene scene;

    private static Hashtable<String, Pane> vues = new Hashtable<>();

    /**
     * Méthode qui initialise tous les panels dans un hashtable
     */

    private static void initVue(){
        vues.put("loginPane", new LoginPane());
        vues.put("joueurPane", new JoueurPane());
        vues.put("entraineurPane", new EntraineurPane());
        vues.put("agentPane", new AgentPane());
        vues.put("presidentPane", new PresidentPane());
        vues.put("publicPane", new PublicPane());
        vues.put("modifMdpPane", new ModifMdpPane());
        vues.put("modifInfoPane", new ModifInfoPane());
        vues.put("publicJoueursProPane", new PublicJoueursProPane());
        vues.put("publicClubPane", new PublicClubPane());
        vues.put("statsJoueurPane", new StatsJoueurPane());
        vues.put("entraineurModifEquipePane", new EntraineurModifEquipePane());
        vues.put("demandePane", new DemandePane());
        vues.put("gestionDemandePane", new GestionDemandePane());
        vues.put("histoDemandesAgentPane", new HistoDemandesAgentPane());
        vues.put("histoDemandesPresidentPane", new HistoDemandesPresidentPane());
        vues.put("inscriptionPane", new InscriptionPane());
        vues.put("moderationPresidentPane", new ModerationPresidentPane());
    }

    /**
     * Méthode qui récupère un panel en fonction de son nom
     *
     * @param s Nom du panel
     * @return panel
     */

    public static Pane getPanel(String s){
        return vues.get(s);
    }

    /**
     * Méthode qui récupère la scène
     *
     * @return scene
     */

    public static Scene getScene(){
        return scene;
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setMaximized(true);
        initVue();
        scene = new Scene(getPanel("loginPane"), 700, 450);
        primaryStage.setTitle("Fédération de Foot");
        primaryStage.setScene(scene);
        primaryStage.show();
        scene.getStylesheets().add(Main.class.getResource("bootstrap3.css").toExternalForm());
    }

    public static void main(String[] args) {
        launch(args);
    }
}
