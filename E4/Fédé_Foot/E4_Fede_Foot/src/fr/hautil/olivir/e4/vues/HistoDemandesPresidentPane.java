/**
 * Classe: HistoDemandesPredidentPane
 * @author: Raphaël Olivier
 * @version: 1.1
 */

package fr.hautil.olivir.e4.vues;

import fr.hautil.olivir.e4.Main;
import fr.hautil.olivir.e4.classes.Demande;
import fr.hautil.olivir.e4.classes.DemandeTraitee;
import fr.hautil.olivir.e4.classes.PresidentClub;
import fr.hautil.olivir.e4.dao.AgentFédéDAO;
import fr.hautil.olivir.e4.dao.DemandeDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.sql.Date;
import java.util.ArrayList;

public class HistoDemandesPresidentPane extends GridPane {
    private PresidentClub president;
    private TableView<DemandeTraitee> table = new TableView<>();
    private TableView<Demande> table2 = new TableView<>();
    private Label txtInfos = new Label("");
    private Label txtInfos2 = new Label("");
    private VBox vBoxTable1 = new VBox();
    private VBox vBoxTable2 = new VBox();
    private DemandeDAO dDao = new DemandeDAO();

    /**
     * Méthode qui récupère et retourne le président de club connecté
     *
     * @return president
     */

    public PresidentClub getPresident() {
        return president;
    }

    /**
     * Méthode qui change le président de club connecté
     *
     * @param president président de club connecté
     */

    public void setPresident(PresidentClub president) {
        this.president = president;
        init();
    }

    /**
     * Méthode qui affiche les informations de la demande quand on clique dessus
     */

    private void clickItem(){
        table.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                AgentFédéDAO afDao = new AgentFédéDAO();
                DemandeDAO dDao = new DemandeDAO();
                int id = table.getSelectionModel().getSelectedItem().getId();
                DemandeTraitee dt = dDao.getDemandeTraiteeById(String.valueOf(id));
                txtInfos.setText("Demande traitée et " + dt.getStatus().toLowerCase() + "e" +
                        " par " + afDao.getAgentById(dt.getIdA()).getPrenom() +
                        " le " + dt.getDateTraitement() +
                        " du club " + dt.getClub().getLibelle() +
                        " pour créer l'équipe " + dt.getNom() + ".");
            }
        });

        table2.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                DemandeDAO dDao = new DemandeDAO();
                int id = table2.getSelectionModel().getSelectedItem().getId();
                Demande d = dDao.getDemandeById(String.valueOf(id));
                txtInfos2.setText("Demande " + d.getStatus().toLowerCase() +
                        " créée le " + d.getDateCreation() +
                        " pour créer l'équipe " + d.getNom()+".");
            }
        });
    }

    /**
     * Méthode qui initialise la page d'historique des demandes de création d'équipes d'un président de club
     */

    private void init(){
        txtInfos.setText("");
        txtInfos2.setText("");
        ArrayList<DemandeTraitee> list = dDao.getHistoriqueDemandeByPresident(String.valueOf(president.getClub().getId()));
        ObservableList list2 = FXCollections.observableArrayList(list);
        table.setItems(list2);

        ArrayList<Demande> list3 = dDao.getDemandesByPresident(String.valueOf(president.getClub().getId()));
        ObservableList list4 = FXCollections.observableArrayList(list3);
        table2.setItems(list4);

        table.setColumnResizePolicy(new Callback<TableView.ResizeFeatures, Boolean>() {
            @Override
            public Boolean call(TableView.ResizeFeatures param) {
                return true;
            }
        });

        table2.setColumnResizePolicy(new Callback<TableView.ResizeFeatures, Boolean>() {
            @Override
            public Boolean call(TableView.ResizeFeatures param) {
                return true;
            }
        });

        clickItem();
    }

    /**
     * Méthode qui créé la page d'historique des demandes de création d'équipes d'un président de club
     */

    public HistoDemandesPresidentPane(){
        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        Button btnRet = new Button("Retour");

        Label titre1 = new Label("Historique des demandes traitées");
        Label titre2 = new Label("Historique des demandes en cours");

        TableColumn<DemandeTraitee,String> id = new TableColumn<>("ID");
        TableColumn<DemandeTraitee,String> club = new TableColumn<>("Club");
        TableColumn<DemandeTraitee,String> nom = new TableColumn<>("Nom");
        TableColumn<DemandeTraitee,Date> dateTraitement = new TableColumn<>("Date de traitement");
        TableColumn<DemandeTraitee,String> status = new TableColumn<>("Status");

        table.getColumns().addAll(id,club,nom,dateTraitement,status);

        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        club.setCellValueFactory(new PropertyValueFactory<>("club"));
        nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        dateTraitement.setCellValueFactory(new PropertyValueFactory<>("dateTraitement"));
        status.setCellValueFactory(new PropertyValueFactory<>("status"));

        id.setSortType(TableColumn.SortType.DESCENDING);

        TableColumn<Demande,String> id2 = new TableColumn<>("ID");
        TableColumn<Demande,String> club2 = new TableColumn<>("Club");
        TableColumn<Demande,String> nom2 = new TableColumn<>("Nom");
        TableColumn<Demande,String> categorie2 = new TableColumn<>("Catégorie");
        TableColumn<Demande,Date> dateCreation = new TableColumn<>("Date de création");
        TableColumn<Demande,String> status2 = new TableColumn<>("Status");

        table2.getColumns().addAll(id2,club2,nom2,categorie2,dateCreation,status2);

        id2.setCellValueFactory(new PropertyValueFactory<>("id"));
        club2.setCellValueFactory(new PropertyValueFactory<>("club"));
        nom2.setCellValueFactory(new PropertyValueFactory<>("nom"));
        categorie2.setCellValueFactory(new PropertyValueFactory<>("categorie"));
        dateCreation.setCellValueFactory(new PropertyValueFactory<>("dateCreation"));
        status2.setCellValueFactory(new PropertyValueFactory<>("status"));

        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("presidentPane"));
            }
        });

        txtInfos.setMaxWidth(380);
        txtInfos.setWrapText(true);
        txtInfos2.setMaxWidth(380);
        txtInfos2.setWrapText(true);

        vBoxTable1.getChildren().addAll(titre1,table,txtInfos);
        vBoxTable2.getChildren().addAll(titre2,table2,txtInfos2);

        this.add(vBoxTable1,1,1);
        this.add(vBoxTable2,2,1);
        this.add(btnRet, 3,3);
    }
}