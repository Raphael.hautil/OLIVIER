/**
 * Classe: Club
 * @author: Raphaël Olivier
 * @version: 1.1
 */

package fr.hautil.olivir.e4.classes;

import java.util.Date;

public class Club {
    private int id;
    private String libelle;
    private Date dateCreation;

    /**
     * Méthode qui créé un club vide
     */

    public Club() {
    }

    /**
     * Méthode qui créé un club avec ses attributs
     *
     * @param id id du club
     * @param libelle libellé de l'équipe
     * @param dateCreation date de Création de l'équipe
     */

    public Club(int id, String libelle, Date dateCreation) {
        this.id = id;
        this.libelle = libelle;
        this.dateCreation = dateCreation;
    }

    /**
     * Méthode qui récupère et retourne l'id du club
     *
     * @return id
     */

    public int getId() {
        return id;
    }

    /**
     * Méthode qui change l'id du club
     *
     * @param id id du club
     */

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Méthode qui récupère et retourne le libellé du club
     *
     * @return libelle
     */

    public String getLibelle() {
        return libelle;
    }

    /**
     * Méthode qui change le libellé du club
     *
     * @param libelle libellé du club
     */

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * Méthode qui récupère et retourne la date de création du club
     *
     * @return dateCreation
     */

    public Date getDateCreation() {
        return dateCreation;
    }

    /**
     * Méthode qui change la date de création du club
     *
     * @param dateCreation date de création du club
     */

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    /**
     * Méthode qui affiche le club
     *
     * @return libelle
     */

    @Override
    public String toString() {
        return libelle;
    }
}
