/**
 * Classe: PublicPane
 * @author: Raphaël Olivier
 * @version: 1.1
 */

package fr.hautil.olivir.e4.vues;

import fr.hautil.olivir.e4.Main;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

public class PublicPane extends GridPane {

    /**
     * Création d'une page publique
     */

    public PublicPane(){

        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        Button btnJoueurs = new Button("Voir les joueurs professionnels");
        Button btnClub = new Button("Voir les clubs");
        Button btnRet = new Button("Retour");

        this.add(btnJoueurs,1,1);
        this.add(btnClub,2,1);
        this.add(btnRet,1,2);

        btnJoueurs.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("publicJoueursProPane"));
            }
        });

        btnClub.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("publicClubPane"));
            }
        });

        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("loginPane"));
            }
        });
    }
}
