/**
 * Classe: AgentPane
 * @author: Raphaël Olivier
 * @version: 1.2
 */

package fr.hautil.olivir.e4.base;

import fr.hautil.olivir.e4.classes.AgentFédé;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;


public class AgentPane extends GridPane {

    private AgentFédé agent = null;
    private Text idAgent = new Text("");
    private Text prenomAgent = new Text("");
    private Text nomAgent = new Text("");
    private Text dateNaissanceAgent = new Text("");
    private Text dateInscriptionAgent = new Text("");
    private Text hierarchieAgent = new Text("");
    private Text passwordAgent = new Text("");
    private Text titre = new Text("");

    /**
     * Méthode qui récupère et retourne l'agent de la fédération connecté à la page
     *
     * @return agent
     */

    public AgentFédé getAgent() {
        return agent;
    }

    /**
     * Méthode qui change l'agent de la fédération connecté à la page
     *
     * @param agent agent de la fédération
     */

    public void setAgent(AgentFédé agent) {
        this.agent = agent;
        init();
    }

    /**
     * Méthode qui initialise la page d'agent de la fédération
     */

    private void init(){
        titre.setText("Bienvenue sur votre page d'agent de la fédération !");
        idAgent.setText("ID: " + agent.getId());
        prenomAgent.setText("Prénom: " + agent.getPrenom());
        nomAgent.setText("Nom: " + agent.getNom());
        dateNaissanceAgent.setText("Date de naisssance: " + agent.getDateNaissance());
        dateInscriptionAgent.setText("Date d'inscription: " + agent.getDateInscription());
        hierarchieAgent.setText("Hiérarchie: " + agent.getHierarchie());
        passwordAgent.setText("Mot de passe: " + agent.getPassword());

        Button btnDemandes = new Button("Voir les demandes");
        Button btnDeco = new Button("Déconnexion");
        VBox vBoxInfos = new VBox();

        btnDemandes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                GestionDemandePane gestionDemandePane = (GestionDemandePane) Main.getPanel("gestionDemandePane");
                gestionDemandePane.setAgentFédé(agent);
                Main.getScene().setRoot(gestionDemandePane);
            }
        });

        btnDeco.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                LoginPane.deco();
            }
        });

        vBoxInfos.getChildren().addAll(idAgent,prenomAgent,nomAgent,dateNaissanceAgent,dateInscriptionAgent,hierarchieAgent,passwordAgent);

        this.add(vBoxInfos,1,1);
        this.add(btnDemandes,1,2);
        this.add(btnDeco, 2,2);
    }

    /**
     * Création d'une page d'agent de la fédération.
     */

    public AgentPane(){
        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        titre.setText("");

        this.add(titre,2,0);
    }
}
