/**
 * Classe: LoginPane
 * @author: Raphaël Olivier
 * @version: 1.2
 */

package fr.hautil.olivir.e4.base;

import fr.hautil.olivir.e4.classes.*;
import fr.hautil.olivir.e4.dao.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import static javafx.geometry.HPos.RIGHT;


public class LoginPane extends GridPane {

    /**
     * Méthode permettant de se déconnecter.
     */

    public static void deco(){
        Main.getScene().setRoot(Main.getPanel("loginPane"));
        idUtile = null;
        mdpUtile = null;
        equipeUtile = 0;
        userTextField.setText("");
        pwBox.setText("");
    }

    public static String idUtile;
    public static String mdpUtile;
    public static int equipeUtile;
    private static PasswordField pwBox = new PasswordField();
    private static TextField userTextField = new TextField();

    /**
     * Création d'une page de connexion
     */

    public LoginPane(){
        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25,25,25,25));

        DAO dao = new DAO();

        Text scenetitle = new Text("Connexion");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        this.add(scenetitle, 0, 0, 2, 1);

        Label userName = new Label("Login:");
        Label pw = new Label("Mot de passe:");

        this.add(userName, 0, 1);
        this.add(userTextField, 1, 1);
        this.add(pw, 0, 2);
        this.add(pwBox, 1, 2);

        Button btn = new Button("Se connecter");
        Button btnPublic = new Button("Public");
        Button btnQuit = new Button("Fermer");

        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(btnPublic);
        hbBtn.getChildren().add(btn);
        this.add(hbBtn, 1, 4);
        this.add(btnQuit,7,7);

        final Text actiontarget = new Text();
        this.add(actiontarget, 0, 6);
        this.setColumnSpan(actiontarget, 2);
        this.setHalignment(actiontarget, RIGHT);
        actiontarget.setId("actiontarget");

        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String login = userTextField.getText();
                String pwd = pwBox.getText();
                Licencié test = dao.login(login, pwd);
                if (test.getType().equals("Joueur")){
                    JoueurDAO jDao = new JoueurDAO();
                    Joueur j = jDao.getJoueurById(login);
                    JoueurPane jPane = (JoueurPane) Main.getPanel("joueurPane");
                    jPane.setJoueur(j);
                    Main.getScene().setRoot(jPane);
                }
                if (test.getType().equals("Entraîneur")){
                    EntraineurDAO eDao = new EntraineurDAO();
                    Entraineur e = eDao.getEntraineurById(login);
                    EntraineurPane ePane = (EntraineurPane) Main.getPanel("entraineurPane");
                    ePane.setEntraineur(e);
                    Main.getScene().setRoot(ePane);
                }
                if (test.getType().equals("Président de Club")){
                    PrésidentClubDAO pCDAO = new PrésidentClubDAO();
                    PresidentClub pc = pCDAO.getPresidentById(login);
                    PresidentPane pPane = (PresidentPane) Main.getPanel("presidentPane");
                    pPane.setPresidentClub(pc);
                    Main.getScene().setRoot(pPane);
                }
                if (test.getType().equals("Agent de Fédération")){
                    AgentFédéDAO afDao = new AgentFédéDAO();
                    AgentFédé af = afDao.getAgentById(login);
                    AgentPane aPane = (AgentPane) Main.getPanel("agentPane");
                    aPane.setAgent(af);
                    Main.getScene().setRoot(aPane);
                }
            }
        });

        btnPublic.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("publicPane"));
            }
        });

        btnQuit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.exit(0);
            }
        });
    }
}
