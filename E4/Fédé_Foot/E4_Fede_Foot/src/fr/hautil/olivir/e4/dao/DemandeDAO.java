/**
 * Classe: DemandeDAO
 * @author: Raphaël Olivier
 * @version: 1.4
 */

package fr.hautil.olivir.e4.dao;

import fr.hautil.olivir.e4.classes.Club;
import fr.hautil.olivir.e4.classes.Demande;
import fr.hautil.olivir.e4.classes.DemandeTraitee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DemandeDAO extends DAO {

    /**
     * Méthode qui récupère toutes les demandes de création d'équipe.
     *
     * @return list Liste des demandes
     */

    public ArrayList<Demande> getDemandes(){
        ArrayList<Demande> list = new ArrayList<>();
        Demande d;
        Club c;
        Connection conn;
        try{
            conn = super.getConnection();
            Statement stmt = conn.createStatement();
            String req = "SELECT * FROM Demande, Club WHERE Demande.idC = Club.id AND status = 'En Cours';";
            ResultSet result = stmt.executeQuery(req);
            while (result.next()){
                d = new Demande();
                c = new Club();
                d.setId(result.getInt(1));
                d.setNom(result.getString(3));
                d.setCategorie(result.getString(4));
                d.setDateCreation(result.getDate(5));
                d.setStatus(result.getString(6));
                c.setId(result.getInt(7));
                c.setLibelle(result.getString(8));
                c.setDateCreation(result.getDate(9));
                d.setClub(c);
                list.add(d);
            }
            stmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Méthode qui récupère toutes les demandes traitées.
     *
     * @return list Liste des demandes traitées
     */

    public ArrayList<DemandeTraitee> getDemandesTraitees(){
        ArrayList<DemandeTraitee> list = new ArrayList<>();
        DemandeTraitee dt;
        Connection conn;
        try{
            conn = super.getConnection();
            Statement stmt = conn.createStatement();
            String req = "SELECT * FROM Demande, DemandeTraitee, Club WHERE Demande.id = DemandeTraitee.idD AND Demande.idC = Club.id;";
            ResultSet result = stmt.executeQuery(req);
            while (result.next()){
                dt = new DemandeTraitee();
                dt.setId(result.getInt(1));
                dt.setNom(result.getString(3));
                dt.setDateCreation(result.getDate(4));
                dt.setStatus(result.getString(5));
                dt.setDateTraitement(result.getDate(7));
                dt.setIdA(result.getString(6));
                list.add(dt);
            }
            stmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    public DemandeTraitee getDemandeTraiteeById(String id){
        DemandeTraitee dt = new DemandeTraitee();
        Club c;
        Connection conn;
        try {
            conn = super.getConnection();
            String req = "SELECT * FROM Demande, DemandeTraitee, Club WHERE Demande.id = DemandeTraitee.idD AND Club.id = Demande.idC AND idD = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,id);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                dt = new DemandeTraitee();
                c = new Club();
                dt.setId(result.getInt(1));
                dt.setNom(result.getString(3));
                dt.setCategorie(result.getString(4));
                dt.setDateCreation(result.getDate(5));
                dt.setStatus(result.getString(6));
                dt.setIdA(result.getString(8));
                dt.setDateTraitement(result.getDate(9));
                c.setId(result.getInt(11));
                c.setLibelle(result.getString(12));
                c.setDateCreation(result.getDate(13));
                dt.setClub(c);
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return dt;
    }

    /**
     * Méthode qui récupère toutes les demandes faites par un président de club
     *
     * @param id id du président de club
     * @return list Liste des demandes faites par le président de club
     */

    public ArrayList<DemandeTraitee> getHistoriqueDemandeByPresident(String id){
        ArrayList<DemandeTraitee> list = new ArrayList<>();
        DemandeTraitee dt;
        Club c;
        Connection conn;
        try{
            conn = super.getConnection();
            String req = "SELECT * FROM Demande, DemandeTraitee, Club WHERE Demande.id = DemandeTraitee.idD AND Club.id = Demande.idC AND Demande.idC = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,id);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                dt = new DemandeTraitee();
                c = new Club();
                dt.setId(result.getInt(1));
                dt.setNom(result.getString(3));
                dt.setCategorie(result.getString(4));
                dt.setDateCreation(result.getDate(5));
                dt.setStatus(result.getString(6));
                dt.setIdA(result.getString(8));
                dt.setDateTraitement(result.getDate(9));
                c.setId(result.getInt(11));
                c.setLibelle(result.getString(12));
                c.setDateCreation(result.getDate(13));
                dt.setClub(c);
                list.add(dt);
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Méthode qui récupère toutes les demandes traitées par un agent spécifique
     *
     * @param id id de l'agent de la fédération
     * @return list Liste des demandes traitées par l'agent
     */

    public ArrayList<DemandeTraitee> getHistoriqueDemandeByAgent(String id){
        ArrayList<DemandeTraitee> list = new ArrayList<>();
        DemandeTraitee dt;
        Club c;
        Connection conn;
        try {
            conn = super.getConnection();
            String req = "SELECT * FROM Demande, DemandeTraitee, Club WHERE Demande.id = DemandeTraitee.idD AND idC = Club.id AND idA = ? ORDER BY Demande.id;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1, id);
            ResultSet result = pstmt.executeQuery();
            while (result.next()) {
                dt = new DemandeTraitee();
                c = new Club();
                dt.setId(result.getInt(1));
                dt.setNom(result.getString(3));
                dt.setCategorie(result.getString(4));
                dt.setDateCreation(result.getDate(5));
                dt.setStatus(result.getString(6));
                dt.setDateTraitement(result.getDate(9));
                c.setId(result.getInt(11));
                c.setLibelle(result.getString(12));
                c.setDateCreation(result.getDate(13));
                dt.setClub(c);
                list.add(dt);
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Méthode qui retourne une demande en fonction de son id
     *
     * @param id id de la demande
     * @return d
     */

    public Demande getDemandeById(String id){
        Connection conn;
        Club c;
        Demande d = new Demande();
        try{
            conn = super.getConnection();
            String req = "SELECT * FROM Demande, Club WHERE Demande.id = ? AND Demande.idC = Club.id";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,id);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                c = new Club();
                d.setId(result.getInt(1));
                d.setNom(result.getString(3));
                d.setCategorie(result.getString(4));
                d.setDateCreation(result.getDate(5));
                d.setStatus(result.getString(6));
                c.setId(result.getInt(7));
                c.setLibelle(result.getString(8));
                c.setDateCreation(result.getDate(9));
                d.setClub(c);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return d;
    }

    /**
     * Méthode qui récupère le dernier id enregistré des demandes
     *
     * @return id
     */

    private int getLastId(){
        Connection conn;
        int id = 0;
        try{
            conn = super.getConnection();
            Statement stmt = conn.createStatement();
            String req = "SELECT id FROM Demande ORDER BY id DESC LIMIT 0,1";
            ResultSet result = stmt.executeQuery(req);
            while (result.next()){
                id = result.getInt(1);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return id;
    }

    /**
     * Méthode qui créé une demande de création d'équipe
     *
     * @param idClub id du club
     * @param nom nom de la nouvelle équipe
     * @param categorie catégorie de la nouvelle équipe
     */

    public void createDemande(String idClub, String nom, String categorie){
        Connection conn;
        String format = "yyyy-MM-dd";
        try{
            conn = super.getConnection();
            SimpleDateFormat formater = new SimpleDateFormat(format);
            Date date = new Date();
            String req = "INSERT INTO Demande VALUES (?, ?, ?, ?, ?, ?);";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,String.valueOf(getLastId() + 1));
            pstmt.setString(2,idClub);
            pstmt.setString(3,nom);
            pstmt.setString(4,categorie);
            pstmt.setString(5,formater.format(date));
            pstmt.setString(6,"En Cours");
            pstmt.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Méthode qui accepte une demande de création d'équipe
     *
     * @param idD id de la demande
     * @param idA id de l'agent de la fédération qui traite la demande
     */

    public void accepterDemande(String idD, String idA){
        Connection conn;
        EquipeDAO eDao = new EquipeDAO();
        String format = "yyyy-MM-dd";
        try{
            conn = super.getConnection();
            SimpleDateFormat formater = new SimpleDateFormat(format);
            Date date = new Date();
            String req = "INSERT INTO DemandeTraitee VALUES (?,?,?,?)";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,idD);
            pstmt.setString(2,idA);
            pstmt.setString(3,formater.format(date));
            pstmt.setString(4,"Accepté");
            pstmt.executeUpdate();
            Demande d = getDemandeById(idD);
            eDao.createTeam(d.getNom(), d.getCategorie(), String.valueOf(d.getClub().getId()));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Méthode qui refuse une demande de création d'équipe
     *
     * @param idD id de la demande
     * @param idA id de l'agent de la fédération qui traite la demande
     */

    public void refuserDemande(String idD, String idA){
        Connection conn;
        String format = "yyyy-MM-dd";
        try{
            conn = super.getConnection();
            SimpleDateFormat formater = new SimpleDateFormat(format);
            Date date = new Date();
            String req = "INSERT INTO DemandeTraitee VALUES (?,?,?,?)";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,idD);
            pstmt.setString(2,idA);
            pstmt.setString(3,formater.format(date));
            pstmt.setString(4,"Refusé");
            pstmt.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Méthode qui récupère toutes les demandes en cours d'un président
     *
     * @param id id du président
     * @return list
     */

    public ArrayList<Demande> getDemandesByPresident(String id){
        ArrayList<Demande> list = new ArrayList<>();
        Demande d;
        Club c;
        Connection conn;
        try{
            conn = super.getConnection();
            String req = "SELECT * FROM Demande, Club WHERE Demande.idC = Club.id AND Club.id = ? AND status = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,id);
            pstmt.setString(2,"En Cours");
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                d = new Demande();
                c = new Club();
                d.setId(result.getInt(1));
                d.setNom(result.getString(3));
                d.setCategorie(result.getString(4));
                d.setDateCreation(result.getDate(5));
                c.setId(result.getInt(7));
                d.setStatus(result.getString(6));
                c.setLibelle(result.getString(8));
                c.setDateCreation(result.getDate(9));
                d.setClub(c);
                list.add(d);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return list;
    }
}