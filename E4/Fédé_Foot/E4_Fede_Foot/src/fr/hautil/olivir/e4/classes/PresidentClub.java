/**
 * Classe: Main
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package fr.hautil.olivir.e4.classes;

import java.util.Date;

public class PresidentClub extends Licencié{
    private Club club;

    /**
     * Méthode qui créé un président de club vide
     */

    public PresidentClub() {
    }

    /**
     * Méthode qui créé un président de club avec ses attributs
     *
     * @param id id du président du club
     * @param prenom prénom du président du club
     * @param nom nom du président du club
     * @param dateNaissance date de naissance du président du club
     * @param dateInscription date d'inscription du président du club
     * @param type type du licencié
     * @param password mot de passe du président du club
     * @param club club du président du club
     */

    public PresidentClub(String id, String prenom, String nom, Date dateNaissance, Date dateInscription, String type, String password, Club club){
        super(id, prenom, nom, dateNaissance, dateInscription, type, password);
        this.club = club;
    }

    /**
     * Méthode qui récupère et retourne le club du président du club
     *
     * @return club
     */

    public Club getClub() {
        return club;
    }

    /**
     * Méthode qui change le club du président du club
     *
     * @param club club du président du club
     */

    public void setClub(Club club) {
        this.club = club;
    }

    /**
     * Méthode qui affiche le président du club
     *
     * @return id, prenom, nom, dateNaissance, dateInscription, club
     */

    @Override
    public String toString() {
        return "PresidentClub{" +
                "Id='" + id + '\'' +
                ", Prénom='" + prenom + '\'' +
                ", Nom='" + nom + '\'' +
                ", Date de naissance=" + dateNaissance + '\'' +
                ", Date d'inscription=" + dateInscription + '\'' +
                ", Club=" + club + '\'' +
                '}';
    }
}
