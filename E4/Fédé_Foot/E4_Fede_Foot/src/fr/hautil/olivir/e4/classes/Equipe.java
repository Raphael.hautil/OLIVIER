/**
 * Classe: Equipe
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package fr.hautil.olivir.e4.classes;

import java.util.Date;

public class Equipe{
    private int id;
    private String libelle;
    private String categorie;
    private Date dateCreation;
    private int club;

    /**
     * Méthode qui créé une équipe vide
     */

    public Equipe() {
    }

    /**
     * Méthode qui créé une équipe avec tous ses attributs
     *
     * @param id id de l'équipe
     * @param libelle libellé de l'équipe
     * @param categorie catégorie de l'équipe
     * @param dateCreation date de création de l'équipe
     * @param club club de l'équipe
     */

    public Equipe(int id, String libelle, String categorie, Date dateCreation, int club) {
        this.id = id;
        this.libelle = libelle;
        this.categorie = categorie;
        this.dateCreation = dateCreation;
        this.club = club;
    }

    /**
     * Méthode qui récupère et retourne l'id de l'équipe
     *
     * @return id
     */

    public int getId() {
        return id;
    }

    /**
     * Méthode qui change l'id de l'équipe
     *
     * @param id id de l'équipe
     */

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Méthode qui récupère et retourne le libellé de l'équipe
     *
     * @return libelle
     */

    public String getLibelle() {
        return libelle;
    }

    /**
     * Méthode qui change le libellé de l'équipe
     *
     * @param libelle libellé de l'équipe
     */

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * Méthode qui récupère et retourne la catégorie de l'équipe
     *
     * @return categorie
     */

    public String getCategorie() {
        return categorie;
    }

    /**
     * Méthode qui change la catégorie de l'équipe
     *
     * @param categorie catégorie de l'équipe
     */

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    /**
     * Méthode qui récupère et retourne la date de création de l'équipe
     *
     * @return dateCreation
     */

    public Date getDateCreation() {
        return dateCreation;
    }

    /**
     * Méthode qui change la date de création de l'équipe
     *
     * @param dateCreation date de création de l'équipe
     */

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    /**
     * Méthode qui récupère et retourne le club de l'équipe
     *
     * @return club
     */

    public int getClub(){
        return club;
    }

    /**
     * Méthode qui change le club d'une équipe
     *
     * @param club club de l'équipe
     */

    public void setClub(int club) {
        this.club = club;
    }

    /**
     * Méthode qui affiche l'équipe
     *
     * @return id, libelle, categorie, dateCreation, club
     */

    @Override
    public String toString() {
        return libelle;
    }
}