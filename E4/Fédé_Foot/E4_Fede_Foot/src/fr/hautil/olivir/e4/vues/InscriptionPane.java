/**
 * Classe: InscriptionPane
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package fr.hautil.olivir.e4.vues;

import fr.hautil.olivir.e4.Main;
import fr.hautil.olivir.e4.dao.DAO;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class InscriptionPane extends GridPane {

    /**
     * Méthode qui créé la page d'inscription
     */

    public InscriptionPane(){
        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        Button btnRet = new Button("Retour");
        Button btnVal = new Button("Valider");

        VBox vBoxText = new VBox();
        VBox vBoxField = new VBox();

        Text txtResult = new Text("");

        Text txtPrenom = new Text("Prénom");
        Text txtNom = new Text("Nom");
        Text txtDateNaissance = new Text("Date de naissance");
        Text txtMdp1 = new Text("Mot de passe");
        Text txtMdp2 = new Text("Confirmez votre mot de passe");
        Text txtType = new Text("Type");

        vBoxText.getChildren().addAll(txtPrenom,txtNom,txtDateNaissance,txtMdp1,txtMdp2,txtType);

        TextField textPrenom = new TextField("");
        TextField textNom = new TextField("");
        DatePicker textDateNaissance = new DatePicker();
        TextField textMdp1 = new TextField("");
        TextField textMdp2 = new TextField("");
        ComboBox type = new ComboBox();

        vBoxField.getChildren().addAll(textPrenom,textNom,textDateNaissance,textMdp1,textMdp2,type);

        type.getItems().setAll("Joueur", "Entraîneur");

        btnVal.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DAO dao = new DAO();
                if (textMdp1.getText().equals(textMdp2.getText())){
                    dao.insciption(textPrenom.getText(),textNom.getText(),String.valueOf(textDateNaissance.getValue()),textMdp1.getText(),type.getSelectionModel().getSelectedItem().toString());
                    txtResult.setText("Inscription réussie, vous pouvez vous connecter en utilisant l'identifiant suivant : " + (textPrenom.getText().substring(0,3) + textNom.getText().substring(0,2)).toUpperCase());
                }else{
                    txtResult.setText("Les deux mots de passe ne correspondent pas.");
                }
            }
        });

        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("loginPane"));
            }
        });

        this.add(vBoxText,1,1);
        this.add(vBoxField,2,1);
        this.add(txtResult,0,2);
        this.add(btnRet,3,3);
        this.add(btnVal,2,3);
    }
}
