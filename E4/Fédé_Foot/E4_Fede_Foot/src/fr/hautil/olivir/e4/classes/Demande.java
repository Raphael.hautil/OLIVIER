/**
 * Classe: Demande
 * @author: Raphaël Olivier
 * @version: 1.2
 */

package fr.hautil.olivir.e4.classes;

import java.util.Date;

public class Demande {
    protected int id;
    protected Club club;
    protected String nom;
    protected String categorie;
    protected Date dateCreation;
    protected String status;

    /**
     * Méthode qui créé une demande vide
     */

    public Demande() {
    }

    /**
     * Méthode qui créé une demande avec ses attributs
     *
     * @param id id de la demande
     * @param club club de la demande
     * @param nom nom de l'équipe de la demande
     * @param categorie catégorie de l'équipe de la demande
     * @param dateCreation date de création de la demande
     * @param status status de la demande
     */

    public Demande(int id, Club club, String nom, String categorie, Date dateCreation, String status) {
        this.id = id;
        this.club = club;
        this.nom = nom;
        this.categorie = categorie;
        this.dateCreation = dateCreation;
        this.status = status;
    }

    /**
     * Méthode qui récupère et renvoie l'id de la demande
     *
     * @return id
     */

    public int getId() {
        return id;
    }

    /**
     * Méthode qui change l'id de la demande
     *
     * @param id id de la demande
     */

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Méthode qui récupère et renvoie le club
     *
     * @return club
     */

    public Club getClub() {
        return club;
    }

    /**
     * Méthode qui change le club
     *
     * @param club club de la demande
     */

    public void setClub(Club club) {
        this.club = club;
    }

    /**
     * Méthode qui récupère et retourne le nom du club demandé
     *
     * @return nom
     */

    public String getNom() {
        return nom;
    }

    /**
     * Méthode qui change le nom du club demandé
     *
     * @param nom nom du club demandé
     */

    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Méthode qui récupère et retourne la catégorie de l'équipe de la demande
     *
     * @return categorie
     */

    public String getCategorie() {
        return categorie;
    }

    /**
     * Méthode qui change la catégorie de l'équipe de la demande
     *
     * @param categorie catégorie de l'équipe de la demande
     */

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    /**
     * Méthode qui récupère et retourne la date de création de la demande
     *
     * @return dateCreation
     */

    public Date getDateCreation() {
        return dateCreation;
    }

    /**
     * Méthode qui change la date de création de la demande
     *
     * @param dateCreation date de création de la demande
     */

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    /**
     * Méthode qui récupère et retourne le status de la demande
     *
     * @return status
     */

    public String getStatus() {
        return status;
    }

    /**
     * Méthode qui change le status de la demande
     *
     * @param status status de la demande
     */

    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Méthode qui affiche la demande avec tous ses attributs
     *
     * @return id, idP, nom, dateCreation, status
     */

    @Override
    public String toString() {
        return "Demande{" +
                "Id=" + id +
                ", club='" + club + '\'' +
                ", nom='" + nom + '\'' +
                ", catégorie= " + categorie +
                ", date=" + dateCreation +
                ", status='" + status +
                '}';
    }
}