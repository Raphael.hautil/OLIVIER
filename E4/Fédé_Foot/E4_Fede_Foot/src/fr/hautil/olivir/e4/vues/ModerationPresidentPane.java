/**
 * Classe: ModerationPresidentPane
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package fr.hautil.olivir.e4.vues;

import fr.hautil.olivir.e4.Main;
import fr.hautil.olivir.e4.classes.Equipe;
import fr.hautil.olivir.e4.classes.Joueur;
import fr.hautil.olivir.e4.classes.JoueurCircle;
import fr.hautil.olivir.e4.classes.PresidentClub;
import fr.hautil.olivir.e4.dao.EquipeDAO;
import fr.hautil.olivir.e4.dao.JoueurDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Text;
import javafx.scene.image.Image;
import javafx.util.Callback;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

public class ModerationPresidentPane extends GridPane{
    private PresidentClub president = null;
    private JoueurDAO jDao = new JoueurDAO();
    private EquipeDAO eDao = new EquipeDAO();
    private Equipe equipe = null;
    private JoueurCircle c1 = new JoueurCircle(40);
    private JoueurCircle c2 = new JoueurCircle(40);
    private JoueurCircle c3 = new JoueurCircle(40);
    private JoueurCircle c4 = new JoueurCircle(40);
    private JoueurCircle c5 = new JoueurCircle(40);
    private JoueurCircle c6 = new JoueurCircle(40);
    private JoueurCircle c7 = new JoueurCircle(40);
    private JoueurCircle c8 = new JoueurCircle(40);
    private JoueurCircle c9 = new JoueurCircle(40);
    private JoueurCircle c10 = new JoueurCircle(40);
    private JoueurCircle c11 = new JoueurCircle(40);
    private JoueurCircle cSelect = new JoueurCircle(40);

    private Text titre = new Text("Bienvenue sur votre page de modération d'équipes");
    private AnchorPane terrain = new AnchorPane();
    private Button btnRet = new Button("Retour");
    private TableView<Joueur> table = new TableView<>();

    /**
     * Méthode qui récupère et retourne le président de club connecté
     *
     * @return president
     */

    public PresidentClub getPresident() {
        return president;
    }

    /**
     * Méthode qui change le président de club connecté
     *
     * @param president président de club connecté
     */

    public void setPresident(PresidentClub president) {
        this.president = president;
        init();
    }

    /**
     * Méthode qui permet de remplir le tableau des joueurs disponibles
     *
     * @param id id de l'équipe
     */

    private void fillTable(String id){
        EquipeDAO eDao = new EquipeDAO();
        ArrayList<Joueur> listJoueurs;
        listJoueurs = jDao.recherchJoueursLibres(eDao.getEquipeById(id).getCategorie());
        ObservableList list2 = FXCollections.observableArrayList(listJoueurs);
        table.setItems(list2);
        table.setColumnResizePolicy(new Callback<TableView.ResizeFeatures, Boolean>() {
            @Override
            public Boolean call(TableView.ResizeFeatures param) {
                return true;
            }
        });
    }

    private void updateImg(JoueurCircle c, Joueur j){
        File f = new File("C:\\Users\\rapho\\IdeaProjects\\E4_Fede_Foot\\src\\fr\\hautil\\olivir\\e4\\img\\joueurs\\" + j.getId() + ".jpg");
        if (f.exists()){
            String img = Main.class.getResource("img/joueurs/" + jDao.getJoueurById(j.getId()) + ".png").toExternalForm();
            Image image = new Image(img);
            c.setFill(new ImagePattern(image));
        }else{
            String img = Main.class.getResource("img/joueurs/inconnu.jpg").toExternalForm();
            Image image = new Image(img);
            c.setFill(new ImagePattern(image));
        }
    }

    /**
     * Méthode qui ajoute les boutons pour ajouter le joueur à l'équipe au tableau
     */

    private void addButtonToTable(){
        TableColumn<Joueur,Void> add = new TableColumn<>("Ajouter ?");

        Callback<TableColumn<Joueur,Void>, TableCell<Joueur,Void>> cellFactory = new Callback<TableColumn<Joueur, Void>, TableCell<Joueur, Void>>() {
            @Override
            public TableCell<Joueur, Void> call(TableColumn<Joueur, Void> param) {
                final TableCell<Joueur,Void> cell = new TableCell<Joueur,Void>(){
                    private final Button add = new Button("Ajouter");
                    {
                        add.setOnAction((ActionEvent event) -> {
                            eDao.addJoueurToTeam(getTableView().getItems().get(getIndex()),equipe);
                            updateImg(cSelect, jDao.getJoueurById(getTableView().getItems().get(getIndex()).getId()));
                            cSelect = null;
                        });
                    }

                    public void updateItem(Void item, boolean empty){
                        super.updateItem(item,empty);
                        if (empty){
                            setGraphic(null);
                        }else{
                            setGraphic(add);
                        }
                    }
                };
                return cell;
            }
        };

        add.setCellFactory(cellFactory);
        table.getColumns().add(add);
    }

    /**
     * Méthode qui initialise la page de modération des équipes d'un président de club
     */

    private void init(){
        titre.setText("Bienvenue sur votre page de modération d'équipes");
        titre.setId("title");
        this.setAlignment(Pos.CENTER);
        terrain.setVisible(false);

        EquipeDAO eDao = new EquipeDAO();
        HBox vBoxBtn = new HBox();

        ArrayList<Equipe> listEquipes = eDao.getEquipesByClub(String.valueOf(president.getClub().getId()));

        for (int i = 0; i<listEquipes.size(); i++){
            Button btnEq = new Button(listEquipes.get(i).toString());
            btnEq.setId(String.valueOf(listEquipes.get(i).getId()));
            vBoxBtn.getChildren().add(btnEq);

            btnEq.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    String id = btnEq.getId();
                    equipe = eDao.getEquipeById(id);
                    titre.setText("Modération de l'équipe " + eDao.getEquipeById(id));
                    vBoxBtn.setVisible(false);
                    terrain.setVisible(true);
                    ModerationPresidentPane.super.setAlignment(Pos.TOP_CENTER);
                    ModerationPresidentPane.super.add(terrain,1,2);
                    ModerationPresidentPane.super.add(table,2,2);

                    if (jDao.getJoueursByEquipe(eDao.getEquipeById(id)).size() == 0){
                        String add = Main.class.getResource("img/joueurs/add.png").toExternalForm();
                        Image imgAdd = new Image(add);

                        //ATTAQUANTS

                        c1.setFill(new ImagePattern(imgAdd));
                        c1.setLayoutX(500);
                        c1.setLayoutY(70);
                        Joueur j = new Joueur();
                        j.setPoste("Attaquant");
                        c1.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                cSelect = c1;
                                fillTable(String.valueOf(eDao.getEquipeById(id).getId()));
                            }
                        });

                        c2.setFill(new ImagePattern(imgAdd));
                        c2.setLayoutX(550);
                        c2.setLayoutY(220);
                        Joueur j2 = new Joueur();
                        j2.setPoste("Attaquant");
                        c2.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                cSelect = c2;
                                fillTable(String.valueOf(eDao.getEquipeById(id).getId()));
                            }
                        });

                        c3.setFill(new ImagePattern(imgAdd));
                        c3.setLayoutX(500);
                        c3.setLayoutY(357);
                        Joueur j3 = new Joueur();
                        j3.setPoste("Attaquant");
                        c3.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                cSelect = c3;
                                fillTable(String.valueOf(eDao.getEquipeById(id).getId()));
                            }
                        });

                        //DEFENSEURS

                        c4.setFill(new ImagePattern(imgAdd));
                        c4.setLayoutX(185);
                        c4.setLayoutY(70);
                        Joueur j4 = new Joueur();
                        j4.setPoste("Défenseur");
                        c4.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                cSelect = c4;
                                fillTable(String.valueOf(eDao.getEquipeById(id).getId()));
                            }
                        });

                        c5.setFill(new ImagePattern(imgAdd));
                        c5.setLayoutX(150);
                        c5.setLayoutY(160);
                        Joueur j5 = new Joueur();
                        j5.setPoste("Défenseur");
                        c5.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                cSelect = c5;
                                fillTable(String.valueOf(eDao.getEquipeById(id).getId()));
                            }
                        });

                        c6.setFill(new ImagePattern(imgAdd));
                        c6.setLayoutX(150);
                        c6.setLayoutY(270);
                        Joueur j6 = new Joueur();
                        j6.setPoste("Défenseur");
                        c6.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                cSelect = c6;
                                fillTable(String.valueOf(eDao.getEquipeById(id).getId()));
                            }
                        });

                        c7.setFill(new ImagePattern(imgAdd));
                        c7.setLayoutX(185);
                        c7.setLayoutY(357);
                        Joueur j7 = new Joueur();
                        j7.setPoste("Défenseur");
                        c7.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                cSelect = c7;
                                fillTable(String.valueOf(eDao.getEquipeById(id).getId()));
                            }
                        });

                        //GARDIEN

                        c8.setFill(new ImagePattern(imgAdd));
                        c8.setLayoutX(50);
                        c8.setLayoutY(222);
                        Joueur j8 = new Joueur();
                        j8.setPoste("Gardien");
                        c8.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                cSelect = c8;
                                fillTable(String.valueOf(eDao.getEquipeById(id).getId()));
                            }
                        });

                        //MILIEUX

                        c9.setFill(new ImagePattern(imgAdd));
                        c9.setLayoutX(350);
                        c9.setLayoutY(70);
                        Joueur j9 = new Joueur();
                        j9.setPoste("Milieu");
                        c9.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                cSelect = c9;
                                fillTable(String.valueOf(eDao.getEquipeById(id).getId()));
                            }
                        });

                        c10.setFill(new ImagePattern(imgAdd));
                        c10.setLayoutX(350);
                        c10.setLayoutY(220);
                        Joueur j10 = new Joueur();
                        j10.setPoste("Milieu");
                        c10.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                cSelect = c10;
                                fillTable(String.valueOf(eDao.getEquipeById(id).getId()));
                            }
                        });

                        c11.setFill(new ImagePattern(imgAdd));
                        c11.setLayoutX(350);
                        c11.setLayoutY(357);
                        Joueur j11 = new Joueur();
                        j11.setPoste("Milieu");
                        c11.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                cSelect = c11;
                                fillTable(String.valueOf(eDao.getEquipeById(id).getId()));
                            }
                        });

                        terrain.getChildren().addAll(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11);
                    }else if (jDao.getJoueursByEquipe(eDao.getEquipeById(id)).size() >=1 && jDao.getJoueursByEquipe(eDao.getEquipeById(id)).size() <=10){

                    }else if (jDao.getJoueursByEquipe(eDao.getEquipeById(id)).size() == 11){

                    }

                    btnRet.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            init();
                            ModerationPresidentPane.super.getChildren().removeAll(terrain,table);
                        }
                    });
                }
            });
        }

        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("presidentPane"));
            }
        });

        this.add(vBoxBtn, 1,1);
    }

    /**
     * Méthode qui créé la page de modération des équipes d'un président de club
     */

    public ModerationPresidentPane(){
        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        String imgTerrain = Main.class.getResource("img/equipe/terrain.png").toExternalForm();
        terrain.setStyle("-fx-background-image: url('" + imgTerrain + "');" + "-fx-background-size: 700px;");
        terrain.setId("terrain");

        TableColumn<Joueur,String> prenom = new TableColumn<>("Prénom");
        TableColumn<Joueur,String> nom = new TableColumn<>("Nom");
        TableColumn<Joueur,Date> dateNaissance = new TableColumn<>("Date de naissance");
        TableColumn<Joueur,Date> dateInsciption = new TableColumn<>("Date d'inscription");

        table.getColumns().addAll(prenom,nom,dateNaissance,dateInsciption);

        prenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));
        nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        dateNaissance.setCellValueFactory(new PropertyValueFactory<>("dateNaissance"));
        dateInsciption.setCellValueFactory(new PropertyValueFactory<>("dateInscription"));

        addButtonToTable();

        btnRet.setAlignment(Pos.BOTTOM_RIGHT);

        this.add(titre,1,0);
        this.add(btnRet,7,7);
    }
}
