/**
 * Classe: LgePalmares
 * @author: Raphaël Olivier
 * @version: 1.1
 */

package fr.hautil.olivir.e4.classes;

public class LgePalmares{
    private int id;
    private int idClub;
    private Competition competition;
    private int nombre;

    /**
     * Méthode qui créé une ligne de palmarès vide
     */

    public LgePalmares(){

    }

    /**
     * Méthode qui créé une ligne de palmarès avec ses attributs
     *
     * @param id id de la ligne
     * @param idClub id du club
     * @param competition compétition gagnée par le club
     * @param nombre nombre de fois que le club a gagné la compétition
     */

    public LgePalmares(int id, int idClub, Competition competition, int nombre) {
        this.id = id;
        this.idClub = idClub;
        this.competition = competition;
        this.nombre = nombre;
    }

    /**
     * Méthode qui récupère et retourne l'id de la ligne
     *
     * @return id
     */

    public int getId() {
        return id;
    }

    /**
     * Méthode qui change l'id de la ligne
     *
     * @param id id de la ligne
     */

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Méthode qui récupère et retourne l'id du club
     *
     * @return idClub
     */

    public int getIdClub() {
        return idClub;
    }

    /**
     * Méthode qui change l'id du club
     *
     * @param idClub id du club
     */

    public void setIdClub(int idClub) {
        this.idClub = idClub;
    }

    /**
     * Méthode qui récupère et retourne l'id de la compétition
     *
     * @return idCompetition
     */

    public Competition getCompetition() {
        return competition;
    }

    /**
     * Méthode qui change l'id de la compétition
     *
     * @param competition id de la compétition
     */

    public void setCompetition(Competition competition) {
        this.competition = competition;
    }

    /**
     * Méthode qui récupère et retourne le nombre de compétition gagnée
     *
     * @return nombre
     */

    public int getNombre() {
        return nombre;
    }

    /**
     * Méthode qui change le nombre de compétition gagnée
     *
     * @param nombre nombre de compétition gagnée
     */

    public void setNombre(int nombre) {
        this.nombre = nombre;
    }

    /**
     * Méthode qui affiche la ligne de palmarès
     *
     * @return idCompetition, nombre
     */

    @Override
    public String toString() {
        return nombre + " " + competition + "\n";
    }
}
