/**
 * Classe: StatsJoueurPane
 * @author: Raphaël Olivier
 * @version: 1.1
 */

package fr.hautil.olivir.e4.base;

import fr.hautil.olivir.e4.classes.Joueur;
import fr.hautil.olivir.e4.classes.StatsJoueurs;
import fr.hautil.olivir.e4.dao.StatsJoueursDAO;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class StatsJoueurPane extends GridPane {

    private Joueur joueur = null;

    public static Text textStats = new Text("");

    /**
     * Méthode qui change le joueur connecté
     *
     * @param joueur joueur connecté
     */

    public void setJoueur(Joueur joueur) {
        this.joueur = joueur;
        init();
    }

    /**
     * Méthode qui récupère et retourne le joueur connecté
     *
     * @return joueur
     */

    public Joueur getJoueur(){
        return joueur;
    }

    /**
     * Méthode qui initialise la page de stats des joueurs
     */

    public void init(){
        StatsJoueursDAO statsDao = new StatsJoueursDAO();
        StatsJoueurs stats;
        stats = statsDao.getStatsByJoueur(joueur.getId());
        textStats.setText(stats.toString());

        Button btnRet = new Button("Retour");

        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                JoueurPane jPane = (JoueurPane) Main.getPanel("joueurPane");
                jPane.setJoueur(joueur);
                Main.getScene().setRoot(jPane);
            }
        });
        this.add(btnRet,7,7);
    }

    /**
     * Création d'une page de statistiques de joueur.
     */

    public StatsJoueurPane(){

        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        this.add(textStats,1,1);
    }
}
