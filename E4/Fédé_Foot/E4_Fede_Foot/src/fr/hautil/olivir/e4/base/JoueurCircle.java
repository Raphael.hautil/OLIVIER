package fr.hautil.olivir.e4.base;

import fr.hautil.olivir.e4.classes.Joueur;
import javafx.scene.shape.Circle;

public class JoueurCircle extends Circle {
    private Joueur joueur;

    /**
     * Création d'un nouveau cercle de joueur avec sa photo.
     *
     * @param radius rayon du cercle
     * @param joueur Joueur associé au cercle
     */

    public JoueurCircle(double radius, Joueur joueur) {
        super(radius);
        this.joueur = joueur;
    }

    /**
     * Méthode qui change le joueur
     *
     * @param joueur joueur associé au cercle
     */

    public void setJoueur(Joueur joueur) {
        this.joueur = joueur;
    }

    /**
     * Méthode qui récupère et retourne le joueur associé au cercle
     *
     * @return joueur
     */

    public Joueur getJoueur() {
        return joueur;
    }
}
