/**
 * Classe: PrésidentClubDAO
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package fr.hautil.olivir.e4.dao;

import fr.hautil.olivir.e4.classes.Club;
import fr.hautil.olivir.e4.classes.PresidentClub;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class PrésidentClubDAO extends DAO{

    /**
     * Méthode qui retourne tous les présidents de club de la vues de données dans une liste
     *
     * @return list
     */

    public ArrayList<PresidentClub> getPrésidents(){
        ArrayList<PresidentClub> list = new ArrayList<>();
        PresidentClub pc;
        Club c;
        Connection conn;
        try{
            conn = super.getConnection();
            Statement stmt = conn.createStatement();
            String req = "SELECT * FROM Licencié, PresidentClub, Club WHERE PresidentClub.idL = Licencié.id AND PresidentClub.club = Club.id;";
            ResultSet result = stmt.executeQuery(req);
            while (result.next()){
                pc = new PresidentClub();
                c = new Club();
                pc.setId(result.getString(1));
                pc.setPrenom(result.getString(2));
                pc.setNom(result.getString(3));
                pc.setDateNaissance(result.getDate(4));
                pc.setDateInscription(result.getDate(5));
                pc.setPassword(result.getString(7));
                c.setId(result.getInt(10));
                c.setLibelle(result.getString(11));
                c.setDateCreation(result.getDate(12));
                pc.setClub(c);
                list.add(pc);
            }
            stmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Méthode qui retourne un président de club en fonction de son id
     *
     * @param id id du président de club
     * @return pc
     */

    public PresidentClub getPresidentById(String id){
        PresidentClub pc = new PresidentClub();
        Club c;
        Connection conn;
        try{
            conn = super.getConnection();
            String req = "SELECT * FROM Licencié, PresidentClub, Club WHERE PresidentClub.idL = Licencié.id AND PresidentClub.club = Club.id AND PresidentClub.idL = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,id);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                pc = new PresidentClub();
                c = new Club();
                pc.setId(result.getString(1));
                pc.setPrenom(result.getString(2));
                pc.setNom(result.getString(3));
                pc.setDateInscription(result.getDate(5));
                pc.setDateNaissance(result.getDate(4));
                pc.setPassword(result.getString(7));
                c.setId(result.getInt(10));
                c.setLibelle(result.getString(11));
                c.setDateCreation(result.getDate(12));
                pc.setClub(c);
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return pc;
    }

    /**
     * Méthode qui retourne le président du club demandé
     *
     * @param club club du président du club
     * @return pc
     */

    public PresidentClub getPrésidentByClub(String club){
        PresidentClub pc = new PresidentClub();
        Connection conn;
        try{
            conn = super.getConnection();
            String req = "SELECT * FROM PresidentClub, Licencié WHERE PresidentClub.idL = Licencié.id AND Club = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,club);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                pc.setId(result.getString(1));
                pc.setPrenom(result.getString(4));
                pc.setNom(result.getString(5));
                pc.setDateNaissance(result.getDate(6));
                pc.setDateInscription(result.getDate(7));
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return pc;
    }
}