/**
 * Classe: GestionDemandePane
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package fr.hautil.olivir.e4.base;

import fr.hautil.olivir.e4.classes.AgentFédé;
import fr.hautil.olivir.e4.classes.Demande;
import fr.hautil.olivir.e4.dao.DemandeDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.sql.Date;
import java.util.ArrayList;

public class GestionDemandePane extends GridPane {

    private AgentFédé agentFédé = null;
    private TableView<Demande> table = new TableView<>();
    private DemandeDAO dDao = new DemandeDAO();
    private HBox hBoxBtn = new HBox();
    private VBox vBox = new VBox();
    private Button btnValider = new Button("Valider");
    private Button btnRefuser = new Button("Refuser");
    private Text txtResult = new Text("");
    private ArrayList<Demande> list = dDao.getDemandes();

    /**
     * Méthode qui récupère et retourne l'agent de la fédération connecté
     *
     * @return agentFédé
     */

    public AgentFédé getAgentFédé() {
        return agentFédé;
    }

    /**
     * Méthode qui change l'agent de la fédération connecté
     *
     * @param agentFédé agent de la fédération connecté
     */

    public void setAgentFédé(AgentFédé agentFédé) {
        this.agentFédé = agentFédé;
        init();
    }

    private void clickItem(){
        table.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                String id = String.valueOf(table.getSelectionModel().getSelectedItem().getId());
                hBoxBtn.setVisible(true);

                btnValider.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        dDao.accepterDemande(id, agentFédé.getId());
                        txtResult.setText("Demande acceptée");
                        list = dDao.getDemandes();
                        ObservableList list2 = FXCollections.observableArrayList(list);
                        table.setItems(list2);
                    }
                });

                btnRefuser.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        dDao.refuserDemande(id, agentFédé.getId());
                        txtResult.setText("Demande refusée");
                        list = dDao.getDemandes();
                        ObservableList list2 = FXCollections.observableArrayList(list);
                        table.setItems(list2);
                    }
                });
            }
        });
    }

    /**
     * Méthode qui initialise la page de gestion de demandes de création d'équipe
     */

    private void init(){
        hBoxBtn.setVisible(false);

        Button btnRet = new Button("Retour");

        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("agentPane"));
            }
        });

        ObservableList list2 = FXCollections.observableArrayList(list);
        table.setItems(list2);

        this.add(btnRet,3,3);

        clickItem();
    }

    /**
     * Création d'une page de gestion de demande de création d'équipe
     */

    public GestionDemandePane(){
        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        TableColumn<Demande,String> id = new TableColumn<>("ID");
        TableColumn<Demande,String> club = new TableColumn<>("Club");
        TableColumn<Demande,String> nom = new TableColumn<>("Nom");
        TableColumn<Demande,String> categorie = new TableColumn<>("Catégorie");
        TableColumn<Demande,Date> dateCreation = new TableColumn<>("Date de création");

        table.getColumns().addAll(id,club,nom,categorie,dateCreation);

        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        club.setCellValueFactory(new PropertyValueFactory<>("club"));
        nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        categorie.setCellValueFactory(new PropertyValueFactory<>("categorie"));
        dateCreation.setCellValueFactory(new PropertyValueFactory<>("dateCreation"));

        id.setSortType(TableColumn.SortType.DESCENDING);

        hBoxBtn.getChildren().addAll(btnValider,btnRefuser);
        vBox.getChildren().addAll(hBoxBtn,txtResult);

        this.add(table, 2,2);
        this.add(vBox,3,2);
    }
}
