/**
 * Classe: PresidentPane
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package fr.hautil.olivir.e4.vues;

import fr.hautil.olivir.e4.Main;
import fr.hautil.olivir.e4.classes.PresidentClub;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class PresidentPane extends GridPane {

    private PresidentClub presidentClub = null;
    private Text idPresident = new Text("");
    private Text prenomPresident = new Text("");
    private Text nomPresident = new Text("");
    private Text dateNaissancePresident = new Text("");
    private Text dateInscriprionPresident = new Text("");
    private Text clubPresident = new Text("");
    private Text passwordPresident = new Text("");
    private Text titre = new Text("");

    /**
     * Méthode qui change le président du club de la page
     *
     * @param presidentClub président du Club
     */

    public void setPresidentClub(PresidentClub presidentClub){
        this.presidentClub = presidentClub;
        init();
    }

    /**
     * Méthode qui récupère et retourne le président du club connecté
     *
     * @return presidentClub
     */

    public PresidentClub getPresidentClub() {
        return presidentClub;
    }

    /**
     * Méthode qui initialise la page de président de club
     */

    private void init(){
        titre.setText("Bienvenue sur votre page de président de club !");
        Button btnDemande = new Button("Faire une demande de nouvelle équipe");
        Button btnDeco = new Button("Déconnexion");
        Button btnHisto = new Button("Historique des demandes de création d'équipe");
        Button btnModo = new Button("Modération équipes");

        VBox vBox = new VBox();

        idPresident.setText("Id: " + presidentClub.getId());
        prenomPresident.setText("Prénom: " + presidentClub.getPrenom());
        nomPresident.setText("Nom: " + presidentClub.getNom());
        dateNaissancePresident.setText("Date de naissance: " + presidentClub.getDateNaissance());
        dateInscriprionPresident.setText("Date d'inscription: " + presidentClub.getDateInscription());
        clubPresident.setText("Club: " + presidentClub.getClub().getLibelle());
        passwordPresident.setText("Mot de passe: " + presidentClub.getPassword());

        vBox.getChildren().addAll(idPresident,prenomPresident,nomPresident,dateNaissancePresident,dateInscriprionPresident,clubPresident,passwordPresident);

        btnDeco.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                LoginPane.deco();
            }
        });

        btnDemande.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DemandePane demandePane = (DemandePane) Main.getPanel("demandePane");
                demandePane.setPresidentClub(presidentClub);
                Main.getScene().setRoot(demandePane);
            }
        });

        btnModo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ModerationPresidentPane moderationPresidentPane = (ModerationPresidentPane) Main.getPanel("moderationPresidentPane");
                moderationPresidentPane.setPresident(presidentClub);
                Main.getScene().setRoot(moderationPresidentPane);
            }
        });

        btnHisto.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                HistoDemandesPresidentPane histoDemandesPresidentPane = (HistoDemandesPresidentPane) Main.getPanel("histoDemandesPresidentPane");
                histoDemandesPresidentPane.setPresident(presidentClub);
                Main.getScene().setRoot(histoDemandesPresidentPane);
            }
        });

        this.add(vBox,0,1);
        this.add(btnDemande,1,2);
        this.add(btnModo,2,2);
        this.add(btnDeco,2,3);
        this.add(btnHisto,1,3);
    }

    /**
     * Création d'une page de président de club
     */

    public PresidentPane(){
        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        titre.setText("");
        titre.setId("title");

        this.add(titre,1,0);
    }
}
