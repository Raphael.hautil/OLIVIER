/**
 * Classe: Entraineur
 * @author: Raphaël Olivier
 * @version: 1.1
 */

package fr.hautil.olivir.e4.classes;

import java.util.Date;

public class Entraineur extends Licencié{
    private Equipe equipe;

    /**
     * Méthode qui créé un entraîneur vide
     */

    public Entraineur() {
    }

    /**
     * Méthode qui créé un entraîneur avec ses attributs
     *
     * @param id id de l'entraîneur
     * @param prenom prénom de l'entraîneur
     * @param nom nom de l'entraîneur
     * @param dateNaissance date de naissance de l'entraîneur
     * @param dateInscription date d'inscription de l'entraîneur
     * @param type type du licencié
     * @param password mot de passe de l'entraîneur
     * @param equipe équipe de l'entraîneur
     */

    public Entraineur(String id, String prenom, String nom, Date dateNaissance, Date dateInscription, String type, String password, Equipe equipe){
        super(id, prenom, nom, dateNaissance, dateInscription, type, password);
        this.equipe = equipe;
    }

    /**
     * Méthode qui récupère et retourne l'équipe de l'entraîneur
     *
     * @return equipe
     */

    public Equipe getEquipe() {
        return equipe;
    }

    /**
     * Méthode qui change l'équipe de l'entraîneur
     *
     * @param equipe équipe de l'entraîneur
     */

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }

    /**
     * Méthode qui affiche l'entraîneur
     *
     * @return
     */

    @Override
    public String toString() {
        return "Entraineur{" +
                "Id='" + id + '\'' +
                ", Prénom='" + prenom + '\'' +
                ", Nom='" + nom + '\'' +
                ", Date de naissance=" + dateNaissance + '\'' +
                ", Date d'inscription=" + dateInscription + '\'' +
                ", Equipe=" + equipe + '\'' +
                '}';
    }
}
