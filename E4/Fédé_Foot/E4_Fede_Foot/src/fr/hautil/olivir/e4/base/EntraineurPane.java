/**
 * Classe: EntraineurPane
 * @author: Raphaël Olivier
 * @version: 1.2
 */

package fr.hautil.olivir.e4.base;

import fr.hautil.olivir.e4.classes.Entraineur;
import fr.hautil.olivir.e4.classes.Equipe;
import fr.hautil.olivir.e4.dao.EquipeDAO;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class EntraineurPane extends GridPane {

    private Entraineur entraineur = null;
    private Text idEntraineur = new Text("");
    private Text prenomEntraineur = new Text("");
    private Text nomEntraineur = new Text("");
    private Text dateNaissanceEntraineur = new Text("");
    private Text dateInscriptionEntraineur = new Text("");
    private Text equipeEntraineur = new Text("");
    private Text passwordEntraineur = new Text("");
    private Text titre = new Text("");

    /**
     * Méthode qui change l'entraîneur de la page
     *
     * @param entraineur entraîneur
     */

    public void setEntraineur(Entraineur entraineur) {
        this.entraineur = entraineur;
        init();
    }

    /**
     * Méthode qui récupère et retourne l'entraîneur
     *
     * @return entraineur
     */

    public Entraineur getEntraineur() {
        return entraineur;
    }

    /**
     * Méthode qui initialise la page d'entraîneur
     */

    private void init(){
        titre.setText("Bienvenue sur votre page d'entraîneur !");
        Button btnModifEquipe = new Button("Voir mon équipe");
        Button btnDeco = new Button("Déconnexion");

        VBox vBoxInfos = new VBox();

        idEntraineur.setText("Id: " + entraineur.getId());
        prenomEntraineur.setText("Prénom: " + entraineur.getPrenom());
        nomEntraineur.setText("Nom: " + entraineur.getNom());
        dateNaissanceEntraineur.setText("Date de naissance: " + entraineur.getDateNaissance().toString());
        dateInscriptionEntraineur.setText("Date d'inscription: " + entraineur.getDateInscription().toString());
        equipeEntraineur.setText("Equipe: " + entraineur.getEquipe().getLibelle());
        passwordEntraineur.setText("Mot de passe: " + entraineur.getPassword());

        vBoxInfos.getChildren().addAll(idEntraineur,prenomEntraineur,nomEntraineur,dateNaissanceEntraineur,dateInscriptionEntraineur,equipeEntraineur,passwordEntraineur);

        this.add(vBoxInfos,0,0);
        this.add(btnDeco,2,2);
        this.add(btnModifEquipe,1,1);
        this.add(titre,2,0);

        btnModifEquipe.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                EquipeDAO eDao = new EquipeDAO();
                Equipe equipe = eDao.getEquipeByEntraineur(entraineur.getId());
                EntraineurModifEquipePane eModif = (EntraineurModifEquipePane) Main.getPanel("entraineurModifEquipePane");
                eModif.setEquipe(equipe);
                Main.getScene().setRoot(eModif);
            }
        });

        btnDeco.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event){
                LoginPane.deco();
            }
        });
    }

    /**
     * Création d'une page d'entraîneur.
     */

    public EntraineurPane(){
        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        idEntraineur.setText("");
        prenomEntraineur.setText("");
        nomEntraineur.setText("");
        dateNaissanceEntraineur.setText("");
        dateInscriptionEntraineur.setText("");
        equipeEntraineur.setText("");
        passwordEntraineur.setText("");
        titre.setText("");
    }
}
