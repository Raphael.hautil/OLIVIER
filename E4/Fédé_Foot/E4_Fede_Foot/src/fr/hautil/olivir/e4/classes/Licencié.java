/**
 * Classe: Licencié
 * @author: Raphaël Olivier
 * @version: 1.1
 */

package fr.hautil.olivir.e4.classes;

import java.util.Date;

public class Licencié {
    protected String id;
    protected String prenom;
    protected String nom;
    protected Date dateNaissance;
    protected Date dateInscription;
    protected String type;
    protected String password;

    /**
     * Méthode qui créé un licencié vide
     */

    public Licencié(){
    }

    /**
     * Méthode qui créé un licencié avec ses attributs
     *
     * @param id id du licencié
     * @param prenom prenom du licencié
     * @param nom nom du licencié
     * @param dateNaissance date de naissance du licencié
     * @param dateInscription date d'inscription du licencié
     * @param type type du licencié
     * @param password mot de passe du licencié
     */

    public Licencié(String id, String prenom, String nom, Date dateNaissance, Date dateInscription, String type, String password){
        this.id = id;
        this.prenom = prenom;
        this.nom = nom;
        this.dateNaissance = dateNaissance;
        this.dateInscription = dateInscription;
        this.type = type;
        this.password = password;
    }

    /**
     * Méthode qui récupère et retourne l'id du licencié
     *
     * @return id
     */

    public String getId() {
        return id;
    }

    /**
     * Méthode qui récupère et retourne le prénom du licencié
     *
     * @return prenom
     */

    public String getPrenom(){
        return prenom;
    }

    /**
     * Méthode qui récupère et retourne le nom du licencié
     *
     * @return nom
     */

    public String getNom(){
        return nom;
    }

    /**
     * Méthode qui récupère et retourne la date de naissance du licencié
     *
     * @return dateNaissance
     */

    public Date getDateNaissance() {
        return dateNaissance;
    }

    /**
     * Méthode qui récupère et retourne la date d'inscription du licencié
     *
     * @return dateInscription
     */

    public Date getDateInscription() {
        return dateInscription;
    }

    /**
     * Méthode qui récupère et retourne le type du licencié
     *
     * @return type
     */

    public String getType() {
        return type;
    }

    /**
     * Méthode qui récupère et retourne le mot de passe du licencié
     *
     * @return password
     */

    public String getPassword() {
        return password;
    }

    /**
     * Méthode qui change l'id du licencié
     *
     * @param id id du licencié
     */

    public void setId(String id) {
        this.id = id;
    }

    /**
     * Méthode qui change le prénom du licencié
     *
     * @param prenom prénom du licencié
     */

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Méthode qui change le nom du licencié
     *
     * @param nom nom du licencié
     */

    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Méthode qui change la date de naissance du licencié
     *
     * @param dateNaissance date de naissance du licencié
     */

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    /**
     * Méthode qui change la date d'inscription du licencié
     *
     * @param dateInscription date d'inscription du licencié
     */

    public void setDateInscription(Date dateInscription) {
        this.dateInscription = dateInscription;
    }

    /**
     * Méthode qui change le type du licencié
     *
     * @param type type du licencié
     */

    public void setType(String type) {
        this.type = type;
    }

    /**
     * Méthode qui change le mot de passe du licencié
     *
     * @param password mot de passe du licencié
     */

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Méthode qui affiche le licencié
     *
     * @return id, prenom, nom, dateNaissance, dateInscription, type, password
     */

    @Override
    public String toString() {
        return "Licencié{" +
                "Id='" + id + '\'' +
                ", Prénom='" + prenom + '\'' +
                ", Nom='" + nom + '\'' +
                ", Date de naissance=" + dateNaissance +
                ", Date d'inscription=" + dateInscription +
                ", Type='" + type + '\'' +
                '}';
    }
}