/**
 * Classe: PublicClubPane
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package fr.hautil.olivir.e4.base;

import fr.hautil.olivir.e4.classes.Club;
import fr.hautil.olivir.e4.dao.ClubDAO;
import fr.hautil.olivir.e4.dao.PalmaresDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.sql.Date;
import java.util.ArrayList;

public class PublicClubPane extends GridPane {

    private TableView<Club> table = new TableView<>();
    private Region logo = new Region();
    private VBox vBoxPalm = new VBox();
    private Text palmares = new Text("");

    private void clickItem(){
        table.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                PalmaresDAO pDao = new PalmaresDAO();
                int id = table.getSelectionModel().getSelectedItem().getId();
                String img = Main.class.getResource("../img/clubs/" + id + ".png").toExternalForm();
                logo.setStyle("-fx-background-image: url('" + img + "');" + "-fx-background-size: 300;");
                palmares.setText(pDao.getPalmaresByClub(String.valueOf(id)));
            }
        });
    }

    /**
     * Création d'une page de consultation de club par le public.
     */

    public PublicClubPane(){

        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        ClubDAO cDao = new ClubDAO();

        Button btnRet = new Button("Retour");

        logo.setId("logo");

        TableColumn<Club,String> id = new TableColumn<>("ID");
        TableColumn<Club,String> libelle = new TableColumn<>("Nom");
        TableColumn<Club,Date> dateCreation = new TableColumn<>("Date de création");

        table.getColumns().addAll(id,libelle,dateCreation);

        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        libelle.setCellValueFactory(new PropertyValueFactory<>("libelle"));
        dateCreation.setCellValueFactory(new PropertyValueFactory<>("dateCreation"));

        id.setSortType(TableColumn.SortType.DESCENDING);

        ArrayList<Club> list = cDao.getClubs();
        ObservableList list2 = FXCollections.observableArrayList(list);
        table.setItems(list2);

        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("publicPane"));
            }
        });

        vBoxPalm.getChildren().addAll(logo,palmares);

        this.add(table,1,1);
        this.add(btnRet,1,2);
        this.add(vBoxPalm,2,1);

        clickItem();
    }
}
