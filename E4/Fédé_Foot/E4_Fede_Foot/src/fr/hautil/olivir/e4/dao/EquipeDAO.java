/**
 * Classe: EquipeDAO
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package fr.hautil.olivir.e4.dao;

import fr.hautil.olivir.e4.classes.Equipe;
import fr.hautil.olivir.e4.classes.Joueur;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class EquipeDAO extends DAO {

    /**
     * Méthode qui retourne toutes les équipes de la vues de données dans une liste
     *
     * @return list
     */

    public ArrayList<Equipe> getEquipes(){
        ArrayList<Equipe> list = new ArrayList<>();
        Connection conn;
        Equipe eq;
        try{
            conn = super.getConnection();
            Statement stmt = conn.createStatement();
            String req = "SELECT * FROM Equipe;";
            ResultSet result = stmt.executeQuery(req);
            while (result.next()){
                eq = new Equipe();
                eq.setId(result.getInt(1));
                eq.setLibelle(result.getString(2));
                eq.setCategorie(result.getString(3));
                eq.setDateCreation(result.getDate(4));
                eq.setClub(result.getInt(5));
                list.add(eq);
            }
            stmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Méthode qui retourne une équipe en fonction de son id
     *
     * @param id id de l'équipe
     * @return eq
     */

    public Equipe getEquipeById(String id){
        Equipe eq = new Equipe();
        Connection conn;
        try{
            conn = super.getConnection();
            String req = "SELECT * FROM Equipe WHERE id = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,id);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                eq.setId(result.getInt(1));
                eq.setLibelle(result.getString(2));
                eq.setCategorie(result.getString(3));
                eq.setDateCreation(result.getDate(4));
                eq.setClub(result.getInt(5));
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return eq;
    }

    /**
     * Méthode qui retourne toutes les équipes d'un club dans une liste
     *
     * @param club club de l'équipe
     * @return list
     */

    public ArrayList<Equipe> getEquipesByClub(String club){
        ArrayList<Equipe> list = new ArrayList<>();
        Equipe eq;
        Connection conn;
        try{
            conn = super.getConnection();
            String req = "SELECT * FROM Equipe WHERE club = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,club);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                eq = new Equipe();
                eq.setId(result.getInt(1));
                eq.setLibelle(result.getString(2));
                eq.setCategorie(result.getString(3));
                eq.setClub(result.getInt(5));
                eq.setDateCreation(result.getDate(4));
                list.add(eq);
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Méthode qui retourne l'équipe d'un entraîneur
     *
     * @param idE id de l'entraineur
     * @return eq
     */

    public Equipe getEquipeByEntraineur(String idE){
        Equipe eq = new Equipe();
        Connection conn;
        try{
            conn = super.getConnection();
            String req = "SELECT * FROM Entraineur, Equipe WHERE Equipe.id = Entraineur.equipe AND Entraineur.idL = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,idE);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                eq.setId(result.getInt(3));
                eq.setLibelle(result.getString(4));
                eq.setCategorie(result.getString(5));
                eq.setDateCreation(result.getDate(6));
                eq.setClub(result.getInt(7));
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return eq;
    }

    /**
     * Méthode qui créé une équipe
     *
     * @param libelle libellé de l'équipe
     * @param categorie catégorie de l'équipe
     * @param idClub club de l'équipe
     */

    public void createTeam(String libelle, String categorie, String idClub){
        Connection conn;
        String format = "yyyy-MM-dd";
        try{
            conn = super.getConnection();
            SimpleDateFormat formater = new SimpleDateFormat(format);
            Date date = new Date();
            String req = "INSERT INTO Equipe VALUES (?,?,?,?,?);";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,String.valueOf(getLastId()+1));
            pstmt.setString(2,libelle);
            pstmt.setString(3,categorie);
            pstmt.setString(4,formater.format(date));
            pstmt.setString(5,idClub);
            pstmt.executeUpdate();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Méthode qui récupère le dernier id enregistré des équipes
     *
     * @return id
     */

    private int getLastId(){
        Connection conn;
        int id = 0;
        try{
            conn = super.getConnection();
            Statement stmt = conn.createStatement();
            String req = "SELECT id FROM Equipe ORDER BY id DESC LIMIT 0,1";
            ResultSet result = stmt.executeQuery(req);
            while (result.next()){
                id = result.getInt(1);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return id;
    }

    public void addJoueurToTeam(Joueur j, Equipe equipe){
        Connection conn;
        try{
            conn = super.getConnection();
            String req = "UPDATE Joueur SET equipe = ? WHERE idL = ?";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,String.valueOf(equipe.getId()));
            pstmt.setString(2,j.getId());
            pstmt.executeUpdate();
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}