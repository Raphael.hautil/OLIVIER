/**
 * Classe: Competition
 * @author: Raphaël Olivier
 * @version: 1.1
 */

package fr.hautil.olivir.e4.classes;

public class Competition {
    private int id;
    private String libelle;

    /**
     * Méthode qui créé une compétition vide
     */

    public Competition(){

    }

    /**
     * Méthode qui créé une compétition avec ses attributs
     *
     * @param id id de la compétition
     * @param libelle nom de la compétition
     */

    public Competition(int id, String libelle) {
        this.id = id;
        this.libelle = libelle;
    }

    /**
     * Méthode qui récupère et retourne l'id de la compétition
     *
     * @return id
     */

    public int getId() {
        return id;
    }

    /**
     * Méthode qui change l'id de la compétition
     *
     * @param id id de la compétition
     */

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Méthode qui récupère et retourne le libellé de la compétition
     *
     * @return libelle
     */

    public String getLibelle() {
        return libelle;
    }

    /**
     * Méthode qui change le libellé de la compétition
     *
     * @param libelle nom de la compétition
     */

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * Méthode qui affiche la compétition
     *
     * @return libelle
     */

    @Override
    public String toString() {
        return libelle;
    }
}
