E4           AS FOOT:

	Sp�cifications techniques :

		- Base de donn�es fonctionnelle (d�ploy�e) + MCD (sch�ma relationnel)
		- C�t� client : JavaFX + Diagramme de classes UML

	Livrables attendus :

		- Code du projet
		- Documentation (Java Doc, Documentation Technique : diagrammes de classes, MCD, pr�requis; Documentation Utilisateur : screenshots explicatifs)

	Consignes :

		- Port�e : F�d�ration
		- Utilisateurs : Joueurs, Entra�neurs, Public, Agent de la f�d�ration, Pr�sident club
		- Ajouter/Supprimer/Modifier des clubs
		- Ajouter/Supprimer/Modifier des �quipes
		- Ajouter/Supprimer/Modifier des joueurs
		- Demandes de nouveaux clubs
		- F�d�ration g�re ces demandes
		- Distinction entre joueurs amateurs et joueurs pro

Sch�ma relationnel :

	Licenci�(id, prenom, nom, dateNaissance, dateInscription, Type)
	Cl� primaire : id

	Joueur(idL, categorie, estPro, poste, equipe, numero)
	Cl� primaire : idL
	Cl�s �trang�res : idL ---> Licenci�, equipe ---> id Equipe

	Entraineur(idL, club, equipe)
	Cl� primaire : idL
	Cl�s �trang�res : idL ---> Licenci�

	Pr�sidentClub(idL, club)
	Cl� primaire : idL
	Cl�s �trang�res : idL ---> Licenci�

	AgentF�d�(idL, hi�rarchie)
	Cl� primaire : idL
	Cl�s �trang�res : idL ---> Licenci�

	Club(id, libelle, dateCreation)
	Cl� primaire : id

	Equipe(id, libelle, categorie, dateCreation, club)
	Cl� primaire : id
	Cl� �trang�re : club ---> id Club

	Demande(id, idP, nom, date)
	Cl� primaire: id
	Cl� �trang�re: idP ---> Pr�sidentClub