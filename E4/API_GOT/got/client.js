var nbCultures;
var id = new Array();
var nom = new Array();

$(document).ready(function(){

  $('#idDeleteChar').change(function(){
    $.ajax({
      type: "GET",
      url: "http://localhost/MissionGoT8/index.php/characters",
      success: function(data){
        $('#idDeleteChar').autocomplete(JSON.parse(data));
      }
    });
  });

  $.ajax({
    type: "GET",
    url: "http://localhost/MissionGoT8/index.php/cultures",
    success: function(data){
      nbCultures = JSON.parse(data);
      for (var i = 0; i < nbCultures.length; i++) {
        nom[i] = nbCultures[i].name;
        id[i] = nbCultures[i].id;
      }
      var select = document.getElementById("culturePersos");
      for(index in nom){
        select.options[select.options.length] = new Option(nom[index], id[index]);
      }
    }
  });
});
