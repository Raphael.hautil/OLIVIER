<?php
    class Hero extends Characters{
      private $nomActeur;
      private $imNoble;

      function __construct($nomActeur, $imNoble, $id, $nom, $dateNaissance, $dateMort, $saCulture){
        parent :: __construct($id, $nom, $dateNaissance, $dateMort, $saCulture);
        $this->nomActeur = $nomActeur;
        $this->imNoble = $imNoble;
      }

      public function getNomActeur(){
        return $this->nomActeur;
      }

      public function getImNoble(){
        return $this->imNoble;
      }

      public function setNomActeur($nomActeur){
        $this->nomActeur = $nomActeur;
      }

      public function setImNoble($imNoble){
        $this->imNoble = $imNoble;
      }

      public function __toString(){
        echo parent :: __toString();
        return $this->nomActeur.' '.$this->imNoble.' ';
      }
    }?>
