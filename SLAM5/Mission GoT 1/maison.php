<?php
  class Maison{
	   private $nom;
	   private $devise;
	   private $armoirie;
     private $dateFondation;
	   private $region;

     function __construct($nom, $devise, $armoirie, $dateFondation, $region){
       $this->nom = $nom;
       $this->devise = $devise;
       $this->armoirie = $armoirie;
       $this->dateFondation = $dateFondation;
       $this->region = $region;
     }

	   public function getNom(){
		    return $this->nom;
	   }

	   public function getDevise(){
		    return $this->devise;
	   }

	public function getArmoirie(){
		return $this->armoirie;
	}
	public function getDatefondation(){
		return $this->dateFondation;
	}
	public function getRegion(){
		return $this->region;
	}
	public function setNom($nom){
		$this->nom=$nom;
	}
	public function setDevise($devise){
		$this->devise=$devise;
	}
	public function setArmoirie($armoirie){
		$this->armoirie=$armoirie;
	}
	public function setDatefondation($dateFondation){
		$this->dateFondation=$dateFondation;
	}
	public function setRegion($region){
		$this->region=$region;
	}

  public function __toString(){
    return $this->nom.' '.$this->devise.' '.$this->armoirie.' '.$this->dateFondation.' '.$this->region.' ';
  }
}
?>
