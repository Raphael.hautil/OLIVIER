<?php
    require_once("region.php");
    require_once("maison.php");
    require_once("characters.php");
    require_once("noble.php");
    require_once("hero.php");
    require_once("culture.php");
    $maRegion = new Region("1", "Les Îles de Fer");
    $maMaison1 = new Maison("Gratte-ciel", "Toujours plus haut !", "Global élite", "23/02/1720", "Les Îles de fer");
    $maMaison2 = new Maison("Châlet", "Sauf une fois au châlet", "Sapin sous la neige", "12/05/1823", "Les Îles de fer");
    $maCulture1 = new Culture("1", "Judaïsme");
    $maCulture2 = new Culture("2", "Christianisme");
    $monNoble = new Noble("Michelle", "Jacques", "Jeanne", $maMaison1, "1", "Matis", "1811", "1847", $maCulture1);

    echo $monNoble->getNom();
    echo " ";
    echo $monNoble->getMyHome()->getNom();

    $tabPerso = array(1 => $monNoble,
                      2 => new Noble("Matis", "Jacques", "Jeanne", $maMaison1, "2", "Florent", "1756", "1799", $maCulture2),
                      3 => new Noble("Yohann", "Philippe", "Chloé", $maMaison2, "3", "Thomas", "1278", "1678", $maCulture1),
                      4 => new Noble("Hermione", "Hyrulle", "Pauline", $maMaison2, "4", "Adrien", "1567", "1606", $maCulture1),
                      5 => new Hero("Léonardo Di Caprio", "oui", "5", "Andy", "1745", "1811", $maCulture2),
                      6 => new Hero("Emma Stone", "non", "6", "Mahmoud", "1348", "1400", $maCulture2));

    echo"<form method='POST' action='nbPersonnage.php' target ='_blank'>";
        echo"<input type='submit' name='Ok' value='Voir nombre de personnages instanciés'>";
    echo"</form>";
    echo "<br>";
    echo"<form method='POST' action='mesPersonnages.php' target ='_blank'>";
        echo"<input type='submit' name='Ok' value='Voir tous les personnages'>";
    echo"</form>";
 ?>
