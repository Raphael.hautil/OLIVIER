$(document).ready(function () {

    console.log('demarrage',localStorage.getItem("login"),sessionStorage.getItem("email"));
    console.log('demarrage',localStorage.length,sessionStorage.length);
    document.getElementById('login').value = localStorage.getItem("login");

    var testConn="0";
    console.log("test connexion",testConn);

    $('#btn').click(function () {
        $.ajax({
            url: "connexion.php",
            type: "GET",
            data: {
            	login:$('#login').val(),
            	mdp:$('#mdp').val()
            },
            success: function (resultat) {
                $("#result").html(resultat);
                testConn=$('#testConn').val();
                if (testConn==1) {
                    var loginRenvoyer=$('#loginR').val();
                    var emailRenvoyer=$('#emailR').val();
                    let u1 = new Utilisateur(loginRenvoyer,emailRenvoyer);
                    localStorage.setItem("login",loginRenvoyer);
                    sessionStorage.setItem("email",emailRenvoyer);
                    $("#afficheuser").html("Objet user = " + u1.login + " " + u1.email);
                }
            }
        });
    });

    $('#btn2').click(function () {
        $.ajax({
            url: "mesPerso.php",
            type: "GET",
            data: {
                login:localStorage.getItem("login"),
                email:sessionStorage.getItem("email")
            },
            success: function (resultat1) {
                if (testConn==1) {
                    $("#result1").html(resultat1);
                }
                else {
                    $("#result1").html("Vous devez vous connecter");
                }
            }
        });
    });

    $('#btn3').click(function () {
        sessionStorage.clear();
        localStorage.clear();
        console.log('supp',localStorage.length,sessionStorage.length);
    });
});
