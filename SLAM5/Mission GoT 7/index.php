<?php
require 'vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Firebase\JWT\JWT;

$app = new \Slim\App;

error_reporting(E_ALL);
ini_set('display_errors', 1);

$app->get('/zaza', function(Request $request, Response $response){
	return "Bonjour Monsieur !";
});

$app->get('/personnage/{name}', function(Request $request, Response $response){
	$name = $request->getAttribute('name');
       return getPersonnage($name);
});
function connexion(){
	return $dbh = new PDO("mysql:host=sl-us-south-1-portal.33.dblayer.com:56245;dbname=got", 'admin', 'ZEWHORDIADVLJPDZ', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
}
function getPersonnage($name){
	$sql = "SELECT * FROM characters WHERE name = '".$name."'";
	try{
		$dbh=connexion();
		$statement = $dbh->prepare($sql);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_CLASS);
		return json_encode($result, JSON_PRETTY_PRINT);
	} catch(PDOException $e){
		return '{"error":'.$e->getMessage().'}';
	}
}

$app->get('/user', function(Request $request, Response $response){
	$tb = $request->getQueryParams();
	$token = $tb["token"];
	if(validJWT($token)){
		$login = $tb["login"];
		$pw = $tb["pwd"];
		//fonction d'insertion
		return checkUser($login, $pw);
	}
	else{
		return $response->withStatus(401);
	}
});
function checkUser($login, $pw){
	$a = 0;
	$sql = "SELECT * FROM utilisateur WHERE login = '".$login."' AND pwd = '".$pw."'";
	try{
		$dbh=connexion();
	} catch(PDOException $e){
		return '{"error":'.$e->getMessage().'}';
	}
	$statement = $dbh->prepare($sql);
	$statement->execute();
	foreach ($statement->fetchAll(PDO::FETCH_OBJ) as $ligne){
		$a = 1;
		return "Connexion réussie.<br>".$ligne->email;
	}
	if ($a == 0) {
		return "Connexion échoué.";
	}
}

$app->post('/user', function(Request $request, Response $response){
	$tb = $request->getQueryParams();
	$token = $tb["token"];
	if(validJWT($token)){
		$login = $tb['login'];
		$mail = $tb['email'];
		$pwd = $tb['pwd'];
		return insertUser($login, $mail, $pwd);
	}
	else{
		return $response->withStatus(401);
	}
});

function insertUser($login, $mail, $pwd){
	$sql = "INSERT INTO utilisateur (login, email, pwd) VALUES ('".$login."', '".$mail."', '".$pwd."');";
	try {
			$dbh=connexion();
			$statement = $dbh->prepare($sql);
			$statement->execute();
			$result = $statement->fetchAll(PDO::FETCH_CLASS);
			return "Inscription réussie !";
		}
	catch(PDOException $e){
		return '{"error":'.$e->getMessage().'}';
	}
}

$app->delete('/user', function(Request $request, Response $response){
	$tb = $request->getQueryParams();
	$token = $tb["token"];
	if(validJWT($token)){
		$id = $tb['id'];
		return deleteUser($id);
	}
	else{
		return $response->withStatus(401);
	}
});

function deleteUser($id){
	$sql = "DELETE FROM utilisateur WHERE id = ".$id.";";
	try{
		$dbh=connexion();
		$statement = $dbh->prepare($sql);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_CLASS);
		return "Suppression effectuée avec succès.";
	}
	catch(PDOException $e){
		return '{"error"}:'.$e->getMessage().'}';
	}
}

$app->put('/user', function(Request $request, Response $response){
	$tb = $request->getQueryParams();
	$token = $tb["token"];
	if(validJWT($token)){
		$login = $tb['login'];
		$newMail = $tb['email'];
		return modifMail($login,$newMail);
	}
	else{
		return $response->withStatus(401);
	}
});

function modifMail($login,$newMail){
	$sql = "UPDATE utilisateur SET email = '".$newMail."' WHERE login = '".$login."';";
	try{
		$dbh=connexion();
		$statement = $dbh->prepare($sql);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_CLASS);
		return "Email mis à jour !";
	}
	catch(PDOException $e){
		return '{Error}:'.$e->getMessage().'}';
	}
}

$app->get('/persos', function(Request $request, Response $response){
	return getPersos();
});

function getPersos(){
	$sql = "SELECT * FROM characters LIMIT 100";
	try{
		$dbh=connexion();
		$statement = $dbh->prepare($sql);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_CLASS);
		return json_encode($result, JSON_PRETTY_PRINT);
	}
	catch(PDOException $e){
		return '{Error}:'.$e->getMessage().'}';
	}
}

function checkUserToken($login, $pw){
	$sql = "SELECT * FROM utilisateur WHERE login = '".$login."' AND pwd = '".$pw."'";
	try{
		$dbh=connexion();
		$statement = $dbh->prepare($sql);
		$statement->execute();
		foreach($statement->fetchAll(PDO::FETCH_CLASS) as $ligne){
			return true;
		}
		return false;
	}
	catch(PDOException $e){
		return false;
	}
}

$app->get('/obtentionToken', function(Request $request, Response $response){
	$tb = $request->getQueryParams();
  $login = $tb['login'];
  $pwd = $tb['pwd'];
  $allowed= checkUserToken($login,$pwd);
  if($allowed){
    return getTokenJWT();
  }else{
    return $response->withStatus(401);
  }
});

$app->post('/token', function(Request $request, Response $response){
  $tb = $request->getQueryParams();
	$token = $tb['token'];
  if(validJWT($token)){
    return true;
  }else{
    return $response->withStatus(401);
  }
});

function getTokenJWT() {
  $payload = array(
    "exp" => time() + (60 * 30)
  );
  return JWT::encode($payload, "secret");
}

function validJWT($token) {
  $res = false;
  try {
      $decoded = JWT::decode($token, "secret", array('HS256'));
  } catch (Exception $e) {
    return $res;
  }
  $res = true;
  return $res;
}

function getStatsHouse(){
	$sql = "SELECT COUNT(*) nb, houses.name name FROM characters, houses WHERE house <> 'NULL' AND characters.house = houses.id GROUP BY house HAVING COUNT(*) > 9";
    try {
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_CLASS);
        return json_encode($result, JSON_PRETTY_PRINT);
    }
    catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
    }
}

$app->get('/statsHouse', function(Request $request, Response $response){
	return getStatsHouse();
});

function getStatsCities(){
	$sql = "SELECT COUNT(*) nb, cities_type.name name FROM cities, cities_type WHERE cities.type = cities_type.id GROUP BY cities_type.name;";
	try{
		$dbh=connexion();
		$statement = $dbh->prepare($sql);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_CLASS);
		return json_encode($result, JSON_PRETTY_PRINT);
	}
	catch(PDOException $e){
		return '{"error":'.$e->getMessage().'}';
	}
}

$app->get('/statsCities', function(Request $request, Response $response){
	return getStatsCities();
});

function getStatsPerso(){
	$sql = "CALL getPerso100;";
	try{
		$dbh = connexion();
		$statement = $dbh->prepare($sql);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_CLASS);
		return json_encode($result, JSON_PRETTY_PRINT);
	}
	catch(PDOException $e){
		return '{"error":'.$e->getMessage().'}';
	}
}

$app->get('/statsPerso', function(Request $request, Response $response){
	return getStatsPerso();
});

$app->run();

/*
DROP PROCEDURE IF EXISTS getPerso100;
DELIMITER //
CREATE PROCEDURE getPerso100()
BEGIN
SELECT COUNT(*) nb FROM characters WHERE date_of_birth > -100 AND date_of_birth <= 0
UNION
SELECT COUNT(*) nb FROM characters WHERE date_of_birth > 0 AND date_of_birth <= 100
UNION
SELECT COUNT(*) nb FROM characters WHERE date_of_birth > 100 AND date_of_birth <= 200
UNION
SELECT COUNT(*) nb FROM characters WHERE date_of_birth > 200 AND date_of_birth <= 300;
END;
//;
*/
?>
