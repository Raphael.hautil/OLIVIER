var nbPersoByHouse;
var nbPersoByHouseParse;

var nbCityByType;
var nbCityByTypeParse;

var nbPerso;
var nbPersoParse;

$(document).ready(function() {
  $('#btn-valide').click(function(){
    $.ajax({
      type: "GET",
      contentType: 'application/json; charset=utf-8',
      url: "https://got7ro.mybluemix.net/index.php/zaza",
      success: function(data){
        $("#result").html(data);
      }
    });
  });

  $('#btn2').click(function(){
    $('#table').bootstrapTable({
      url: "https://got7ro.mybluemix.net/index.php/personnage/"+$('#idx').val(),
      columns: [{
        field: 'name',
        title: 'Nom'
      },{
        field: 'id',
        title: 'Id'
      },]
    });
  });

  $('#btn3').click(function(){
    $.ajax({
      url: "https://got7ro.mybluemix.net/index.php/user?token="+ localStorage.getItem('token') + "&login="+$('#login').val()+"&pwd="+$('#pw').val(),
      type: "GET",
      success: function(resultat){
        var mail = resultat;
        $('#result2').html(mail);
      },
      error: function(){
        $('#result2').html("Vous n'êtes pas authorisé.");
      }
    });
  });

  $('#btn4').click(function(){
    $.ajax({
      type: "POST",
      contentType: 'application/json; charset=utf-8',
      url: "https://got7ro.mybluemix.net/index.php/user?login="+$('#login2').val()+"&email="+$('#email').val()+"&pwd="+$('#pw2').val()+"&token="+ localStorage.getItem('token'),
      success: function(data){
        $('#result3').html(data);
      },
      error: function(){
        $('#result3').html("Vous n'êtes pas authorisé.");
      }
    });
  });

  $('#btnDel').click(function(){
    $.ajax({
      type: "DELETE",
      url: "https://got7ro.mybluemix.net/index.php/user?id="+$('#idDel').val()+"&token="+ localStorage.getItem('token'),
      success: function(data){
        $('#resultDel').html(data);
      },
      error: function(){
        $('#resultDel').html("Vous n'êtes pas authorisé.");
      }
    });
  });

  $('#btnNewMail').click(function(){
    $.ajax({
      type: "PUT",
      url: "https://got7ro.mybluemix.net/index.php/user?login="+$('#loginNewMail').val()+"&email="+$('#newMail').val()+"&token="+ localStorage.getItem('token'),
      success: function(data){
        $('#resultNewMail').html(data);
      },
      error: function(){
        $('#resultNewMail').html("Vous n'êtes pas authorisé.");
      }
    });
  });

  $('#btnPersos').click(function(){
    $('#resultPersos').bootstrapTable({
      url: "https://got7ro.mybluemix.net/index.php/persos",
      columns: [{
        field: 'name',
        title: 'Nom'
      },{
        field: 'titles',
        title: 'Titre'
      },{
        field: 'culture',
        title: 'Culture'
      },]
    });
  });

  $('#btnGetToken').click(function(event) {
    $.ajax({
      type: "GET",
      url: "https://got7ro.mybluemix.net/index.php/obtentionToken?login="+$('#loginToken').val()+"&pwd="+$('#pwdToken').val(),
      success: function(data){
          localStorage.setItem('token', data);
          $('#resultToken').html(data);
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $("#formToken").addClass("has-danger");
        $("#pwdToken").addClass("form-control-danger");
      }
    });
  });

  $('#btnTestToken').click(function(){
    $.ajax({
      type: "POST",
      url: "https://got7ro.mybluemix.net/index.php/token?token=" + localStorage.getItem('token'),
      success: function(){
        $('#resultVerifToken').html("Le token est valide.");
      },
      error: function(){
        $('#resultVerifToken').html('Le token est invalide.');
      }
    });
  });

  function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    };


  $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        url: "https://got7ro.mybluemix.net/index.php/statsHouse",
        success: function(data){
            nbPersoByHouse = data;
            nbPersoByHouseParse = JSON.parse(data);
        }
    });

    $('#btnStats').click(function(){
      var nombre = new Array();
      var nom = new Array();
      var backgroundColor = new Array();

      for (var i = 0; i < nbPersoByHouseParse.length; i++) {
        nombre[i] = nbPersoByHouseParse[i].nb;
        nom[i] = nbPersoByHouseParse[i].name;
        backgroundColor[i] = getRandomColor();
      }

      var ctx = document.getElementById("graphHouse").getContext('2d');
      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: nom,
              datasets: [{
                  label: 'Nombre de personnages par maison',
                  data: nombre,
                  backgroundColor: backgroundColor
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    });

    $.ajax({
      type: "GET",
      contentType: 'application/json; charset=utf-8',
      url: "https://got7ro.mybluemix.net/index.php/statsCities",
      success: function(data){
        nbCityByType = data;
        nbCityByTypeParse = JSON.parse(data);
      }
    });

    $('#btnStatsCities').click(function(){
      var nombre = new Array();
      var nom = new Array();
      var backgroundColor = new Array();

      for (var i = 0; i < nbCityByTypeParse.length; i++) {
        nombre[i] = nbCityByTypeParse[i].nb;
        nom[i] = nbCityByTypeParse[i].name;
        backgroundColor[i] = getRandomColor();
      }

      var ctx = document.getElementById("graphCity").getContext('2d');
      var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
              labels: nom,
              datasets: [{
                  label: 'Nombre de ville par type',
                  data: nombre,
                  backgroundColor: backgroundColor
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    });

    $.ajax({
      type: "GET",
      contentType: 'application/json; charset=utf-8',
      url: "https://got7ro.mybluemix.net/index.php/statsPerso",
      success: function(data){
        nbPerso = data;
        nbPersoParse = JSON.parse(data);
      }
    });

    $('#btnStatsPerso').click(function(){
      var nombre = new Array();
      var backgroundColor = new Array();

      for (var i = 0; i < nbPersoParse.length; i++) {
        nombre[i] = nbPersoParse[i].nb;
        backgroundColor[i] = getRandomColor();
      }

      var ctx = document.getElementById("graphPerso").getContext('2d');
      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: ['-100 -> 0', '1 -> 100', '101 -> 200', '201 -> 300'],
              datasets: [{
                  label: 'Nombre de personnages par tranche de date de naissance',
                  data: nombre,
                  backgroundColor: backgroundColor
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    });
});
