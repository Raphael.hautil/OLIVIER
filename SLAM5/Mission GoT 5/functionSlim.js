$(document).ready(function() {
  $('#btn-valide').click(function(){
    $.ajax({
      type: "GET",
      contentType: 'application/json; charset=utf-8',
      url: "https://got5ro.mybluemix.net/index.php/zaza",
      success: function(data){
        $("#result").html(data);
      }
    });
  });

  $('#btn2').click(function(){
    $('#table').bootstrapTable({
      url: "https://got5ro.mybluemix.net/index.php/personnage/"+$('#idx').val(),
      columns: [{
        field: 'name',
        title: 'Nom'
      },{
        field: 'id',
        title: 'Id'
      },]
    });
  });

  $('#btn3').click(function(){
    $.ajax({
      url: "https://got5ro.mybluemix.net/index.php/user?login="+$('#login').val()+"&pwd="+$('#pw').val(),
      type: "GET",
      success: function(resultat){
        var mail = resultat;
        $('#result2').html(mail);
      }
    });
  });

  $('#btn4').click(function(){
    $.ajax({
      type: "POST",
      contentType: 'application/json; charset=utf-8',
      url: "https://got5ro.mybluemix.net/index.php/user?login="+$('#login2').val()+"&email="+$('#email').val()+"&pwd="+$('#pw2').val(),
      success: function(data){
        $('#result3').html(data);
      }
    });
  });

  $('#btnDel').click(function(){
    $.ajax({
      type: "DELETE",
      url: "https://got5ro.mybluemix.net/index.php/user/"+$('#idDel').val(),
      success: function(data){
        $('#resultDel').html(data);
      }
    });
  });

  $('#btnNewMail').click(function(){
    $.ajax({
      type: "PUT",
      url: "https://got5ro.mybluemix.net/index.php/user?login="+$('#loginNewMail').val()+"&email="+$('#newMail').val(),
      success: function(data){
        $('#resultNewMail').html(data);
      }
    });
  });

  $('#btnPersos').click(function(){
    $('#resultPersos').bootstrapTable({
      url: "https://got5ro.mybluemix.net/index.php/persos",
      columns: [{
        field: 'name',
        title: 'Nom'
      },{
        field: 'titles',
        title: 'Titre'
      },{
        field: 'culture',
        title: 'Culture'
      },]
    });
  });
});
