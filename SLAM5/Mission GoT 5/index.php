<?php
require 'vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Firebase\JWT\JWT;

$app = new \Slim\App;
$app->get('/zaza', function(Request $request, Response $response){
	return "Bonjour Monsieur !";
});

$app->get('/personnage/{name}', function(Request $request, Response $response){
	$name = $request->getAttribute('name');
       return getPersonnage($name);
});
function connexion(){
	return $dbh = new PDO("mysql:host=sl-us-south-1-portal.33.dblayer.com:56245;dbname=got", 'admin', 'ZEWHORDIADVLJPDZ', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
}
function getPersonnage($name){
	$sql = "SELECT * FROM characters WHERE name = '".$name."'";
	try{
		$dbh=connexion();
		$statement = $dbh->prepare($sql);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_CLASS);
		return json_encode($result, JSON_PRETTY_PRINT);
	} catch(PDOException $e){
		return '{"error":'.$e->getMessage().'}';
	}
}

$app->get('/user', function(Request $request, Response $response){
	$tb = $request->getQueryParams();
	$login = $tb["login"];
	$pw = $tb["pwd"];
	//fonction d'insertion
	return checkUser($login, $pw);
});
function checkUser($login, $pw){
	$a = 0;
	$sql = "SELECT * FROM utilisateur WHERE login = '".$login."' AND pwd = '".$pw."'";
	try{
		$dbh=connexion();
	} catch(PDOException $e){
		return '{"error":'.$e->getMessage().'}';
	}
	$statement = $dbh->prepare($sql);
	$statement->execute();
	foreach ($statement->fetchAll(PDO::FETCH_OBJ) as $ligne){
		$a = 1;
		return "Connexion réussie.<br>".$ligne->email;
	}
	if ($a == 0) {
		return "Connexion échoué.";
	}
}

$app->post('/user', function(Request $request, Response $response){
	$tb = $request->getQueryParams();
	$login = $tb['login'];
	$mail = $tb['email'];
	$pwd = $tb['pwd'];
	return insertUser($login, $mail, $pwd);
});

function insertUser($login, $mail, $pwd){
	$sql = "INSERT INTO utilisateur (login, email, pwd) VALUES ('".$login."', '".$mail."', '".$pwd."');";
	try {
			$dbh=connexion();
			$statement = $dbh->prepare($sql);
			$statement->execute();
			$result = $statement->fetchAll(PDO::FETCH_CLASS);
			return "Inscription réussie !";
		}
	catch(PDOException $e){
		return '{"error":'.$e->getMessage().'}';
	}
}

$app->delete('/user/{id}', function(Request $request, Response $response){
	$id = $request->getAttribute('id');
	return deleteUser($id);
});

function deleteUser($id){
	$sql = "DELETE FROM utilisateur WHERE id = ".$id.";";
	try{
		$dbh=connexion();
		$statement = $dbh->prepare($sql);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_CLASS);
		return "Suppression effectuée avec succès.";
	}
	catch(PDOException $e){
		return '{"error"}:'.$e->getMessage().'}';
	}
}

$app->put('/user', function(Request $request, Response $response){
	$tb = $request->getQueryParams();
	$login = $tb['login'];
	$newMail = $tb['email'];
	return modifMail($login,$newMail);
});

function modifMail($login,$newMail){
	$sql = "UPDATE utilisateur SET email = '".$newMail."' WHERE login = '".$login."';";
	try{
		$dbh=connexion();
		$statement = $dbh->prepare($sql);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_CLASS);
		return "Email mis à jour !";
	}
	catch(PDOException $e){
		return '{Error}:'.$e->getMessage().'}';
	}
}

$app->get('/persos', function(Request $request, Response $response){
	return getPersos();
});

function getPersos(){
	$sql = "SELECT * FROM characters LIMIT 100";
	try{
		$dbh=connexion();
		$statement = $dbh->prepare($sql);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_CLASS);
		return json_encode($result, JSON_PRETTY_PRINT);
	}
	catch(PDOException $e){
		return '{Error}:'.$e->getMessage().'}';
	}
}

$app->run();
?>
