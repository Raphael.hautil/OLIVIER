$(document).ready(function () {
  $('#btn-get-livres').click(function () {
    $('#table-livres').bootstrapTable({
      url: 'https://anapioficeandfire.com/api/books',
      columns: [{
        field: 'name',
        title: 'Nom'
      }, {
        field: 'isbn',
        title: 'Id'
      },{
        field: 'authors',
        title: 'Auteur'
      },]
    });
  });

  $('#btn-get-maisons').click(function () {
    $('#table-maisons').bootstrapTable({
      url: 'https://anapioficeandfire.com/api/houses',
      columns: [{
        field: 'name',
        title: 'Nom'
      },{
        field: 'region',
        title: 'Région'
      },{
        field: 'coatOfArms',
        title: 'devise'
      },]
    })
  })

  $('#btn-form-ville').click(function () {
    $.ajax({
      url: 'https://api.got.show/api/cities/'+$('#form-ville').val(),
      type: "GET",
      success: function(resultat){
        var name = resultat.data.name;
        var id = resultat.data._id;
        $('#table-1').html(name);
        $('#table-2').html(id);
      }
    })
  })

  $('#btn-form-perso').click(function () {
    $.ajax({
      url: 'https://api.got.show/api/characters/'+$('#form-perso').val(),
      type: "GET",
      success: function(resultat){
        var name = resultat.data.name;
        var id = resultat.data._id;
        $('#tableP-1').html(name);
        $('#tableP-2').html(id);
      }
    })
  })
});
