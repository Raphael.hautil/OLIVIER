1) DELIMITER //
CREATE PROCEDURE pet_x_joueurs()
BEGIN
DECLARE randid INT;
DECLARE petgib INT;
DECLARE nbgib INT unsigned DEFAULT ROUND(RAND()100);
WHILE nbgib > 0 DO
SELECT pets_id INTO petgib FROM pets ORDER BY RAND() LIMIT 1;
SELECT members_character_id INTO randid FROM members ORDER BY RAND() LIMIT 1;
INSERT INTO pets_members VALUES (petgib, randid);
SET nbgib = nbgib - 1;
END WHILE;
END;
//;

Pour tester :
call pet_x_joueurs();

2) DELIMITER //
DROP PROCEDURE IF EXISTS mounts_x_joueurs;
CREATE PROCEDURE mounts_x_joueurs()
BEGIN
DECLARE randid INT;
DECLARE mountgib INT;
DECLARE nbgib INT unsigned DEFAULT ROUND(RAND()100);
WHILE nbgib > 0 DO
SELECT mounts_creature_id INTO mountgib FROM mounts ORDER BY RAND() LIMIT 1;
SELECT members_character_id INTO randid FROM members ORDER BY RAND() LIMIT 1;
INSERT INTO mounts_members VALUES (mountgib, randid);
SET nbgib = nbgib - 1;
END WHILE;
END;
//;

Pour tester :
call mounts_x_joueurs;