1) Select members_character_name FROM members

2) SELECT members_character_name, members_character_level, members_character_race FROM members, faction, races
WHERE members.members_character_race = races.races_id
AND races.races_side = faction.id
AND faction.id = 2

3) SELECT members_character_name, COUNT(mounts_members.name) FROM members, mounts_members, mounts
WHERE mounts_members.name = members.members_character_id
AND mounts_members.mounts_collected_creature_id = mounts.mounts_creature_id
GROUP BY members_character_name
HAVING COUNT(mounts_members.name) > 5;

4) SELECT members_character_name, COUNT(mounts_members.name) FROM members, mounts_members, mounts
WHERE mounts_members.name = members.members_character_id
AND mounts_members.mounts_collected_creature_id = mounts.mounts_creature_id
GROUP BY members_character_name
HAVING count(mounts_members.name) >= ALL(SELECT count(mounts_members.name) FROM members, mounts_members, mounts WHERE mounts_members.name = members.members_character_id
AND mounts_members.mounts_collected_creature_id = mounts.mounts_creature_id
GROUP BY members_character_name)

5) SELECT members_character_name, COUNT(mounts_members.name) FROM members, mounts_members, mounts
WHERE mounts_members.name = members.members_character_id
AND mounts_members.mounts_collected_creature_id = mounts.mounts_creature_id
GROUP BY members_character_name
HAVING COUNT(mounts_members.name) = ALL(SELECT COUNT(*) from mounts)

6.1) SELECT members_character_name, COUNT(pets_members.id_pet) FROM members, pets_members, pets
WHERE pets_members.id_members = members.members_character_id
AND pets_members.id_pet = pets.pets_id
GROUP BY members_character_name
HAVING COUNT(pets_members.id_pet) >= ALL(SELECT COUNT(pets_members.id_pet) FROM members, pets_members, pets WHERE pets_members.id_members = members.members_character_id
AND pets_members.id_pet = pets.pets_id
GROUP BY members_character_name)

6.2) SELECT members_character_name, COUNT(pets_members.id_pet) FROM members, pets_members, pets
WHERE pets_members.id_members = members.members_character_id
AND pets_members.id_pet = pets.pets_id
GROUP BY members_character_name
ORDER BY COUNT(pets_members.id_pet)

6.3) SELECT members_character_name, pet_types_name, COUNT(pets_members.id_pet) FROM members, pets_members, pets, pet_types
WHERE pets_members.id_members = members.members_character_id
AND pets_members.id_pet = pets.pets_id
AND pets.pets_type_id = pet_types.pet_types_id
GROUP BY members_character_name, pet_types_name
ORDER BY members_character_name