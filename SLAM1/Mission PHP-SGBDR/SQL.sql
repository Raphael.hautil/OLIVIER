Etape 1:

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `COLLECTION`;
CREATE TABLE `COLLECTION` (
  `NUMCOL` int(4) NOT NULL,
  `DATEL` date DEFAULT NULL,
  `NOMCOL` varchar(15) DEFAULT NULL,
  `HARMONIE` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`NUMCOL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `M2_COLLECTION` (`NUMCOL`, `DATEL`, `NOMCOL`, `HARMONIE`) VALUES
(1, '2005-04-01', 'Marée Haute', 'Bleu'),
(2, '2005-04-15', 'Soleil', 'Jaune');

DROP TABLE IF EXISTS `PRODUIT`;
CREATE TABLE `PRODUIT` (
  `REFP` char(4) NOT NULL,
  `DESIGN` varchar(30) DEFAULT NULL,
  `COLOR` varchar(15) DEFAULT NULL,
  `DIM` varchar(10) DEFAULT NULL,
  `PRIXHT` float DEFAULT NULL,
  `NUMCOL` int(11) DEFAULT NULL,
  `NOMCOL` varchar(30) DEFAULT NULL,
  `HARMONIE` varchar(30) DEFAULT NULL,
  `LAUNCHDATE` date DEFAULT NULL,
  PRIMARY KEY (`REFP`),
  KEY `FKNumcol` (`NUMCOL`),
  CONSTRAINT `FKNumcol` FOREIGN KEY (`NUMCOL`) REFERENCES `COLLECTION` (`NUMCOL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `PRODUIT` (`REFP`, `DESIGN`, `COLOR`, `DIM`, `PRIXHT`, `NUMCOL`, `NOMCOL`, `HARMONIE`, `LAUNCHDATE`) VALUES
('A12',	'Chaise longue', 'Marine', '120x60', 90, 1, 'Marée Haute', 'Bleu', '2005-04-01'),
('A14',	'Serviette de bain', 'Orangé', '130x80', 65, 2, 'Soleil', 'Jaune', '2005-04-15'),
('A15',	'Coussin', 'Paille', '30x30', 12, 2, 'Soleil', 'Jaune', '2005-04-15'),
('B32', 'Parasol', 'Paille', 'diam 90', 45, 2, 'Soleil', 'Jaune', '2005-04-15'),
('B25', 'Parasol', 'Ciel', 'diam 110', 60, 1, 'Marée Haute', 'Bleu', '2005-04-01');


Etape 2:

create user 'olivir' identified by 'olivier';
grant all privileges on COLLECTION to 'olivir';
grant all privileges on PRODUIT to 'olivir';
flush privileges;


Etape 4:

UPDATE PRODUIT
set PRIXHT=PRIXHT*0.8
where DESIGN like 'Chaise longue';