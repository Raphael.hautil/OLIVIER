<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">
	<title>Commande</title>
</head>
<body>

	<?php
	require_once 'header.php';
	?>

	<form method="post" id="frmConnection" action="mission-php.php">

		<fieldset>
		<legend>Information commande</legend>
		Prénom : <input name="prenom" type="text" required required placeholder="Votre prénom"/><br />
		Nom : <input name="nom" type="text" required required placeholder="Votre nom"/><br />
		Adresse : <input name="adresse" type="text" required placeholder="Votre adresse de livraison"/><br />
		</fieldset>
	
		<fieldset>
		<legend>Produits disponibles</legend>
		<select name="maListe" size="1"> 
			<option value="Sapin">Sapin</option> 
			<option value="Téléphone">Téléphone</option> 
			<option value="Chat" selected>Chat</option>
		</select ></br>
		Quantité : <input name="quantite" type="range" min="1" max="15" required required placeholder="Quantité voulue" onchange="showValue(value)"><span id="range"></span><br />
		Couleur : <input name="couleur" type="color" required required placeholder="Couleur du produit"/><br />
		Date de livraison : <input name="date" type="date" required required placeholder="Date de livraison"/></br>
		<input type="submit" value="ok">
		</fieldset>
	</form>

</body>
</html>