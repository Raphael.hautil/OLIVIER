<?php

//Etape 2
	echo "Etape 2<br>";
	$chaine = "J'adore le piano";
	if(preg_match("/piano/", $chaine)){
		echo preg_replace("/piano/", "PIANO", $chaine);
	}
	else{
		echo "Nul :(";
	}

//Etape 3
	echo "<br><br>Etape 3";
	$chaine2 = "j'adore la programmation";
	if(preg_match("/^[aeiouy]/", $chaine2)){
		echo "<br>Bravo !";
	}
	else{
		echo "<br>Nul :(";
	}

//Etape 4
	echo "<br><br>Etape 4";
	if(preg_match("/programmation$/", $chaine2)){
		echo "<br>Bravo !";
	}
	else{
		echo "<br>Nul :(";
	}

//Etape 5
	echo "<br><br>Etape 5";
	$chaine3 = "youhou :)";
	if(preg_match("/:\)/", $chaine3)){
		echo "<br>Bravo !";
	}
	else{
		echo "<br>Nul :(";
	}

//Etape 6
	echo "<br><br>Etape 6";
	$mail = "olivir@sio.fr";
	if (preg_match("/sio.fr$/", $mail)) {
		echo "<br>L'adresse mail ", $mail, " correspond au domaine sio.fr";
	}
	else{
		echo "<br>L'adresse mail ", $mail, " ne correspond pas au domaine sio.fr";
	}
	$mail2 = "olivir@gmail.com";
	if (preg_match("/sio.fr$/", $mail2)) {
		echo "<br>L'adresse mail ", $mail2, " correspond au domaine sio.fr";
	}
	else{
		echo "<br>L'adresse mail ", $mail2, " ne correspond pas au domaine sio.fr";
	}

//Etape 7
	echo "<br><br>Etape 7<br>";
	$chaine4 = "Lili";
	if(preg_match("/Lili/", $chaine4)){
		echo $chaine4, " devient ";
		echo preg_replace("/Lili/", "Lulu", $chaine4);
	}
	else{
		echo "Nul :(";
	}
	?>