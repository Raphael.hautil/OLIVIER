/**
 * Classe: Main
 * @author: Raphaël Olivier
 * @version: 2.3
 */

package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.concurrent.atomic.AtomicReference;

public class Main extends Application {

    private Maths oui = new Maths();
    private Text txtchiffre = new Text();
    private Text txtchiffre2 = new Text();
    private Text textoperation = new Text();
    private Text textRes = new Text();
    private boolean verif = false;
    private boolean verifAdd = false;
    private boolean verifSou = false;
    private boolean verifMul = false;
    private boolean verifDiv = false;
    private boolean verifExp = false;
    private boolean verifRac = false;
    private boolean verifMod = false;
    private boolean verifExpo = false;
    private boolean verifLn = false;
    private boolean verifEgal = false;
    private final AtomicReference<String> chiffre = new AtomicReference<>("");
    private final AtomicReference<String> chiffre2 = new AtomicReference<>("");
    private double res = 0;

    /**
     * Méthode qui ajoute et affiche les chiffres des boutons appuyés dans une chaîne de caractères
     *
     * @param bouton bouton appuyé
     */

    private void addChiffre(Button bouton){
        bouton.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event) {
                if (!verif) {
                    chiffre.set(chiffre + bouton.getText());
                    txtchiffre.setText(String.valueOf(chiffre));
                }else {
                    chiffre2.set(chiffre2 + bouton.getText());
                    txtchiffre2.setText(String.valueOf(chiffre2));
                }
            }
        });
    }

    @Override
    public void start(Stage primaryStage) throws Exception{

        primaryStage.setTitle("Calculatrice");
        VBox box = new VBox(2);
        GridPane grid = new GridPane();
        GridPane gridAff = new GridPane();

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(80,35,25,25));

        gridAff.setAlignment(Pos.CENTER);
        gridAff.setVgap(10);
        gridAff.setHgap(10);
        gridAff.setPadding(new Insets(10,10,10,10));

        Scene scene = new Scene(box, 700, 450);
        primaryStage.setScene(scene);
        box.getChildren().add(grid);

        Button b0 = new Button("0");
        grid.add(b0,2,4);

        Button b1 = new Button("1");
        grid.add(b1, 2,3);

        Button b2 = new Button("2");
        grid.add(b2, 3,3);

        Button b3 = new Button("3");
        grid.add(b3,4,3);

        Button b4 = new Button("4");
        grid.add(b4,2,2);

        Button b5 = new Button("5");
        grid.add(b5,3,2);

        Button b6 = new Button("6");
        grid.add(b6,4,2);

        Button b7 = new Button("7");
        grid.add(b7,2,1);

        Button b8 = new Button("8");
        grid.add(b8,3,1);

        Button b9 = new Button("9");
        grid.add(b9, 4,1);

        Button bég = new Button("=");
        grid.add(bég, 5,4);

        Button bdiv = new Button("/");
        grid.add(bdiv,5,0);

        Button bmul = new Button("*");
        grid.add(bmul,5,1);

        Button badd = new Button("+");
        grid.add(badd, 5,2);

        Button bsou = new Button("-");
        grid.add(bsou,5,3);

        Button bc = new Button("C");
        grid.add(bc,4,4);

        Button bpoint = new Button(".");
        grid.add(bpoint,3,4);

        Button bexp = new Button("^");
        grid.add(bexp, 4,0);

        Button brac = new Button("√");
        grid.add(brac,3,0);

        Button bmod = new Button("%");
        grid.add(bmod,2,0);

        Button bexpo = new Button("e");
        grid.add(bexpo,6,0);

        Button bln = new Button("ln");
        grid.add(bln, 6,1);

        addChiffre(b0);
        addChiffre(b1);
        addChiffre(b2);
        addChiffre(b3);
        addChiffre(b4);
        addChiffre(b5);
        addChiffre(b6);
        addChiffre(b7);
        addChiffre(b8);
        addChiffre(b9);
        addChiffre(bpoint);


        gridAff.add(txtchiffre,0,0);
        gridAff.add(txtchiffre2,2,0);

        badd.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (verifAdd || verifSou || verifMul || verifDiv || verifExp || verifRac || verifMod || verifExpo || verifLn){
                    verif = true;
                    verifSou = false;
                    verifMul = false;
                    verifDiv = false;
                    verifExp = false;
                    verifRac = false;
                    verifMod = false;
                    verifExpo = false;
                    verifLn = false;
                    verifAdd = true;
                    chiffre.set(String.valueOf(res));
                    txtchiffre.setText(String.valueOf(chiffre));
                    chiffre2.set("");
                    txtchiffre2.setText(String.valueOf(chiffre2));
                    textRes.setText("");
                    textoperation.setText("+");
                }else {
                    textoperation.setText("+");
                    verif = true;
                    verifAdd = true;
                }
            }
        });

        bsou.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (verifAdd || verifSou || verifMul || verifDiv || verifExp || verifRac || verifMod || verifExpo || verifLn){
                    verif = true;
                    verifAdd = false;
                    verifMul = false;
                    verifDiv = false;
                    verifExp = false;
                    verifRac = false;
                    verifLn = false;
                    verifExpo = false;
                    verifMod = false;
                    verifSou = true;
                    chiffre.set(String.valueOf(res));
                    txtchiffre.setText(String.valueOf(chiffre));
                    chiffre2.set("");
                    txtchiffre2.setText(String.valueOf(chiffre2));
                    textRes.setText("");
                    textoperation.setText("-");
                }else {
                    textoperation.setText("-");
                    verif = true;
                    verifSou = true;
                }
            }
        });

        bmul.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (verifAdd || verifSou || verifMul || verifDiv || verifExp || verifRac || verifMod || verifExpo || verifLn){
                    verif = true;
                    verifSou = false;
                    verifAdd = false;
                    verifDiv = false;
                    verifExp = false;
                    verifRac = false;
                    verifMod = false;
                    verifExpo = false;
                    verifLn = false;
                    verifMul = true;
                    chiffre.set(String.valueOf(res));
                    txtchiffre.setText(String.valueOf(chiffre));
                    chiffre2.set("");
                    txtchiffre2.setText(String.valueOf(chiffre2));
                    textRes.setText("");
                    textoperation.setText("*");
                }else {
                    textoperation.setText("*");
                    verif = true;
                    verifMul = true;
                }
            }
        });

        bdiv.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (verifAdd || verifSou || verifMul || verifDiv || verifExp || verifRac || verifMod || verifExpo || verifLn){
                    verif = true;
                    verifSou = false;
                    verifMul = false;
                    verifAdd = false;
                    verifExp = false;
                    verifRac = false;
                    verifLn = false;
                    verifExpo = false;
                    verifMod = false;
                    verifDiv = true;
                    chiffre.set(String.valueOf(res));
                    txtchiffre.setText(String.valueOf(chiffre));
                    chiffre2.set("");
                    txtchiffre2.setText(String.valueOf(chiffre2));
                    textRes.setText("");
                    textoperation.setText("/");
                }else {
                    textoperation.setText("/");
                    verif = true;
                    verifDiv = true;
                }
            }
        });

        bexp.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (verifAdd || verifSou || verifMul || verifDiv || verifExp || verifRac || verifMod || verifExpo || verifLn){
                    verif = true;
                    verifSou = false;
                    verifMul = false;
                    verifDiv = false;
                    verifAdd = false;
                    verifRac = false;
                    verifExpo = false;
                    verifLn = false;
                    verifMod = false;
                    verifExp = true;
                    chiffre.set(String.valueOf(res));
                    txtchiffre.setText(String.valueOf(chiffre));
                    chiffre2.set("");
                    txtchiffre2.setText(String.valueOf(chiffre2));
                    textRes.setText("");
                    textoperation.setText("^");
                }else {
                    textoperation.setText("^");
                    verif = true;
                    verifExp = true;
                }
            }
        });

        brac.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (verifAdd || verifSou || verifMul || verifDiv || verifExp || verifRac || verifMod || verifExpo || verifLn){
                    verif = true;
                    verifAdd = false;
                    verifSou = false;
                    verifMul = false;
                    verifDiv = false;
                    verifExp = false;
                    verifLn = false;
                    verifExpo = false;
                    verifMod = false;
                    verifRac = true;
                    chiffre.set(String.valueOf(res));
                    txtchiffre.setText(String.valueOf(chiffre));
                    chiffre2.set("");
                    txtchiffre2.setText(String.valueOf(chiffre2));
                    textRes.setText("");
                    textoperation.setText("√");
                }else {
                    textoperation.setText("√");
                    verif = true;
                    verifRac = true;
                }
                double a = Double.parseDouble(chiffre.get());
                res = oui.racineCarre(a);
                textRes.setText(String.valueOf(res));
            }
        });

        bmod.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (verifAdd || verifSou || verifMul || verifDiv || verifExp || verifRac || verifMod || verifExpo || verifLn){
                    verif = true;
                    verifAdd = false;
                    verifSou = false;
                    verifMul = false;
                    verifDiv = false;
                    verifExp = false;
                    verifRac = false;
                    verifExpo = false;
                    verifLn = false;
                    verifMod = true;
                    chiffre.set(String.valueOf(res));
                    txtchiffre.setText(String.valueOf(chiffre));
                    chiffre2.set("");
                    txtchiffre2.setText(String.valueOf(chiffre2));
                    textRes.setText("");
                    textoperation.setText("%");
                }else {
                    textoperation.setText("%");
                    verif = true;
                    verifMod = true;
                }
            }
        });

        bexpo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (verifAdd || verifSou || verifMul || verifDiv || verifExp || verifRac || verifMod || verifExpo || verifLn){
                    verif = true;
                    verifAdd = false;
                    verifSou = false;
                    verifMul = false;
                    verifDiv = false;
                    verifExp = false;
                    verifRac = false;
                    verifLn = false;
                    verifExpo = true;
                    chiffre.set(String.valueOf(res));
                    txtchiffre.setText(String.valueOf(chiffre));
                    chiffre2.set("");
                    txtchiffre2.setText(String.valueOf(chiffre2));
                    textRes.setText("");
                    textoperation.setText("e");
                }else {
                    textoperation.setText("e");
                    verif = true;
                    verifExpo = true;
                }
                double a = Double.parseDouble(chiffre.get());
                res = oui.exponentielle(a);
                textRes.setText(String.valueOf(res));
            }
        });

        bln.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (verifAdd || verifSou || verifMul || verifDiv || verifExp || verifRac || verifMod || verifExpo || verifLn){
                    verif = true;
                    verifAdd = false;
                    verifSou = false;
                    verifMul = false;
                    verifDiv = false;
                    verifExp = false;
                    verifRac = false;
                    verifExpo = false;
                    verifLn = true;
                    chiffre.set(String.valueOf(res));
                    txtchiffre.setText(String.valueOf(chiffre));
                    chiffre2.set("");
                    txtchiffre2.setText(String.valueOf(chiffre2));
                    textRes.setText("");
                    textoperation.setText("ln");
                }else {
                    textoperation.setText("ln");
                    verif = true;
                    verifExpo = true;
                }
                double a = Double.parseDouble(chiffre.get());
                res = oui.logarithmeNeperien(a);
                textRes.setText(String.valueOf(res));
            }
        });

        bc.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                textRes.setText("");
                textoperation.setText("");
                txtchiffre.setText("");
                txtchiffre2.setText("");
                verifDiv = false;
                verifMul = false;
                verifSou = false;
                verifAdd = false;
                verifExp = false;
                verifRac = false;
                verifMod = false;
                verifExpo = false;
                verifLn = false;
                verif = false;
                verifEgal = false;
                chiffre.set("");
                chiffre2.set("");
            }
        });

        bég.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (verifEgal){
                    chiffre.set(String.valueOf(res));
                    txtchiffre.setText(String.valueOf(chiffre));
                }
                double a = Double.parseDouble(chiffre.get());
                double b = Double.parseDouble(chiffre2.get());
                if (verifAdd){
                    res = oui.addition(a,b);
                }else if (verifSou){
                    res = oui.soustraction(a,b);
                }else if (verifMul){
                    res = oui.multiplication(a,b);
                }else if (verifDiv){
                    res = oui.division(a,b);
                }else if (verifExp){
                    res = oui.exposant(a,b);
                }else if (verifMod){
                    res = oui.modulo(a,b);
                }
                textRes.setText(String.valueOf(res));
                verifEgal = true;
            }
        });

        gridAff.add(textoperation,1,0);
        gridAff.add(textRes, 3,1);
        box.getChildren().add(gridAff);

        scene.getStylesheets().add(Main.class.getResource("style.css").toExternalForm());
        primaryStage.show();
    }

    public static void main(String[] args){
        launch(args);
    }
}