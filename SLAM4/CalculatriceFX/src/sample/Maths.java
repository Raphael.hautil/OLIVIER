/**
 * Classe: Maths
 * @author: Raphaël Olivier
 * @version: 1.1
 */

package sample;

class Maths{
    private double res;

    /**
     * Création d'un constructeur vide
     */

    Maths(){}

    /**
     * Méthode qui additionne deux nombres
     *
     * @param a premier nombre
     * @param b deuxième nombre
     * @return res Somme des deux nombres
     */

    double addition(double a, double b){
        res = a + b;
        return res;
    }

    /**
     * Méthode qui soustrait deux nombres
     *
     * @param a premier nombre
     * @param b deuxième nombre
     * @return res Différence des deux nombres
     */

    double soustraction(double a, double b){
        res = a - b;
        return res;
    }

    /**
     * Méthode qui multiplie deux nombres
     *
     * @param a premier nombre
     * @param b deuxième nombre
     * @return res Produit des deux nombres
     */

    double multiplication(double a, double b){
        res = a * b;
        return res;
    }

    /**
     * Méthode qui divise deux nombres
     *
     * @param a premier nombre
     * @param b deuxième nombre
     * @return res Quotient des deux nombres
     */

    double division(double a, double b){
        res = a / b;
        return res;
    }

    /**
     * Méthode qui calcule la puissance de deux nombres
     *
     * @param a premier nombre
     * @param b deuxième nombre
     * @return res Puissance des deux nombres
     */

    double exposant(double a, double b){
        res = Math.pow(a,b);
        return res;
    }

    /**
     * Méthode qui calcule la racine carrée d'un nombre
     *
     * @param a nombre
     * @return res Racine carrée du nombre
     */

    double racineCarre(double a){
        res = Math.sqrt(a);
        return res;
    }

    /**
     * Méthode qui calcule le reste de la division euclidienne de deux nombres
     *
     * @param a premier nombre
     * @param b deuxième nombre
     * @return res Reste de la division euclidienne
     */

    double modulo(double a, double b){
        res = a % b;
        return res;
    }

    /**
     * Méthode qui calcule l'exponentielle d'un nombre
     *
     * @param a nombre
     * @return res Exponentielle du nombre
     */

    double exponentielle(double a){
        res = Math.exp(a);
        return res;
    }

    /**
     * Méthode qui calcule le logarithme népérien d'un nombre
     *
     * @param a nombre
     * @return res Logarithme népérien du nombre
     */

    double logarithmeNeperien (double a){
        res = Math.log(a);
        return res;
    }
}
