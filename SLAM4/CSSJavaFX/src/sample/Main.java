package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setTitle("Hello World");
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25,25,25,25));
        Scene scene = new Scene(grid, 700, 450);
        Text scenetitle = new Text("Salut !");
        scenetitle.setId("id_scenetitle");
        grid.add(scenetitle, 0, 0, 2, 1);

        Label text = new Label("Text:");
        grid.add(text, 0, 1);

        TextField txtfield = new TextField();
        grid.add(txtfield, 1, 1);

        primaryStage.setScene(scene);

        Button btnMaj = new Button("Majuscule");
        Button btnMin = new Button("Minuscule");

        grid.add(btnMaj, 0,3);
        grid.add(btnMin,1,3);

        final Text actiontarget = new Text();
        actiontarget.setId("id_actiontarget");
        grid.add(actiontarget, 1, 6);

        btnMaj.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent e) {
                actiontarget.setFill(Color.BLUEVIOLET);
                actiontarget.setText(txtfield.getText().toUpperCase());
                txtfield.setText("");
            }
        });

        btnMin.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event) {
                actiontarget.setText(txtfield.getText().toLowerCase());
                txtfield.setText("");
            }
        });
        scene.getStylesheets().add(Main.class.getResource("style.css").toExternalForm());
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
