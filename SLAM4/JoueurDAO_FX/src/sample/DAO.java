/**
 * Classe: DAO
 * @author: Raphaël Olivier
 * @version; 1.0
 */

package sample;

import java.sql.*;

public class DAO {
    private String driver = "com.mysql.cj.jdbc.Driver";
    private String url = "jdbc:mysql://sl-us-south-1-portal.33.dblayer.com:56245/SLAM4_JoueurFoot?useSSL=false";
    private String user="admin";
    private String pwd = "ZEWHORDIADVLJPDZ";

    /**
     * Méthode qui permet de se connecter à la base de données
     *
     * @return conn
     */

    protected Connection getConnection(){
        Connection conn = null;
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url,user,pwd);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }
}

