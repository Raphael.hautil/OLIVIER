/**
 * Classe: Main
 * @author: Raphaël Olivier
 * @version: 1.1
 */

package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;

public class Main extends Application {

    private GridPane grid = new GridPane();
    private Scene scene = new Scene(grid,700,450);
    private Label text = new Label("Id");
    private Button btnRes = new Button("Valider");
    private ComboBox textId = new ComboBox();
    private VBox vbox = new VBox(5);

    private JoueurDAO jDao = new JoueurDAO();
    private Joueur res = new Joueur();

    @Override
    public void start(Stage primaryStage){
        primaryStage.setTitle("JoueurDAO_FX");
        
        ArrayList<Joueur> listJ = jDao.getJoueurs();
        for (Joueur j:listJ) {
            textId.getItems().addAll(j.getId());
        }

        grid.setAlignment(Pos.CENTER_LEFT);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25,25,25,25));

        primaryStage.setScene(scene);
        primaryStage.show();

        grid.add(textId,1,1);

        grid.add(text,0,1);

        grid.add(btnRes,2,1);

        grid.add(vbox,4,2);

        Text idJoueur = new Text("");
        vbox.getChildren().add(idJoueur);

        Text nomJoueur = new Text();
        vbox.getChildren().add(nomJoueur);

        Text prenomJoueur = new Text();
        vbox.getChildren().add(prenomJoueur);

        Text posteJoueur = new Text();
        vbox.getChildren().add(posteJoueur);

        Text numeroJoueur = new Text();
        vbox.getChildren().add(numeroJoueur);

        Text clubJoueur = new Text();
        vbox.getChildren().add(clubJoueur);

        Text dateDeNaissanceJoueur = new Text();
        vbox.getChildren().add(dateDeNaissanceJoueur);

        Text nationaliteJoueur = new Text();
        vbox.getChildren().add(nationaliteJoueur);

        Region imageJoueur = new Region();
        imageJoueur.setId("image");
        grid.add(imageJoueur, 2,2);

        btnRes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String id = textId.getValue().toString();
                res = jDao.getJoueurById(id);
                if (res != null) {
                    idJoueur.setText(res.getId());
                    String image = Main.class.getResource("img/" + res.getId() + ".png").toExternalForm();
                    imageJoueur.setStyle("-fx-background-image: url('" + image + "');" + "-fx-background-size: 300;");

                    idJoueur.setText("Id : " + res.getId());
                    nomJoueur.setText("Nom : " + res.getNom());
                    prenomJoueur.setText("Prénom : " + res.getPrenom());
                    posteJoueur.setText("Poste : " + res.getPoste());
                    numeroJoueur.setText("Numéro : " + res.getNumero());
                    clubJoueur.setText("Club : " + res.getClub());
                    dateDeNaissanceJoueur.setText("Date de naissance : " + res.getDateNaissance());
                    nationaliteJoueur.setText("Nationalité : " + res.getNationnalite());
                }else{
                    idJoueur.setText("");
                    nomJoueur.setText("Aucun joueurs trouvé");
                    prenomJoueur.setText("Essayer un autre Id");
                    posteJoueur.setText("");
                    numeroJoueur.setText("");
                    clubJoueur.setText("");
                    dateDeNaissanceJoueur.setText("");
                    nationaliteJoueur.setText("");
                    imageJoueur.setStyle("-fx-background-size: 0%;");
                }
            }
        });
        scene.getStylesheets().add(Main.class.getResource("style.css").toExternalForm());
    }
}
