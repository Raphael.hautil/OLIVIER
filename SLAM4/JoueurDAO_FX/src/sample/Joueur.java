/**
 * Classe: Joueur
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package sample;

import java.util.Date;

public class Joueur {
    private String id;
    private String nom;
    private String prenom;
    private String poste;
    private int numero;
    private String club;
    private Date dateNaissance;
    private String nationnalite;

    /**
     * Méthode qui créé un joueur vide
     */

    public Joueur(){

    }

    /**
     * Méthode qui créé un joueur avec toutes ses informations
     *
     * @param id id du joueur
     * @param nom nom du joueur
     * @param prenom prénom du joueur
     * @param poste poste du joueur
     * @param numero numéro du joueur
     * @param club club du joueur
     * @param dateNaissance date de naissance du joueur
     * @param nationnalite nationalité du joueur
     */


    public Joueur(String id, String nom, String prenom, String poste, int numero, String club, Date dateNaissance, String nationnalite){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.poste = poste;
        this.numero = numero;
        this.club = club;
        this.dateNaissance = dateNaissance;
        this.nationnalite = nationnalite;
    }

    /**
     * Méthode qui récupère et retourne l'id du joueur
     *
     * @return id
     */

    public String getId(){
        return id;
    }

    /**
     * Méthode qui récupère et retourne le nom du joueur
     *
     * @return nom
     */

    public String getNom(){
        return nom;
    }

    /**
     * Méthode qui récupère et retourne le prénom du joueur
     *
     * @return prenom
     */

    public String getPrenom(){
        return prenom;
    }

    /**
     * Méthode qui récupère et retourne le poste du joueur
     *
     * @return poste
     */

    public String getPoste(){
        return poste;
    }

    /**
     * Méthode qui récupère et retourne le numéro du joueur
     *
     * @return numero
     */

    public int getNumero(){
        return numero;
    }

    /**
     * Méthode qui récupère et retourne le club du joueur
     *
     * @return club
     */

    public String getClub(){
        return club;
    }

    /**
     * Méthode qui récupère et retourne la date de naissance du joueur
     *
     * @return dateNaissance
     */

    public Date getDateNaissance(){
        return dateNaissance;
    }

    /**
     * Méthode qui récupère et retourne la nationalité du joueur
     *
     * @return nationnalite
     */

    public String getNationnalite(){
        return nationnalite;
    }

    /**
     * Méthode qui change le nom du joueur
     *
     * @param nom nom du joueur
     */

    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Méthode qui change l'id du joueur
     *
     * @param id id du joueur
     */

    public void setId(String id) {
        this.id = id;
    }

    /**
     * Méthode qui change le prénom du joueur
     *
     * @param prenom prenom du joueur
     */

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Méthode qui change le poste du joueur
     *
     * @param poste poste du joueur
     */

    public void setPoste(String poste) {
        this.poste = poste;
    }

    /**
     * Méthode qui change le numéro du joueur
     *
     * @param numero numéro du joueur
     */

    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * Méthode qui change le club du joueur
     *
     * @param club club du joueur
     */

    public void setClub(String club) {
        this.club = club;
    }

    /**
     * Méthode qui change la date de naissance du joueur
     *
     * @param dateNaissance date de naissance du joueur
     */

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    /**
     * Méthode qui change la nationalité du joueur
     *
     * @param nationnalite nationalité du joueur
     */

    public void setNationnalite(String nationnalite){
        this.nationnalite = nationnalite;
    }

    /**
     * Méthode qui affiche tous les paramètres du joueur
     *
     * @return id
     * @return prenom
     * @return nom
     * @return poste
     * @return numero
     * @return club
     * @return dateNaissance
     * @return nationnalite
     */

    @Override
    public String toString() {
        return " \nJoueur :"+
                " \nid :" + id +
                " \nprénom : " + prenom +
                " \nnom :" + nom +
                " \nposte : " + poste +
                " \nnuméro : " + numero +
                " \nclub : " + club +
                " \ndate de naissance : " + dateNaissance +
                " \nnationnalité : " + nationnalite + " ";
    }
}
