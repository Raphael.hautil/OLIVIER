/**
 * Classe: JoueurDAO
 * @author: Raphaël Olivier
 * @version: 2.2
 */

package sample;

import java.sql.*;
import java.util.ArrayList;

public class JoueurDAO extends DAO {

    /**
     * Méthode qui permet de rechercher un joueur dans la base de données en fonction de son id
     *
     * @param x id du joueur
     * @return res
     */


    public Joueur getJoueurById(String x) {
        Joueur res = null;
        try {
            Connection conn = super.getConnection();
            String req = "SELECT * FROM SLAM4_JoueurFoot WHERE id = ?";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1, x);
            ResultSet result = pstmt.executeQuery();
            while (result.next()) {
                res = new Joueur();
                res.setId(result.getString(1));
                res.setNom(result.getString(2));
                res.setPrenom(result.getString(3));
                res.setPoste(result.getString(4));
                res.setNumero(result.getInt(5));
                res.setClub(result.getString(6));
                res.setDateNaissance(result.getDate(7));
                res.setNationnalite(result.getString(8));
            }
            result.close();
            pstmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Méthode qui permet d'afficher tous les joueurs de la base de données
     *
     * @return list
     */

    public ArrayList<Joueur> getJoueurs() {
        ArrayList<Joueur> list = new ArrayList<>();
        Joueur res;
        Connection conn;
        try {
            conn = super.getConnection();
            Statement stmt = conn.createStatement();
            String req = "SELECT * FROM SLAM4_JoueurFoot";
            ResultSet result = stmt.executeQuery(req);
            while (result.next()) {
                res = new Joueur();
                res.setId(result.getString(1));
                res.setNom(result.getString(2));
                res.setPrenom(result.getString(3));
                res.setPoste(result.getString(4));
                res.setNumero(result.getInt(5));
                res.setClub(result.getString(6));
                res.setDateNaissance(result.getDate(7));
                res.setNationnalite(result.getString(8));
                list.add(res);
            }
            result.close();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
