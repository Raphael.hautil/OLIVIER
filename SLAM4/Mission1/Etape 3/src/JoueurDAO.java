/**
 * Classe: JoueurDAO
 * @author: Raphaël Olivier
 * @version: 1.0
 */

import java.sql.*;
import java.util.ArrayList;

public class JoueurDAO {

    /**
     * Méthode qui permet de se connecter à la base de données
     *
     * @return conn
     */

    private Connection getConnection(){
        Connection conn = null;
        String driver = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mariadb://sl-us-south-1-portal.33.dblayer.com:56245/SLAM4_JoueurFoot";
        String user="admin";
        String pwd = "ZEWHORDIADVLJPDZ";
        try{
            Class.forName(driver);
            System.out.println("driver ok");
            conn = DriverManager.getConnection(url,user,pwd);
            System.out.println("connection ok");
        }catch (Exception e){
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * Méthode qui permet d'ajouter un joueur dans la base de données
     *
     * @param j Joueur
     */


    public void insertJoueur(Joueur j){
        try{
            Connection conn = getConnection();
            String req1 = "INSERT INTO SLAM4_JoueurFoot VALUES (?,?,?,?,?,?,?,?)";
            PreparedStatement pstmt = conn.prepareStatement(req1);

            pstmt.setString(1, j.getId());

            pstmt.setString(2, j.getNom());

            pstmt.setString(3, j.getPrenom());

            pstmt.setString(4, j.getPoste());

            pstmt.setInt(5, j.getNumero());

            pstmt.setString(6, j.getClub());

            pstmt.setDate(7, new Date(j.getDateNaissance().getTime()));

            pstmt.setString(8, j.getNationnalite());

            int res1 = pstmt.executeUpdate();
            System.out.println("Nombre de lignes modifiées : " + res1);
            pstmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Méthode qui permet de supprimer un joueur de la base de données
     *
     * @param j Joueur
     */


    public void deleteJoueur(Joueur j){
        try{
            Connection conn = getConnection();
            String req1 = "DELETE FROM SLAM4_JoueurFoot WHERE nom = ?";
            PreparedStatement pstmt = conn.prepareStatement(req1);

            pstmt.setString(1, j.getNom());

            int res = pstmt.executeUpdate();
            System.out.println("Nombre de lignes modifiées : " + res);
            pstmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Méthode qui permet de rechercher un joueur dans la base de données en fonction de son id
     *
     * @param x id du joueur
     * @return res
     */


    public ArrayList<Joueur> getJoueurByPoste(String x){
        ArrayList<Joueur> list = new ArrayList<>();
        Joueur res;
        try{
            Connection conn = getConnection();
            String req = "SELECT * FROM SLAM4_JoueurFoot WHERE id = ?";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1, x);
            ResultSet result = pstmt.executeQuery();
            while(result.next()){
                res = new Joueur();
                res.setId(result.getString(1));
                res.setNom(result.getString(2));
                res.setPrenom(result.getString(3));
                res.setPoste(result.getString(4));
                res.setNumero(result.getInt(5));
                res.setClub(result.getString(6));
                res.setDateNaissance(result.getDate(7));
                res.setNationnalite(result.getString(8));
                list.add(res);
            }
            result.close();
            pstmt.close();
        }catch (Exception  e){
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Méthode qui permet d'afficher tous les joueurs de la base de données
     *
     * @return list
     */

    public ArrayList<Joueur> getJoueurs(){
        ArrayList<Joueur> list = new ArrayList<>();
        Joueur res;
        try{
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        String req = "SELECT * FROM SLAM4_JoueurFoot";
        ResultSet result = stmt.executeQuery(req);
        while(result.next()){
            res = new Joueur();
            res.setId(result.getString(1));
            res.setNom(result.getString(2));
            res.setPrenom(result.getString(3));
            res.setPoste(result.getString(4));
            res.setNumero(result.getInt(5));
            res.setClub(result.getString(6));
            res.setDateNaissance(result.getDate(7));
            res.setNationnalite(result.getString(8));
            list.add(res);
        }
        result.close();
        stmt.close();
    }catch(Exception e){
        e.printStackTrace();
    }
    return list;
    }
}