import java.sql.*;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args){
        Connection conn = null;
        String driver = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mariadb://sio-hautil.eu:3306/olivir";
        String user="olivir";
        String pwd = "olivier-1209";
        ArrayList<Joueur> list = new ArrayList<>();
        try {
            Class.forName(driver);
            System.out.println("driver ok");
            conn = DriverManager.getConnection(url,user,pwd);
            System.out.println("connection ok");

            Statement stmt = conn.createStatement();
            String req = "SELECT * FROM SLAM4_Y2_JoueurFoot";
            ResultSet result = stmt.executeQuery(req);
            while(result.next()){
                Joueur res = new Joueur();
                res.setId(result.getString(1));
                res.setNom(result.getString(2));
                res.setPrenom(result.getString(3));
                res.setPoste(result.getString(4));
                res.setNumero(result.getInt(5));
                res.setClub(result.getString(6));
                res.setDateNaissance(result.getDate(7));
                res.setNationnalite(result.getString(8));
                list.add(res);
            }
            System.out.println(list);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
