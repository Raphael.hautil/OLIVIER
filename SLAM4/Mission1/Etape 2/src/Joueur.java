import java.util.Date;

public class Joueur {
    private String id;
    private String nom;
    private String prenom;
    private String poste;
    private int numero;
    private String club;
    private Date dateNaissance;
    private String nationnalite;

    public Joueur(){

    }

    public Joueur(String id, String nom, String prenom, String poste, int numero, String club, Date dateNaissance, String nationnalite){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.poste = poste;
        this.numero = numero;
        this.club = club;
        this.dateNaissance = dateNaissance;
        this.nationnalite = nationnalite;
    }

    public String getId(){
        return id;
    }

    public String getNom(){
        return nom;
    }

    public String getPrenom(){
        return prenom;
    }

    public String getPoste(){
        return poste;
    }

    public int getNumero(){
        return numero;
    }

    public String getClub(){
        return club;
    }

    public Date getDateNaissance(){
        return dateNaissance;
    }

    public String getNationnalite(){
        return nationnalite;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public void setNationnalite(String nationnalite){
        this.nationnalite = nationnalite;
    }

    @Override
    public String toString() {
        return " \nJoueur :"+
                " \nid :" + id +
                " \nprénom : " + prenom +
                " \nnom :" + nom +
                " \nposte : " + poste +
                " \nnuméro : " + numero +
                " \nclub : " + club +
                " \ndate de naissance : " + dateNaissance +
                " \nnationnalité : " + nationnalite + " ";
    }
}
