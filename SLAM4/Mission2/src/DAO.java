/**
 * Classe: DAO
 * @author: Raphaël Olivier
 * @version; 1.0
 */

import java.sql.*;

public class DAO {
    String driver = "com.mysql.cj.jdbc.Driver";
    String url = "jdbc:mariadb://sl-us-south-1-portal.33.dblayer.com:56245/SLAM4_JoueurFoot";
    String user="admin";
    String pwd = "ZEWHORDIADVLJPDZ";

    /**
     * Méthode qui permet de se connecter à la base de données
     *
     * @return conn
     */

    protected Connection getConnection() throws ClassNotFoundException, SQLException{
        Connection conn;
        Class.forName(driver);
        System.out.println("driver ok");
        conn = DriverManager.getConnection(url,user,pwd);
        System.out.println("connection ok");
        return conn;
    }
}
