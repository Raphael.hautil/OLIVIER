/**
 * Classe: Main
 * @author: Raphaël Olivier
 * @version: 2.0
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Main {
    public static String menu(){
        return "Que voulez faire ? Ajouter un joueur (1)\nSupprimer un joueur (2)\nAfficher un joueur en fonction de son id (3)\nAfficher tous les joueurs(4)\nQuitter l'application (5)";
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        try{
        JoueurDAO jDao = new JoueurDAO();
        Joueur j = new Joueur();
        int choix = 0;
        String id;
        String nom;
        String prenom;
        String poste;
        int numero;
        String club;
        String dateNaissance;
        String nationnalite;
        System.out.println(Main.menu());
        choix = sc.nextInt();
        sc.nextLine();
        while(choix >=0 && choix<6) {
            if (choix == 1) {
                System.out.println("Saisir le numéro de license : ");
                id = sc.next();
                j.setId(id);
                sc.nextLine();

                System.out.println("Saisir le nom du joueur : ");
                nom = sc.nextLine();
                j.setNom(nom);

                System.out.println("Saisir le prénom du joueur : ");
                prenom = sc.nextLine();
                j.setPrenom(prenom);

                System.out.println("Saisir le poste du joueur : ");
                poste = sc.next();
                j.setPoste(poste);
                sc.nextLine();

                System.out.println("Saisir le numéro du joueur : ");
                numero = sc.nextInt();
                j.setNumero(numero);
                sc.nextLine();

                System.out.println("Saisir le club du joueur : ");
                club = sc.nextLine();
                j.setClub(club);

                System.out.println("Saisir la date de naissance du joueur : ");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                dateNaissance = sc.next();
                Date date1 = null;
                try {
                    date1 = sdf.parse(dateNaissance);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                j.setDateNaissance(date1);
                sc.nextLine();

                System.out.println("Saisir la nationnalité du joueur : ");
                nationnalite = sc.nextLine();
                j.setNationnalite(nationnalite);
                jDao.insertJoueur(j);

            }else if (choix == 2) {
                System.out.println("Saisir le nom du joueur à supprimer : ");
                nom = sc.nextLine();
                j.setNom(nom);
                jDao.deleteJoueur(j);

            }else if (choix == 3) {
                System.out.println("Saisir l'id dont vous voulez voir les joueurs : ");
                id = sc.next();
                j.setId(id);
                ArrayList<Joueur> list;
                    list = jDao.getJoueurByPoste(j.getId());
                System.out.println(list);

            }else if (choix == 4) {
                ArrayList<Joueur> list;
                    list = jDao.getJoueurs();
                for (Joueur j2 : list) {
                    System.out.println(j2);
                }

            }else if (choix == 5) {
                System.out.println("Au revoir.");
                break;
            }

            System.out.println(Main.menu());
            choix = sc.nextInt();
            sc.nextLine();
            }
        }catch (DaoException e){
            e.printStackTrace();
        }
    }
}