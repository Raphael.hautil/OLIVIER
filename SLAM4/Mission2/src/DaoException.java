/**
 * Classe: DaoException
 * @author: Raphaël Olivier
 * @version: 1.0
 */
public class DaoException extends Exception{

    /**
     * Méthode qui créé une exception avec ses paramètres
     *
     * @param message message de l'erreur
     * @param cause cause de l'erreur
     */

    public DaoException(String message, Throwable cause){
        super(message, cause);
    }
}
