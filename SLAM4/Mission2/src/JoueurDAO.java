/**
 * Classe: JoueurDAO
 * @author: Raphaël Olivier
 * @version: 2.2
 */

import java.sql.*;
import java.util.ArrayList;

public class JoueurDAO extends DAO {

    /**
     * Méthode qui permet d'ajouter un joueur dans la base de données
     *
     * @param j Joueur
     */


    public void insertJoueur(Joueur j) throws DaoException{
        Connection conn = null;
        try{
            conn = super.getConnection();
            String req1 = "INSERT INTO SLAM4_JoueurFoot VALUES (?,?,?,?,?,?,?,?)";
            PreparedStatement pstmt = conn.prepareStatement(req1);

            pstmt.setString(1, j.getId());

            pstmt.setString(2, j.getNom());

            pstmt.setString(3, j.getPrenom());

            pstmt.setString(4, j.getPoste());

            pstmt.setInt(5, j.getNumero());

            pstmt.setString(6, j.getClub());

            pstmt.setDate(7, new Date(j.getDateNaissance().getTime()));

            pstmt.setString(8, j.getNationnalite());

            int res1 = pstmt.executeUpdate();
            System.out.println("Nombre de lignes modifiées : " + res1);
            pstmt.close();
        }catch (SQLException e){
            DaoException ex = new DaoException("Problème base de données", e);
            throw ex;
        }catch (ClassNotFoundException e){
            DaoException ex = new DaoException("Problème de Driver", e);
            throw ex;
        }finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Méthode qui permet de supprimer un joueur de la base de données
     *
     * @param j Joueur
     */


    public void deleteJoueur(Joueur j) throws DaoException{
        Connection conn = null;
        try{
            conn = super.getConnection();
            String req1 = "DELETE FROM SLAM4_JoueurFoot WHERE nom = ?";
            PreparedStatement pstmt = conn.prepareStatement(req1);

            pstmt.setString(1, j.getNom());

            int res = pstmt.executeUpdate();
            System.out.println("Nombre de lignes modifiées : " + res);
            pstmt.close();
        }catch (SQLException e){
            DaoException ex = new DaoException("Problème base de données", e);
            throw ex;
        }catch (ClassNotFoundException e){
            DaoException ex = new DaoException("Problème de Driver", e);
            throw ex;
        }finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Méthode qui permet de rechercher un joueur dans la base de données en fonction de son id
     *
     * @param x id du joueur
     * @return res
     */


    public ArrayList<Joueur> getJoueurByPoste(String x) throws DaoException{
        ArrayList<Joueur> list = new ArrayList<>();
        Joueur res;
        Connection conn = null;
        try{
            conn = super.getConnection();
            String req = "SELECT * FROM SLAM4_JoueurFoot WHERE id = ?";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1, x);
            ResultSet result = pstmt.executeQuery();
            while(result.next()){
                res = new Joueur();
                res.setId(result.getString(1));
                res.setNom(result.getString(2));
                res.setPrenom(result.getString(3));
                res.setPoste(result.getString(4));
                res.setNumero(result.getInt(5));
                res.setClub(result.getString(6));
                res.setDateNaissance(result.getDate(7));
                res.setNationnalite(result.getString(8));
                list.add(res);
            }
            result.close();
            pstmt.close();
        }catch (SQLException  e){
            DaoException ex = new DaoException("Problème de base de données", e);
            throw ex;
        }catch (ClassNotFoundException e){
            DaoException ex = new DaoException("Problème de Driver", e);
            throw ex;
        }finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    /**
     * Méthode qui permet d'afficher tous les joueurs de la base de données
     *
     * @return list
     */

    public ArrayList<Joueur> getJoueurs() throws DaoException{
        ArrayList<Joueur> list = new ArrayList<>();
        Joueur res;
        Connection conn = null;
        try{
        conn = super.getConnection();
        Statement stmt = conn.createStatement();
        String req = "SELECT * FROM SLAM4_JoueurFoot";
        ResultSet result = stmt.executeQuery(req);
        while(result.next()){
            res = new Joueur();
            res.setId(result.getString(1));
            res.setNom(result.getString(2));
            res.setPrenom(result.getString(3));
            res.setPoste(result.getString(4));
            res.setNumero(result.getInt(5));
            res.setClub(result.getString(6));
            res.setDateNaissance(result.getDate(7));
            res.setNationnalite(result.getString(8));
            list.add(res);
        }
        result.close();
        stmt.close();
    }catch(SQLException e){
        DaoException ex = new DaoException("Problème de base de données", e);
        throw ex;
    }catch(ClassNotFoundException e){
        DaoException ex = new DaoException("Problème de Driver", e);
        throw ex;
    }finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    return list;
    }
}