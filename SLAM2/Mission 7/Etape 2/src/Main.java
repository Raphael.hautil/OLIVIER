/**
 * Classe: Main
 * @author: Raphaël Olivier
 * @version: 1.0
 */

import eu.hautil.dao.ProfesseurDAO;
import eu.hautil.modele.Professeur;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String [] args){
        Scanner sc = new Scanner(System.in);
        ProfesseurDAO pDao = new ProfesseurDAO();
        Professeur p = new Professeur();
        int choix = 0;
        int id;
        String nom;
        String spécialité;
        System.out.println("Que voulez vous faire ?\nAjouter un professeur (1)\nSupprimer un professeur (2)\nRechercher les professeurs en fonction de la spécialité (3)\nRechercher un professeur en fonction de son id (4)\nQuitter l'application (5)");
        choix = sc.nextInt();
        sc.nextLine();
        while (choix >= 0 && choix <6) {
            if (choix == 1){
                System.out.println("Saisir l'id du professeur : ");
                id = sc.nextInt();
                p.setId(id);
                sc.nextLine();

                System.out.println("Saisir le nom du professeur : ");
                nom = sc.nextLine();
                p.setNom(nom);

                System.out.println("Saisir la spécialité du professeur : ");
                spécialité = sc.nextLine();
                p.setSpecialite(spécialité);
                pDao.insertProfesseur(p);
            }

            else if (choix == 2){
                System.out.println("Entrer le nom du professeur à supprimer : ");
                nom = sc.nextLine();
                p.setNom(nom);
                pDao.deleteProfesseur(p);
            }

            else if (choix == 3){
                System.out.println("Saisir la spécialité du ou des professeurs que vous cherchez : ");
                spécialité = sc.nextLine();
                p.setSpecialite(spécialité);
                ArrayList<Professeur> r = pDao.getProfesseursBySpecialite(p.getSpecialite());
                System.out.println(r);
            }

            else if (choix == 4){
                System.out.println("Saisir l'id du professeur que vous cherchez : ");
                id = sc.nextInt();
                p.setId(id);
                Professeur res = pDao.getProfesseurById(p.getId());
                System.out.println(res.toString());
                sc.nextLine();
            }

            else if (choix == 5){
                System.out.println("Au revoir.");
                break;
            }
            System.out.println("Que voulez vous faire ?\nAjouter un professeur (1)\nSupprimer un professeur (2)\nRechercher les professeurs en fonction de la spécialité (3)\nRechercher un professeur en fonction de son id (4)\nQuitter l'application (5)");
            choix = sc.nextInt();
            sc.nextLine();
        }
    }
}
