/**
 * Classe: Professeur
 * @author: Zoubeida Abdelmoula
 * @version: 1.0
 */

package eu.hautil.modele;

public class Professeur {
    private int id;
    private String nom;
    private String specialite;

    /**
     * Méthode qui créé un professeur avec son id, son nom et sa spécialité
     *
     * @param id id du professeur
     * @param nom nom du professeur
     * @param specialite spécialité du professeur
     */

    public Professeur(int id, String nom, String specialite) {
        this.id = id;
        this.nom = nom;
        this.specialite = specialite;
    }

    /**
     * Méthode qui créé un professeur vide
     */

    public Professeur() {
    }

    /**
     * Méthode qui récupère et retourne l'id du professeur
     *
     * @return id
     */

    public int getId() {
        return id;
    }

    /**
     * Méthode qui récupère et retourne le nom du professeur
     *
     * @return nom
     */

    public String getNom() {
        return nom;
    }

    /**
     * Méthode qui récupère et retourne la spécialité du professeur
     *
     * @return specialite
     */

    public String getSpecialite() {
        return specialite;
    }

    /**
     * Méthode qui change l'id du professeur
     *
     * @param id id du professeur
     */

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Méthode qui change le nom du professeur
     *
     * @param nom nom du professeur
     */

    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Méthode qui change la spécialité du professeur
     *
     * @param specialite spécialité du professeur
     */

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    /**
     * Méthode qui affiche tous les paramètres du professeur
     *
     * @return id
     * @return nom
     * @return specialite
     */

    @Override
    public String toString() {
        return "Professeur{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", specialite='" + specialite + '\'' +
                '}';
    }
}
