import eu.hautil.dao.ProfesseurDAO;
import eu.hautil.modele.Professeur;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String [] args){
        Scanner sc = new Scanner(System.in);
        ProfesseurDAO pDao = new ProfesseurDAO();
        Professeur p = new Professeur();
        System.out.println("Saisir l'id du professeur : ");
        int id = sc.nextInt();
        p.setId(id);
        sc.nextLine();

        System.out.println("Saisir le nom du professeur : ");
        String nom = sc.nextLine();
        p.setNom(nom);

        System.out.println("Saisir la spécialité du professeur : ");
        String spécialité = sc.nextLine();
        p.setSpecialite(spécialité);
        pDao.insertProfesseur(p);

        System.out.println("Entrer le nom du professeur à supprimer : ");
        nom = sc.nextLine();
        p.setNom(nom);
        pDao.deleteProfesseur(p);

        System.out.println("Saisir l'id du professeur que vous cherchez : ");
        id = sc.nextInt();
        p.setId(id);
        Professeur res = pDao.getProfesseurById(p.getId());
        System.out.println(res.toString());
        sc.nextLine();

        System.out.println("Saisir la spécialité du ou des professeurs que vous cherchez : ");
        spécialité = sc.nextLine();
        p.setSpecialite(spécialité);
        ArrayList<Professeur> r = pDao.getProfesseursBySpecialite(p.getSpecialite());
        System.out.println(r);
    }
}
