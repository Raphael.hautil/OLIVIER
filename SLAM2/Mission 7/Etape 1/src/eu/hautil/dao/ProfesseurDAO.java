package eu.hautil.dao;

import com.sun.org.apache.regexp.internal.RE;
import eu.hautil.modele.Professeur;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ProfesseurDAO {

    private Connection getConnexion(){
        Connection conn = null;
        String driver = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mysql://sl-us-south-1-portal.11.dblayer.com:25137/BTS";
        String user="admin";
        String pwd = "GLJVDTPONRYBVEAR";
        try{
            Class.forName(driver);
            System.out.println("driver ok");
            conn=	DriverManager.getConnection(url,user,pwd);
            System.out.println("connection ok");

        }catch(Exception e){
            e.printStackTrace();
        }
        return conn;
    }

    public void insertProfesseur(Professeur p)
    {
        try {
            Connection conn = getConnexion();
            String req1 = "INSERT INTO professeurs VALUES (?,?,?)";
            PreparedStatement pstmt = conn.prepareStatement(req1);

            pstmt.setInt(1, p.getId());

            pstmt.setString(2, p.getNom());

            pstmt.setString(3, p.getSpecialite());

            int res1 = pstmt.executeUpdate();
            System.out.println("Nombre de lignes modifiées : " + res1);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void deleteProfesseur(Professeur p)
    {
        try{
            Connection conn = getConnexion();
            String req1 = "DELETE FROM professeurs WHERE nom = ?";
            PreparedStatement pstmt = conn.prepareStatement(req1);

            pstmt.setString(1, p.getNom());

            int res = pstmt.executeUpdate();
            System.out.println("Nombre de lignes modifiées : " + res);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public Professeur getProfesseurById(int id)
    {
        Professeur res = null;
        try{
            Connection conn = getConnexion();
            String req = "SELECT * FROM professeurs WHERE id = ?";
            PreparedStatement pstmt = conn.prepareStatement(req);

            pstmt.setInt(1, id);

            ResultSet result = pstmt.executeQuery();
            if (result.next()){
                res = new Professeur();
                res.setId(id);
                res.setNom(result.getString(2));
                res.setSpecialite(result.getString(3));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return res;
    }
    public ArrayList<Professeur> getProfesseursBySpecialite(String s)
    {
        ArrayList<Professeur> list = new ArrayList<>();
        Professeur res = null;
        try{
            Connection conn = getConnexion();
            String req = "SELECT * FROM professeurs WHERE spécialité = ?";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,s);

            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                res = new Professeur();
                res.setId(result.getInt(1));
                res.setNom(result.getString(2));
                res.setSpecialite(result.getString(3));
                list.add(res);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        // S'il y a des données : les récupérer dans un objet professeur
        //                         ajouter le professeur à la liste (list)
        return list;
    }
}
