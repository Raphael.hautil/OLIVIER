/**
 * Classe: Adresse Postale
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package eu.siohautil.base;

public class AdressePostale {
    private String adresse;
    private String ville;
    private String codePostale;

    /**
     * Créé une adresse postale vide
     */

    public AdressePostale(){

    }

    /**
     * Créé une adresse postale complète
     *
     * @param adresse Adresse de l'adresse postale
     * @param ville Ville de l'adresse postale
     * @param codePostale Code postal de l'adresse postale
     */

    public AdressePostale (String adresse, String ville, String codePostale){
        this.adresse=adresse;
        this.ville=ville;
        this.codePostale=codePostale;
    }

    /**
     * Change l'adresse de l'adresse postale
     *
     * @param adresse Adresse de l'adresse postale
     */

    public void setAdresse(String adresse){
        this.adresse=adresse;
    }

    /**
     * Retourne l'adresse de l'adresse postale
     *
     * @return adresse
     */

    public String getAdresse(){
        return this.adresse;
    }

    /**
     * Change la ville de l'adresse postale
     *
     * @param ville Ville de l'adresse postale
     */

    public void setVille(String ville){
        this.ville=ville;
    }

    /**
     * Retourne la ville de l'adresse postale
     *
     * @return ville
     */

    public String getVille() {
        return this.ville;
    }

    /**
     * Change le code postal de l'adresse postale
     *
     * @param codePostale Code postal de l'adresse postale
     */

    public void setCodePostale(String codePostale){
        this.codePostale=codePostale;
    }

    /**
     * Retourne le code postal de l'adresse postale
     *
     * @return codePostale
     */

    public String getCodePostale() {
        return this.codePostale;
    }

    /**
     * Affiche l'adresse postale complète
     */

    @Override

    public String toString(){
        if (this.adresse != null) {
            return("Adresse : "+this.adresse+", "+this.ville+", "+this.codePostale);
        }
        else{
            return("L'adresse n'est pas renseignée.");
        }

    }
}
