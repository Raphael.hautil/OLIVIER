/**
 * Classe: Agence Voyage
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package eu.siohautil.base;

import eu.siohautil.heritage.*;
import java.util.ArrayList;

public class AgenceVoyage {
    private String nom;
    private AdressePostale adresse;
    private ArrayList<Voyageur> tabVoy = new ArrayList<>();

    /**
     * Change le nom de l'agence de voyage
     *
     * @param nom Nom de l'agence de voyage
     */

    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Retourne le nom de l'agence de voyage
     *
     * @return nom
     */

    public String getNom() {
        return this.nom;
    }

    /**
     * Change l'adresse postale de l'agence de voyage
     *
     * @param adresse Adresse de l'agence de voyage
     */

    public void setAdresse(AdressePostale adresse) {
        this.adresse = adresse;
    }

    /**
     * Retourne l'adresse postale de l'agence de voyage
     *
     * @return adresse
     */

    public AdressePostale getAdresse() {
        return this.adresse;
    }

    /**
     * Créé une agence de voyage avec un nom, une adresse et une liste de 5 voyageurs déjà enregistrés
     *
     * @param nom Nom de l'agence de voyage
     * @param adresse Adresse de l'agnce de voyage
     */

    public AgenceVoyage(String nom, AdressePostale adresse){
        tabVoy = new ArrayList<>();
        VoyageurPrivilege v1 = new VoyageurPrivilege("Raphaël",18,"HDQ7E34FKF01");
        v1.setAdresse(new AdressePostale("22 rue des réservoirs","Triel sur Seine","78510"));
        v1.setBagage(new Bagage(0,"null",4.87));

        Voyageur v2 = new Voyageur("Thomas",20);
        v2.setAdresse(new AdressePostale("64 Grande Rue","Boisemont","95000"));
        v2.setBagage(new Bagage(34,"Rouge",11.34));

        Voyageur v3 = new Voyageur("Bastien",20);
        v3.setAdresse(new AdressePostale("9 rue de l'enfance","Vauréal","95490"));
        v3.setBagage(new Bagage(0,"null",4.87));

        VoyageurHandicape v4 = new VoyageurHandicape("Matis",5,"Autisme");
        v4.setAdresse(new AdressePostale("10 rue de sansonnets","Vauréal","95490"));
        v4.setBagage(new Bagage(0,"null",4.87));

        Voyageur v5 = new Voyageur("Adrien",18);
        v5.setAdresse(new AdressePostale("11 rue de la croix de Monsoult","Monsoult","95560"));
        v5.setBagage(new Bagage(0,"null",4.87));
        tabVoy.add(v1);
        tabVoy.add(v2);
        tabVoy.add(v3);
        tabVoy.add(v4);
        tabVoy.add(v5);
        this.nom=nom;
        this.adresse=adresse;
    }

    /**
     * Ajoute un voyageur à la liste
     *
     * @param tabVoy Liste de voyageurs
     */

    public void addVoyageur(Voyageur tabVoy){
        this.tabVoy.add(tabVoy);
    }

    /**
     * Cherche dans la liste de voyageurs le nom entré par l'utilisateur et retourne les informations de ce dernier
     *
     * @param nom Nom du voyageur
     *
     * @return voyageur
     */

    public Voyageur rechVoyageur(String nom){
        for (int i=0; i<this.tabVoy.size(); i++){
            if (nom.equals(this.tabVoy.get(i).getNom())){
                return this.tabVoy.get(i);
            }
        }
        return null;
    }

    /**
     * Cherche dans la liste de voyageurs le nom entré par l'utilisateur et supprime ce voyageur de la liste
     *
     * @param nom Nom du voyageur
     *
     * @return null
     */

    public Voyageur supprVoyageur(String nom){
        for (int i=0; i<this.tabVoy.size(); i++){
            if (nom.equals(this.tabVoy.get(i).getNom())){
                this.tabVoy.remove(i);
            }
        }
        return null;
    }

    /**
     * Affiche les informations de l'agence de voyage
     */

    @Override

    public String toString(){
        for(int i=0 ;i< tabVoy.size() ;i++) {
            Voyageur p= tabVoy.get(i);
            System.out.println(p.toString());
        }
        return("\nVous êtes dans l'agence de voyage: "+this.nom+"\n"+this.adresse.toString());
    }
}