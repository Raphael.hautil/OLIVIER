/**
 * Classe: Voyageur Handicapé
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package eu.siohautil.heritage;

import eu.siohautil.base.Voyageur;

public class VoyageurHandicape extends Voyageur {
    private String handicap;

    /**
     *Créé un voyageur handicapé vide
     */

    public VoyageurHandicape (){

    }

    /**
     * Créé et associe le nom et l'âge du voyageur initial avec un handicap
     *
     * @param nom Nom du voyageur
     * @param age Âge du voyageur
     * @param handicap Handicap du voyageur
     */

    public VoyageurHandicape (String nom, int age, String handicap){
        super(nom,age);
        this.handicap = handicap;
    }

    /**
     * Change le handicap du voyageur
     *
     * @param handicap Handicap du voyageur
     */

    public void setHandicap (String handicap){
        this.handicap = handicap;
    }

    /**
     * Retourne l'handicap du voyageur
     *
     * @return handicap
     */

    public String getHandicap() {
        return this.handicap;
    }

    /**
     * Affiche toutes les informations du voyageur handicapé
     */

    @Override

    public String toString(){
            return super.toString()+"\nIl possède le handicap suivant: " + this.handicap;
    }
}