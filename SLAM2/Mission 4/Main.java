/**
 * Classe: Main
 * @author: Raphaël Olivier
 * @version: 2.0
 */

package eu.siohautil.base;
import eu.siohautil.heritage.*;

import java.util.Scanner;
public class Main {
    private static Scanner sc;

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int test = 1;
        int testH = 0;
        String code=null;
        String handicap=null;

        AgenceVoyage av = new AgenceVoyage("AiRaphaël", new AdressePostale("22 avenue des Champs Elysées", "Paris", "75008"));
        System.out.println(av.toString());

        while (test == 1 || test == 2 || test == 3 || test == 4 || test == 5) {

            System.out.println("\nQue voulez vous faire ?\nAjouter un voyageur: Tapez 1\nRechercher un utilisateur: Tapez 2\nSupprimer un voyageur: Tapez 3\nAfficher les informations de l'agence: Tapez 4\nQuitter l'application: Tapez 5");
            test = sc.nextInt();
            sc.nextLine();

            if (test == 1) {
                AdressePostale x = new AdressePostale();
                Bagage a = new Bagage();

                System.out.println("Le voyageur que vous voulez ajouter a t'il un code privilège ? 1 pour oui, 0 pout non : ");
                int testC = sc.nextInt();
                if (testC != 0) {
                    sc.nextLine();
                    System.out.println("Saisissez le code : ");
                    code = sc.nextLine();
                } else {
                    System.out.println("Le voyageur que vous voulez ajouter a t'il un handicap ? 1 pour oui, 0 pout non : ");
                    testH = sc.nextInt();
                    if (testH != 0) {
                        sc.nextLine();
                        System.out.println("Quel est ce handicap.");
                        handicap = sc.nextLine();
                    }
                }

                    System.out.println("Saisissez votre nom: ");
                    String nom = sc.next();

                    System.out.println("Quel est votre âge ?");
                    int age = sc.nextInt();
                    sc.nextLine();

                    System.out.println("Saisissez votre adresse: ");
                    String adresse = sc.nextLine();
                    x.setAdresse(adresse);

                    System.out.println("Saisissez votre ville: ");
                    String ville = sc.nextLine();
                    x.setVille(ville);

                    System.out.println("Saisissez votre code postal: ");
                    String codePostale = sc.nextLine();
                    x.setCodePostale(codePostale);

                    System.out.println("Avez vous un bagage ? 1 pour oui, 0 pour non :");
                    int testB = sc.nextInt();
                    sc.nextLine();

                    if (testB == 1) {

                        System.out.println("Quel est le numéro de votre bagage ?");
                        int numero = sc.nextInt();
                        a.setNumero(numero);
                        sc.nextLine();

                        System.out.println("De quelle couleur est votre bagage ?");
                        String couleur = sc.nextLine();
                        a.setCouleur(couleur);

                        System.out.println("Combien pèse votre bagage ?");
                        double poids = sc.nextDouble();
                        a.setPoids(poids);
                        sc.nextLine();
                    }

                    if (testC != 0){
                        VoyageurPrivilege vp = new VoyageurPrivilege();
                        vp.setCode(code);
                        vp.setNom(nom);
                        vp.setAge(age);
                        vp.setAdresse(x);
                        vp.setBagage(a);
                        av.addVoyageur(vp);
                        System.out.println(vp.toString());
                    }
                    else if(testH !=0){
                        VoyageurHandicape vh = new VoyageurHandicape();
                        vh.setHandicap(handicap);
                        vh.setNom(nom);
                        vh.setAge(age);
                        vh.setAdresse(x);
                        vh.setBagage(a);
                        av.addVoyageur(vh);
                        System.out.println(vh.toString());
                    }
                    else {
                        Voyageur v = new Voyageur();
                        v.setNom(nom);
                        v.setAge(age);
                        v.setAdresse(x);
                        v.setBagage(a);
                        av.addVoyageur(v);
                        System.out.println(v.toString());
                    }
                }

                if (test == 2) {
                    System.out.println("Entrer le nom du voyageur que vous cherchez : ");
                    String nom = sc.nextLine();
                    Voyageur v = av.rechVoyageur(nom);
                    if (v != null) {
                        System.out.println(v.toString());
                    } else {
                        System.out.println("Ce voyageur n'existe pas.");
                    }
                }

                if (test == 3) {
                    System.out.println("Entrer le nom du voyageur que vous voulez supprimer : ");
                    String nom = sc.nextLine();
                    Voyageur v = av.rechVoyageur(nom);
                    if (v != null) {
                        System.out.println(v.toString());
                        System.out.println("Confirmer la supression, ressaisir le nom : ");
                        nom = sc.nextLine();
                        av.supprVoyageur(nom);
                        System.out.println(av.toString());
                    } else {
                        System.out.println("Ce voyageur n'existe pas.");
                    }
                }

            if (test == 4) {
                System.out.println(av.toString());
            }

            if (test == 5) {
                System.out.println("Merci, à bientôt !");
                    break;
            }
        }
    }
}