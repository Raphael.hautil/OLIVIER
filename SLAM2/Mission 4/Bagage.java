/**
 * Classe: Bagage
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package eu.siohautil.base;

public class Bagage {
    private int numero;
    private String couleur;
    private double poids;

    /**
     * Créé un bagage vide
     */

    public Bagage(){

    }

    /**
     * Créé un bagage complet
     *
     * @param numero Numéro du bagage
     * @param couleur Couleur du bagage
     * @param poids Poids du bagage
     */

    public Bagage (int numero, String couleur, double poids){
        this.numero=numero;
        this.couleur=couleur;
        this.poids=poids;
    }

    /**
     * Change le numéro du bagage
     *
     * @param numero Numéro du bagage
     */

    public void setNumero(int numero){
        this.numero=numero;
    }

    /**
     * Retourne le numéro du bagage
     *
     * @return numero
     */

    public int getNumero(){
        if (this.numero != 0) {
            return this.numero;
        }
        return 0;
    }

    /**
     * Change la couuleur du bagage
     *
     * @param couleur Couleur du bagage
     */

    public void setCouleur(String couleur){
        this.couleur=couleur;
    }

    /**
     * Retourne la couleur du bagage
     *
     * @return couleur
     */

    public String getCouleur(){
        return this.couleur;
    }

    /**
     * Change le poids du bagage
     *
     * @param poids Poids du bagage
     */

    public void setPoids(double poids){
        this.poids=poids;
    }

    /**
     * Retourne le poids du bagage
     *
     * @return poids
     */

    public double getPoids(){
        return this.poids;
    }

    /**
     * Affiche toutes les informations du bagage
     */

    @Override

    public String toString(){
        if(this.numero != 0) {
            return("Le bagage a les caractéristiques suivantes: "+this.numero+", "+this.couleur+", "+this.poids);
        }
        else{
            return ("Vous n'avez pas de bagage.");
        }
    }
}
