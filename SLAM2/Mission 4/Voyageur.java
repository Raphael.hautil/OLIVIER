/**
 * Classe: Voyageur
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package eu.siohautil.base;

public class Voyageur{
    protected String nom;
    protected int age;
    protected String categorie;
    protected AdressePostale adresse;
    protected Bagage bagage;

    /**
     * Change le nom du voyageur
     *
     * @param nom Nom du voyageur
     */

    public void setNom(String nom){
        if (nom.length()>= 2){
            this.nom=nom;
        }
    }

    /**
     * Retourne le nom du voyageur
     *
     * @return nom
     */

    public String getNom(){
        return this.nom;
    }

    /**
     * Change l'âge du voyageur
     *
     * @param age Âge du voyageur
     */

    public void setAge(int age){
        if (age > 0){
            this.age=age;
            setCategorie();
        }
    }

    /**
     * Retourne l'âge du voyageur
     *
     * @return age
     */

    public int getAge(){
        return this.age;
    }

    /**
     * Affecte une catégorie au voyageur en fonction de son âge
     */

    private void setCategorie(){
        if (age<2){
            this.categorie="nourisson";
        }
        else if (age<18){
            this.categorie="enfant";
        }
        else if (age<65){
            this.categorie="adulte";
        }
        else {
            this.categorie="sénior";
        }
    }

    /**
     * Retourne la catégorie du voyageur
     *
     * @return categorie
     */

    public String getCategorie(){
        return this.categorie;
    }

    /**
     *Créé un voyageur vide
     */

    public Voyageur(){
        this.nom = " ";
        this.age = 0;
        setCategorie();
    }

    /**
     * Créé et associe le nom et l'âge du voyageur en fonction des valeurs que rentre l'utilsateur
     * Associe une catégorie au voyageur en fonction de son âge
     *
     * @param nom Nom du voyageur
     * @param age Âge du voyageur
     */

    public Voyageur(String nom, int age){
        this.nom=nom;
        this.age=age;
        setCategorie();
    }

    /**
     * Change l'adresse du voyageur
     *
     * @param adresse Adresse postale du voyageur
     */

    public void setAdresse(AdressePostale adresse) {
        this.adresse = adresse;
    }

    /**
     * Retourne l'adresse du voyageur
     *
     * @return adresse
     */

    public AdressePostale getAdresse(){
        return this.adresse;
    }

    /**
     * Change le bagage du voyageur
     *
     * @param bagage Bagage du voyageur
     */

    public void setBagage(Bagage bagage) {
        this.bagage = bagage;
    }

    /**
     * Retourne le bagage du voyageur
     *
     * @return bagage
     */

    public Bagage getBagage() {
        return this.bagage;
    }

    /**
     * Affiche toutes les informations du voyageur
     */

    @Override

    public String toString() {
        return ("\nIdentité :("+this.nom+", "+this.age+", "+this.categorie+")" +
                "\nAdresse : "+this.adresse+
                "\n"+this.bagage);
    }
}