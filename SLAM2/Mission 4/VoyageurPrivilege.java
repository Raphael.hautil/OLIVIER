/**
 * Classe: Voyageur Privilège
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package eu.siohautil.heritage;

import eu.siohautil.base.Voyageur;

public class VoyageurPrivilege extends Voyageur {
    private String code;

    /**
     *Créé un voyageur privilège vide
     */

    public VoyageurPrivilege (){

    }

    /**
     * Créé et associe le nom et l'âge du voyageur initial avec un code privilège
     *
     * @param nom Nom du voyageur
     * @param age Âge du voyageur
     * @param code Code privilège du voyageur
     */

    public VoyageurPrivilege (String nom, int age, String code){
        super(nom,age);
        this.code = code;
    }

    /**
     * Change le code privilège du voyageur
     *
     * @param code Code privilège du voyageur
     */

    public void setCode(String code){
        this.code = code;
    }

    /**
     * Retourne le code privilège du voyageur
     *
     * @return code
     */

    public String getCode(){
        return this.code;
    }

    /**
     * Affiche toutes les informations du voyageur privilège
     */

    @Override

    public String toString(){
            return super.toString() + "\nIl possède le code privilège suivant: " + this.code;
    }
}
