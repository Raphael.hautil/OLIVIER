/**
 * Classe: Bagage
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package eu.siohautil.mission3;

public class Bagage {
    private int numero;
    private String couleur;
    private double poids;

    /**
     * Créé un bagage vide
     */

    public Bagage(){

    }

    /**
     * Créé un bagage complet
     *
     * @param numero
     * @param couleur
     * @param poids
     */

    public Bagage (int numero, String couleur, double poids){
        this.numero=numero;
        this.couleur=couleur;
        this.poids=poids;
    }

    /**
     * Change le numéro du bagage
     *
     * @param numero
     */

    public void setNumero(int numero){
        this.numero=numero;
    }

    /**
     * Retourne le numéro du bagage
     *
     * @return numero
     */

    public int getNumero(){
        return this.numero;
    }

    /**
     * Change la couuleur du bagage
     *
     * @param couleur
     */

    public void setCouleur(String couleur){
        this.couleur=couleur;
    }

    /**
     * Retourne la couleur du bagage
     *
     * @return couleur
     */

    public String getCouleur(){
        return this.couleur;
    }

    /**
     * Change le poids du bagage
     *
     * @param poids
     */

    public void setPoids(double poids){
        this.poids=poids;
    }

    /**
     * Retourne le poids du bagage
     *
     * @return poids
     */

    public double getPoids(){
        return this.poids;
    }

    /**
     * Affiche toutes les informations du bagage
     */

    void afficher(){
        System.out.println("Le bagage a les caractéristiques suivantes: "+numero+", "+couleur+", "+poids);
    }
}