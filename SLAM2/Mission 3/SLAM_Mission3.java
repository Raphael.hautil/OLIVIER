/**
 * Classe: Main
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package eu.siohautil.raphael;
import eu.siohautil.mission3.Voyageur;
import eu.siohautil.mission3.AdressePostale;
import eu.siohautil.mission3.Bagage;
import eu.siohautil.mission3.AgenceVoyage;

import java.util.ArrayList;
import java.util.Scanner;
public class SLAM_Mission3 {
    private static Scanner sc;

    public static void main(String[] args) {
        ArrayList<Voyageur> Voyageur = new ArrayList<>();
        sc = new Scanner(System.in);
        int test = 1;

        AgenceVoyage av = new AgenceVoyage("AiRaphaël", new AdressePostale("22 avenue des Champs Elysées", "Paris", "75008"));
        av.afficher();

        while (test == 1 || test == 2 || test == 3 || test == 4 || test == 5) {

            System.out.println("\nQue voulez vous faire ?\nAjouter un voyageur: Tapez 1\nRechercher un utilisateur: Tapez 2\nSupprimer un voyageur: Tapez 3\nAfficher les informations de l'agence: Tapez 4\nQuitter l'application: Tapez 5");
            test = sc.nextInt();
            sc.nextLine();

            if (test == 1) {
                AdressePostale x = new AdressePostale();
                Bagage a = new Bagage();
                Voyageur v = new Voyageur();

                System.out.println("Saisissez votre nom: ");
                String nom = sc.next();
                v.setNom(nom);

                System.out.println("Quel est votre âge ?");
                int age = sc.nextInt();
                v.setAge(age);
                sc.nextLine();

                System.out.println("Saisissez votre adresse: ");
                String adresse = sc.nextLine();
                x.setAdresse(adresse);

                System.out.println("Saisissez votre ville: ");
                String ville = sc.nextLine();
                x.setVille(ville);

                System.out.println("Saisissez votre code postal: ");
                String codePostale = sc.nextLine();
                x.setCodePostale(codePostale);

                v.setAdresse(x);

                System.out.println("Avez vous un bagage ? (0 pour non, 1 pour oui) :");
                int p = sc.nextInt();
                sc.nextLine();

                if (p == 1) {
                    v.setBagage(a);
                    System.out.println("Saisissez le numéro de votre bagage : ");

                    int numero = sc.nextInt();
                    a.setNumero(numero);
                    sc.nextLine();

                    System.out.println("De quelle couleur est votre bagage ?");
                    String couleur = sc.nextLine();
                    a.setCouleur(couleur);

                    System.out.println("Combien pèse votre bagage ?");
                    double poids = sc.nextDouble();
                    a.setPoids(poids);
                    sc.nextLine();

                    av.addVoyageur(v);
                } else if (p == 0) {
                    av.addVoyageur(v);
                }

                av.afficher();
            }

            if (test == 2) {
                System.out.println("Entrer le nom du voyageur que vous cherchez : ");
                String nom = sc.nextLine();
                Voyageur v = av.rechVoyageur(nom);
                if (v != null){
                    v.afficher();
                } else {
                    System.out.println("Ce voyageur n'existe pas.");
                }
            }

            if (test == 3){
                System.out.println("Entrer le nom du voyageur que vous voulez supprimer : ");
                String nom = sc.nextLine();
                Voyageur v = av.rechVoyageur(nom);
                if (v != null){
                    v.afficher();
                    System.out.println("Confirmer la supression, ressaisir le nom : ");
                    nom = sc.nextLine();
                    v = av.supprVoyageur(nom);
                    av.afficher();
                }
                else{
                    System.out.println("Ce voyageur n'existe pas.");
                }

            }

            if (test == 4){
                av.afficher();
            }

            if (test == 5) {
                System.out.println("Merci, à bientôt !");
                break;
            }
        }
    }
}