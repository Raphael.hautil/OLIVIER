/**
 * Classe: Voyageur
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package eu.siohautil.mission3;

public class Voyageur{
    private String nom;
    private int age;
    private String categorie;
    private AdressePostale adresse;
    private Bagage bagage;

    /**
     * Change le nom du voyageur
     *
     * @param nom
     */

    public void setNom(String nom){
        if (nom.length()>= 2){
            this.nom=nom;
        }
    }

    /**
     * Retourne le nom du voyageur
     *
     * @return nom
     */

    public String getNom(){
        return this.nom;
    }

    /**
     * Change l'âge du voyageur
     *
     * @param age
     */

    public void setAge(int age){
        if (age > 0){
            this.age=age;
            setCategorie();
        }
    }

    /**
     * Retourne l'âge du voyageur
     *
     * @return age
     */

    public int getAge(){
        return this.age;
    }

    /**
     * Affecte une catégorie au voyageur en fonction de son âge
     *
     */

    public void setCategorie(){
        if (age<2){
            this.categorie="nourisson";
        }
        else if (age>=2 && age<18){
            this.categorie="enfant";
        }
        else if (age>=18 && age<65){
            this.categorie="adulte";
        }
        else {
            this.categorie="sénior";
        }
    }

    /**
     * Retourne la catégorie du voyageur
     *
     * @return categorie
     */

    public String getCategorie(){
        return this.categorie;
    }

    /**
     *Créé un voyageur vide
     */

    public Voyageur(){
        nom = " ";
        age = 0;
        setCategorie();
    }

    /**
     * Créé et associe le nom et l'âge du voyageur en fonction des valeurs que rentre l'utilsateur
     * Associe une catégorie au voyageur en fonction de son âge
     *
     * @param nom
     * @param age
     */

    public Voyageur(String nom, int age){
        this.nom=nom;
        this.age=age;
        setCategorie();
    }

    /**
     * Change l'adresse du voyageur
     *
     * @param adresse
     */

    public void setAdresse(AdressePostale adresse) {
        this.adresse = adresse;
    }

    /**
     * Retourne l'adresse du voyageur
     *
     * @return adresse
     */

    public AdressePostale getAdresse(){
        return adresse;
    }

    /**
     * Change le bagage du voyageur
     *
     * @param bagage
     */

    public void setBagage(Bagage bagage) {
        this.bagage = bagage;
    }

    /**
     * Retourne le bagage du voyageur
     *
     * @return bagage
     */

    public Bagage getBagage() {
        return bagage;
    }

    /**
     * Affiche toutes les informations du voyageur
     */

    public void afficher() {
        System.out.println ("\nIdentité :("+nom+", "+age+", "+categorie+")");
        if (adresse != null) {
            this.adresse.afficher();
        }
        else{
            System.out.println("L'adresse n'est pas renseignée.");
        }
        if(bagage != null) {
            this.bagage.afficher();
        }
        else{
            System.out.println("Vous n'avez pas de bagage.");
        }
    }
}