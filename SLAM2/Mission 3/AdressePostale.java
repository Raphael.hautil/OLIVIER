/**
 * Classe: Adresse Postale
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package eu.siohautil.mission3;

public class AdressePostale {
    private String adresse;
    private String ville;
    private String codePostale;

    /**
     * Créé une adresse postale vide
     */

    public AdressePostale(){

    }

    /**
     * Créé une adresse postale complète
     *
     * @param adresse
     * @param ville
     * @param codePostale
     */

    public AdressePostale (String adresse, String ville, String codePostale){
        this.adresse=adresse;
        this.ville=ville;
        this.codePostale=codePostale;
    }

    /**
     * Change l'adresse de l'adresse postale
     *
     * @param adresse
     */

    public void setAdresse(String adresse){
        this.adresse=adresse;
    }

    /**
     * Retourne l'adresse de l'adresse postale
     *
     * @return adresse
     */

    public String getAdresse(){
        return this.adresse;
    }

    /**
     * Change la ville de l'adresse postale
     *
     * @param ville
     */

    public void setVille(String ville){
        this.ville=ville;
    }

    /**
     * Retourne la ville de l'adresse postale
     *
     * @return ville
     */

    public String getVille() {
        return this.ville;
    }

    /**
     * Change le code postal de l'adresse postale
     *
     * @param codePostale
     */

    public void setCodePostale(String codePostale){
        this.codePostale=codePostale;
    }

    /**
     * Retourne le code postal de l'adresse postale
     *
     * @return codePostale
     */

    public String getCodePostale() {
        return this.codePostale;
    }

    /**
     * Affiche l'adresse postale complète
     */

    void afficher(){
        System.out.println("Adresse : "+adresse+", "+ville+", "+codePostale);
    }
}
