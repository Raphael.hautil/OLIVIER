Etape 4:

import java.util.Scanner;
public class SLAM_Mission2 {
    private static Scanner sc;
    public static void main(String [] args){
        sc = new Scanner(System.in);

        AdressePostale x = new AdressePostale();
        Bagage a = new Bagage();

        Voyageur v = new Voyageur("Raphaël",18);
        v.afficher();

        System.out.println("Saisissez votre adresse: ");
        String adresse=sc.nextLine();
        x.setAdresse(adresse);

        System.out.println("Saisissez votre ville: ");
        String ville=sc.nextLine();
        x.setVille(ville);

        System.out.println("Saisissez votre code postal: ");
        String codePostale=sc.nextLine();
        x.setCodePostale(codePostale);

        v.setAdresse(x);
        v.afficher();

        System.out.println("Avez vous un bagage ? (0 pour non, 1 pour oui) :");
        int z = sc.nextInt();

        if(z == 1){
            v.setBagage(a);
            System.out.println("Saisissez le numéro de votre bagage : ");

            int numero = sc.nextInt();
            a.setNumero(numero);

            System.out.println("De quelle couleur est votre bagage ?");
            String couleur = sc.next();
            a.setCouleur(couleur);

            System.out.println("Combien pèse votre bagage ?");
            double poids = sc.nextDouble();
            a.setPoids(poids);

            v.afficher();

            System.out.println("ERREUR mauvaise couleur du bagage, veuillez resaisir la couleur du bagage : ");
            couleur = sc.next();
            a.setCouleur(couleur);

            v.afficher();
        }
        else if(z == 0){
            v.afficher();
        }
    }
}