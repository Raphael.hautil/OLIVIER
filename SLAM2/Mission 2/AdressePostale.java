Etape 4:

public class AdressePostale {
    private String adresse;
    private String ville;
    private String codePostale;

    AdressePostale(){

    }

    public AdressePostale (String adresse, String ville, String codePostale){
        this.adresse=adresse;
        this.ville=ville;
        this.codePostale=codePostale;
    }
    public void setAdresse(String adresse){
        this.adresse=adresse;
    }
    public String getAdresse(){
        return this.adresse;
    }
    public void setVille(String ville){
        this.ville=ville;
    }
    public String getVille() {
        return this.ville;
    }
    public void setCodePostale(String codePostale){
        this.codePostale=codePostale;
    }
    public String getCodePostale() {
        return this.codePostale;
    }
    void afficher(){
        System.out.println("Adresse : "+adresse+", "+ville+", "+codePostale);
    }
}