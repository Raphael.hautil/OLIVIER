Etape 4:

public class Bagage {
    private int numero;
    private String couleur;
    private double poids;

    Bagage(){

    }

    public Bagage (int numero, String couleur, double poids){
        this.numero=numero;
        this.couleur=couleur;
        this.poids=poids;
    }

    public void setNumero(int numero){
        this.numero=numero;
    }
    public int getNumero(){
        return this.numero;
    }

    public void setCouleur(String couleur){
        this.couleur=couleur;
    }
    public String getCouleur(){
        return this.couleur;
    }

    public void setPoids(double poids){
        this.poids=poids;
    }
    public double getPoids(){
        return this.poids;
    }

    void afficher(){
        System.out.println("Le bagage a les caractéristiques suivantes: "+numero+", "+couleur+", "+poids);
    }
}