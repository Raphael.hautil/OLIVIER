/**
 * Classe: Main
 * @author: Raphaël Olivier
 * @version: 1.0
 */
package eu.siohautil.Base;

import java.util.Scanner;
import java.sql.*;

public class Main {
     public static void main(String[] args) {
         Scanner sc = new Scanner(System.in);
         int test = 1;
         int id;
         String nom;
         String spécialité;
         System.out.println("\nQue voulez vous faire ? Ajouter un professeur : 1\nSupprimer un professeur : 2\nRechercher les professeurs en fonction de leur spécialité : 3\nRechercher les professeurs en fonction de leur nom : 4\nAfficher tous les professeurs : 5\nQuitter l'application : 6");
         test = sc.nextInt();
         while (test>0 & test<7){
             if (test == 1) {
                 try {
                     Class.forName("com.mysql.cj.jdbc.Driver");
                     System.out.println("Driver OK.");
                     Connection conn = DriverManager.getConnection("jdbc:mysql://sl-us-south-1-portal.11.dblayer.com:25137/BTS", "admin", "GLJVDTPONRYBVEAR");
                     System.out.println("Connection OK.");
                     String req3 = "INSERT INTO professeurs VALUES (?,?,?)";
                     PreparedStatement pstmt = conn.prepareStatement(req3);
                     System.out.println("Saisir l'id du professeur : ");
                     id = sc.nextInt();
                     pstmt.setInt(1, id);
                     sc.nextLine();

                     System.out.println("Saisir le nom du professeur : ");
                     nom = sc.nextLine();
                     pstmt.setString(2, nom);

                     System.out.println("Saisir la spécialité du professeur : ");
                     spécialité = sc.nextLine();
                     pstmt.setString(3, spécialité);

                     int res2 = pstmt.executeUpdate();
                     System.out.println("Nombre de lignes modifiées : " + res2);
                 } catch (Exception e) {
                     e.printStackTrace();
                 }
             }
             else if (test == 2) {
                 try {
                     Class.forName("com.mysql.cj.jdbc.Driver");
                     System.out.println("Driver OK.");
                     Connection conn = DriverManager.getConnection("jdbc:mysql://sl-us-south-1-portal.11.dblayer.com:25137/BTS", "admin", "GLJVDTPONRYBVEAR");
                     System.out.println("Connection OK.");
                     String req5 = "DELETE FROM professeurs WHERE id = ?";
                     PreparedStatement pstmt3 = conn.prepareStatement(req5);
                     System.out.println("Saisir l'id de l'utilisateur à supprimer : ");
                     id = sc.nextInt();
                     pstmt3.setInt(1, id);
                     int res3 = pstmt3.executeUpdate();
                     System.out.println("Nombre de lignes modifiées : " + res3);
                     sc.nextLine();
                 }catch (Exception e) {
                     e.printStackTrace();
                 }
             }
             else if (test == 3) {
                 try {
                     Class.forName("com.mysql.cj.jdbc.Driver");
                     System.out.println("Driver OK.");
                     Connection conn = DriverManager.getConnection("jdbc:mysql://sl-us-south-1-portal.11.dblayer.com:25137/BTS", "admin", "GLJVDTPONRYBVEAR");
                     System.out.println("Connection OK.");
                     String req4 = "SELECT * FROM professeurs WHERE spécialité = ?";
                     PreparedStatement pstmt2 = conn.prepareStatement(req4);
                     System.out.println("Recherche de professeurs en fonction de la spécialité : ");
                     spécialité = sc.nextLine();
                     pstmt2.setString(1, spécialité);
                     ResultSet result2 = pstmt2.executeQuery();
                     while (result2.next()) {
                         System.out.println("ID : " + result2.getString(1));
                         System.out.println("Nom : " + result2.getString(2));
                         System.out.println("Spécialité : " + result2.getString(3));
                         System.out.println("\n");
                     }
                 } catch (Exception e) {
                     e.printStackTrace();
                 }
             }
             else if (test == 4) {
                 try {
                     Class.forName("com.mysql.cj.jdbc.Driver");
                     System.out.println("Driver OK.");
                     Connection conn = DriverManager.getConnection("jdbc:mysql://sl-us-south-1-portal.11.dblayer.com:25137/BTS", "admin", "GLJVDTPONRYBVEAR");
                     System.out.println("Connection OK.");
                     String req6 = "SELECT * FROM professeurs WHERE nom = ?";
                     PreparedStatement pstmt4 = conn.prepareStatement(req6);
                     System.out.println("Saisir le nom que vous recherchez : ");
                     nom = sc.nextLine();
                     pstmt4.setString(1, nom);
                     ResultSet result3 = pstmt4.executeQuery();
                     while (result3.next()) {
                         System.out.println("ID : " + result3.getString(1));
                         System.out.println("Nom : " + result3.getString(2));
                         System.out.println("Spécialité : " + result3.getString(3));
                         System.out.println("\n");
                     }
                 }catch (Exception e) {
                     e.printStackTrace();
                 }
             }
             else if (test == 5){
                 try{
                     Class.forName("com.mysql.cj.jdbc.Driver");
                     System.out.println("Driver OK.");
                     Connection conn = DriverManager.getConnection("jdbc:mysql://sl-us-south-1-portal.11.dblayer.com:25137/BTS", "admin", "GLJVDTPONRYBVEAR");
                     System.out.println("Connection OK.");
                     Statement stmt = conn.createStatement();
                     String req2 = "SELECT * FROM professeurs";
                     ResultSet result = stmt.executeQuery(req2);
                     while (result.next()){
                         System.out.println("ID : " + result.getString(1));
                         System.out.println("Nom : " + result.getString(2));
                         System.out.println("Spécialité : " + result.getString(3));
                         System.out.println("\n");
                     }
                 }catch (Exception e){
                     e.printStackTrace();
                 }
             }
             else if (test == 6){
                 System.out.println("Au revoir !");
                 break;
             }
             System.out.println("\nQue voulez vous faire ? Ajouter un professeur : 1\nSupprimer un professeur : 2\nRechercher les professeurs en fonction de leur spécialité : 3\nRechercher les professeurs en fonction de leur nom : 4\nAfficher tous les professeurs : 5\nQuitter l'application : 6");
             test = sc.nextInt();
             sc.nextLine();
             //String req1 = "INSERT INTO professeurs VALUES (3,'Massat','Français')";
                 //int res = stmt.executeUpdate(req1);
                 //System.out.println("Nombre de modifications : " + res);
         }
     }
}
