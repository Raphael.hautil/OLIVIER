/**
 * Classe: Animal
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package eu.siohautil.animal;
public class Animal {
    protected String surnom;
    protected int age;
    protected String type;

    /**
     * Créé un animal vide
     */

    public Animal(){
        this.surnom = " ";
        this.age = 0;
        this.type = " ";
    }

    /**
     * Créé et associe le surnom, l'âge et le type de l'animal en fonction des valeurs que rentre l'utilisateur
     *
     * @param surnom Surnom de l'animal
     * @param age Âge de l'animal
     * @param type Type de l'animal
     */

    public Animal(String surnom, int age, String type){
        this.surnom=surnom;
        this.age=age;
        this.type=type;
    }

    /**
     * Change le surnom de l'animal
     *
     * @param surnom Surnom de l'animal
     */

    public void setSurnom(String surnom){
        this.surnom = surnom;
    }

    /**
     * Récupère et retourne le surnom de l'animal
     *
     * @return surnom
     */

    public String getSurnom() {
        return this.surnom;
    }

    /**
     * Change l'âge de l'animal
     *
     * @param age Âge de l'animal
     */

    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Récupère et retourne l'âge de l'animal
     *
     * @return age
     */

    public int getAge() {
        return this.age;
    }

    /**
     * Change le type de l'animal
     *
     * @param type Type de l'animal
     */

    public void setType(String type) {
        this.type = type;
    }

    /**
     * Récupère et retourne le type de l'animal
     *
     * @return type
     */

    public String getType() {
        return this.type;
    }

    /**
     * Affiche toutes les informations de l'animal
     */

    @Override
    public String toString() {
        return this.surnom + " est un " + this.type + " et a " + this.age + " ans.";
    }
}
