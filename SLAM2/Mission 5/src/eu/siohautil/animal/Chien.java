/**
 * Classe: Chien
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package eu.siohautil.animal;
public class Chien extends Animal {
    private String couleur;

    /**
     * Créé un chien vide
     */

    public Chien(){

    }

    /**
     * Créé et associe le surnom, l'âge, le type et la couleur du chien avec les valeurs saisies par l'utilisateur
     *
     * @param surnom Surnom du chien
     * @param age Âge du chien
     * @param type Type du chien
     * @param couleur Couleur du chien
     */

    public Chien(String surnom, int age, String type, String couleur){
        super(surnom, age, type);
        this.couleur=couleur;
    }

    /**
     * Change la couleur du chien
     *
     * @param couleur Couleur du chien
     */

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    /**
     * Récupère et retourne la couleur du chien
     *
     * @return couleur
     */

    public String getCouleur() {
        return this.couleur;
    }

    /**
     * Méthode qui affiche une phrase quand le chien aboie
     */

    public void aboyer(){
        System.out.println("Le chien " + this.surnom + " aboie.");
    }
}
