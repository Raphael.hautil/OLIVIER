/**
 * Classe: Canard
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package eu.siohautil.animal;
import eu.siohautil.base.*;
public class Canard extends Animal implements Navigable {
    private int nbPlumes;

    /**
     * Créé un canard vide
     */

    public Canard(){

    }

    /**
     * Créé et associe le surnom, l'âge, le type et le nombre de plumes du canard avec les valeurs saisies par l'utilisateur
     *
     * @param surnom Surnom du canard
     * @param age Âge du canard
     * @param type Type du canard
     * @param nbPlumes Nombre de plumes du canard
     */

    public Canard(String surnom, int age, String type, int nbPlumes){
        super(surnom, age, type);
        this.nbPlumes=nbPlumes;
    }

    /**
     * Change le nombre de plumes du canard
     *
     * @param nbPlumes Nombre de plumes du canard
     */

    public void setNbPlumes(int nbPlumes){
        this.nbPlumes=nbPlumes;
    }

    /**
     * Récupère et retourne le nombre de plumes du canard
     *
     * @return nbPlumes
     */

    public int getNbPlumes() {
        return this.nbPlumes;
    }

    /**
     * Méthode qui affiche une phrase quand le canard accoste
     */

    @Override
    public void accoster() {
        System.out.println("Le canard " + this.surnom + " accoste.");
    }

    /**
     * Méthode qui affiche une phrase quand le canard navigue
     */

    @Override
    public void naviguer() {
        System.out.println("Le canard " + this.surnom + " navigue.");
    }

    /**
     * Méthode qui affiche une phrase quand le canard coule
     */

    @Override
    public void couler() {
        System.out.println("Le canard " + this.surnom + " coule.");
    }
}
