/**
 * Interface: Navigable
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package eu.siohautil.base;
public interface Navigable {
    /**
     * Affiche une phrase qui explique que le navigable est en train d'accoster
     */

    public void accoster();

    /**
     * Affiche une phrase qui explique que le navigable est en train de naviguer
     */

    public void naviguer();

    /**
     * Affiche une phrase qui explique que le navigable est en train de couler
     */

    public void couler();
}
