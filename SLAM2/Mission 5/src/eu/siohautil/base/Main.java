/**
 * Classe: Main
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package eu.siohautil.base;
import eu.siohautil.animal.*;

import java.util.ArrayList;
public class Main {
    public static void main(String[]args){
        ArrayList<Véhicule> listVehicule = new ArrayList<>();
        listVehicule.add(new Voiture("Ferrari", "rouge", "F40",2));
        listVehicule.add(new Voiture("Ford", "noire", "GT",2));
        listVehicule.add(new Voiture("Renault", "jaune", "Avantime",5));
        listVehicule.add(new Voiture("Porsche", "grise", "Carrera GT", 2));
        listVehicule.add(new Bateau("Prestige","blanc","750",2000));

        for (Véhicule n : listVehicule){
            n.demarrer();
        }

        System.out.println("\n");

        for (Véhicule n : listVehicule){
            if( n instanceof Voiture){
                Voiture v = (Voiture)n;
                v.stationner();
            }
        }

        System.out.println("\n");

        ArrayList<Animal> listAnimal = new ArrayList<>();
        listAnimal.add(new Animal("Lou", 2,"chat"));
        listAnimal.add(new Animal("Hulk",40,"gorille"));
        listAnimal.add(new Canard("Coin coin",1,"canard",17));
        listAnimal.add(new Chien("Doge",3,"chien","Roux"));

        for(Animal a : listAnimal){
            System.out.println(a.toString());
        }

        System.out.println("\n");

        for (Animal a : listAnimal){
            if( a instanceof Chien){
                Chien c = (Chien)a;
                c.aboyer();
            }
        }

        System.out.println("\n");

        ArrayList<Navigable> listNavigable = new ArrayList<>();
        listNavigable.add(new Bateau("Riva","noir","Iseo",556));
        listNavigable.add(new Canard("Laqué",1,"canard",200));

        for(Navigable n : listNavigable){
            n.naviguer();
        }

        System.out.println("\n");

        for(Navigable n : listNavigable){
            if( n instanceof Animal){
                Animal a = (Animal) n;
                System.out.println(a.toString());
            }
        }
    }
}

