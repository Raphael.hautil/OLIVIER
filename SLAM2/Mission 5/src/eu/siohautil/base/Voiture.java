/**
 * Classe: Voiture
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package eu.siohautil.base;
public class Voiture extends Véhicule{
    private int nbPlaces;

    /**
     * Créé une voiture vide
     */

    public Voiture(){

    }

    /**
     * Créé et associe la marque, la couleur, le modèle et le nombre de places de la voiture avec les valeurs saisies par l'utilisateur
     *
     * @param marque Marque de la voiture
     * @param couleur Couleur de la voiture
     * @param modele Modèle de la voiture
     * @param nbPlaces Nombre de places de la voiture
     */

    public Voiture(String marque, String couleur, String modele, int nbPlaces){
        super(marque,couleur,modele);
        this.nbPlaces=nbPlaces;
    }

    /**
     * Change le nombre de places de la voiture
     *
     * @param nbPlaces Nombre de places de la voiture
     */

    public void setNbPlaces(int nbPlaces) {
        this.nbPlaces = nbPlaces;
    }

    /**
     * Récupère et retourne le nombre de places de la voiture
     *
     * @return nbPlaces
     */

    public int getNbPlaces() {
        return this.nbPlaces;
    }

    /**
     * Méthode qui affiche une phrase quand la voiture démarre en faisant appel à la méthode de la classe Véhicule
     */

    @Override
    public void demarrer() {
        super.demarrer();
    }

    /**
     * Méthode qui affiche une phrase quand la voiture freine en faisant appel à la méthode de la classe Véhicule
     */

    @Override
    public void freiner() {
        super.freiner();
    }

    /**
     * Méthode qui affiche une phrase quand le véhicule se stationne
     */

    public void stationner(){
        System.out.println("La voiture " + marque + " " + modele + " " + couleur + " se stationne.");
    }
}
