package eu.siohautil.base;

/**
 * Classe: Véhicule
 * @author: Raphaël Olivier
 * @version: 1.0
 */

abstract class Véhicule {
    protected String marque;
    protected String couleur;
    protected String modele;

    /**
     * Créé un véhicule vide
     */

    public Véhicule(){

    }

    /**
     * Créé et associe la marque, la couleur et le modèle du véhicule avec les valeurs entrées par l'utilisateur
     *
     * @param marque Marque du véhicule
     * @param couleur Couleur du véhicule
     * @param modele Modèle du véhicule
     */

    public Véhicule(String marque, String couleur, String modele) {
        this.marque=marque;
        this.couleur=couleur;
        this.modele=modele;
    }

    /**
     * Méthode qui affiche une phrase quand le véhicule démarre
     */

    public void demarrer(){
        System.out.println("Le véhicule " + marque + " " + modele + " " + couleur + " démarre.");
    }

    /**
     * Méthode qui affiche une phrase quand le véhicule freine
     */

    public void freiner(){
        System.out.println("Le véhicule " + marque + " " + modele + " " + couleur + " freine.");
    }
}
