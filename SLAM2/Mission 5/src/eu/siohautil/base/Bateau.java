/**
 * Classe: Bateau
 * @author: Raphaël Olivier
 * @version: 1.0
 */

package eu.siohautil.base;
public class Bateau extends Véhicule implements Navigable{
    private int volume;

    /**
     * Créé un bateau vide
     */

    public Bateau(){

    }

    /**
     * Créé et associe la marque, la couleur, le modèle et le volume du bateau avec les valeurs saisies par l'utilisateur
     *
     * @param marque Marque du bateau
     * @param couleur Couleur du bateau
     * @param modele Modèle du bateau
     * @param volume Volume du bateau
     */

    public Bateau(String marque, String couleur, String modele, int volume){
        super(marque, couleur, modele);
        this.volume=volume;
    }

    /**
     * Change le volume du bateau
     *
     * @param volume Volume du bateau
     */

    public void setVolume(int volume){
        this.volume=volume;
    }

    /**
     * Récupère et retourne le volume du bateau
     *
     * @return volume
     */

    public int getVolume(){
        return this.volume;
    }

    /**
     * Méthode qui affiche une phrase quand le bateau démarre en faisant appel à la méthode de la classe Véhicule
     */

    @Override
    public void demarrer() {
        super.demarrer();
    }

    /**
     * Méthode qui affiche une phrase quand le bateau freine en faisant appel à la méthode de la classe Véhicule
     */

    @Override
    public void freiner() {
        super.freiner();
    }

    /**
     * Méthode qui affiche une phrase quand le bateau accoste
     */

    @Override
    public void accoster() {
        System.out.println("Le bateau " + marque + " " + modele + " " + couleur + " accoste.");
    }

    /**
     * Méthode qui affiche une phrase quand le bateau navigue
     */

    @Override
    public void naviguer() {
        System.out.println("Le bateau " + marque + " " + modele + " " + couleur + " navigue.");
    }

    /**
     * Méthode qui affiche une phrase quand le bateau coule
     */

    @Override
    public void couler() {
        System.out.println("Le bateau " + marque + " " + modele + " " + couleur + "coule.");
    }
}
