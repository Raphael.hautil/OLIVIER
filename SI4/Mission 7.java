Question 1:

public class Mission7 {
    public static void main(String[] args) {

        String[] nom = {"Michel", "Robert", "Thomas", "Matis", "Benjamin", "Chloé", "Marie", "Paul", "Lucas", "Raphaël"};
        double[][] note = {{15, 12, 8}, {4, 7, 18}, {2, 6, 5}, {20, 18, 15.5}, {7.25, 15, 9.5}, {19, 18, 20}, {5.5, 7, 2}, {12, 13, 9.5}, {17, 11, 10}, {20, 20, 20}};
        for (int i = 0; i < 10; i++) {
            System.out.println(nom[i]);
            for (int j=0; j<3; j++){
                System.out.println(note[i][j]);
            }
        }
    }
}


Question 2:

public class Mission7 {
    public static void main(String[] args) {

        String[] nom = {"Michel", "Robert", "Thomas", "Matis", "Benjamin", "Chloé", "Marie", "Paul", "Lucas", "Raphaël"};
        double[][] note = {{15, 12, 8}, {4, 7, 18}, {2, 6, 5}, {20, 18, 15.5}, {7.25, 15, 9.5}, {19, 18, 20}, {5.5, 7, 2}, {12, 13, 9.5}, {17, 11, 10}, {20, 20, 20}};
        double[] moy = new double [10];
        double mclasse=0;

        for (int i = 0; i < 10; i++) {
            double m=0;
            System.out.println(nom[i]);
            for(int j = 0; j < 3; j++) {
                System.out.println(note[i][j]);
                m=m + note[i][j];
            }
            m=m/3;
            moy[i]=m;
            mclasse=mclasse+m;
            System.out.println("La moyenne de "+nom[i]+" est de "+m);
        }
        mclasse=mclasse/10;
        System.out.println("La moyenne de la classe est de "+mclasse);
    }
}


Question 3:

import java.util.Scanner;
public class Mission7 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String[] nom = {"Michel", "Robert", "Thomas", "Matis", "Benjamin", "Chloé", "Marie", "Paul", "Lucas", "Raphaël"};
        double[][] note = {{15, 12, 8}, {4, 7, 18}, {2, 6, 5}, {20, 18, 15.5}, {7.25, 15, 9.5}, {19, 18, 20}, {5.5, 7, 2}, {12, 13, 9.5}, {17, 11, 10}, {20, 20, 20}};
        double[] moy = new double [10];
        double mclasse=0;

        for (int i = 0; i < 10; i++) {
            double m=0;
            System.out.println(nom[i]);
            for(int j = 0; j < 3; j++) {
                System.out.println(note[i][j]);
                m=m + note[i][j];
            }
            m=m/3;
            moy[i]=m;
            mclasse=mclasse+m;
            System.out.println("La moyenne de "+nom[i]+" est de "+m);
        }
        mclasse=mclasse/10;
        System.out.println("La moyenne de la classe est de "+mclasse);






        System.out.print( "\nVous cherchez un élève en particulier ? Entrer son nom ici : ");
        String rech = sc.next();

        for (int i = 0; i < 10 ; i++)
        {
            if (rech.equals(nom[i]))
            {
                System.out.println(nom[i] + " a obtenu les notes suivantes : " + note[i][0] + ", " + note[i][1] + " et " + note[i][2] + ". Il a une moyenne de " + moy[i] + " et la moyenne de la classe est " + mclasse);
            }
        }

        if ( !rech.equals(nom[0]) && !rech.equals(nom[1]) && !rech.equals(nom[2]) && !rech.equals(nom[3]) && !rech.equals(nom[4]) && !rech.equals(nom[5]) && !rech.equals(nom[6]) && !rech.equals(nom[7])&& !rech.equals(nom[8]) && !rech.equals(nom[9]) )
        {
            System.out.println("-1");
        }
    }
}