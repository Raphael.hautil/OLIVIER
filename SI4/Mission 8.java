Question 1:

package com.company;

import java.util.Scanner;


class Mission8 {
    static void phrase(String x, int y){
        System.out.println("Vous êtes "+x+" et vous avez "+y+ " ans.");
    }


    private static Scanner sc;
    public static void main (String[] args){
        sc = new Scanner(System.in);

        int min;
        int max;
        int sum = 0;
        String nom[] = new String[5];
        int[] age = new int[5];
        String categorie[] = new String[5];
        for(int i=0; i<5; i++) {
            System.out.println("Saisissez votre nom.");
            nom[i] = sc.next();
            System.out.println("Saisissez votre âge.");
            age[i] = sc.nextInt();

            Mission8.phrase(nom[i], age[i]);

            if(age[i]>0 && age[i]<2){
                categorie[i] = "nourisson";
            }
            else if(age[i]>=2 && age[i]<12){
                categorie[i] = "enfant";
            }
            else if(age[i]>=12 && age[i]<55){
                categorie[i] = "adulte";
            }
            else{
                categorie[i] = "senior";
            }
            sum = sum + age[i];
        }
        sum = sum/5;
        min = age[0];
        max = age[0];
        for(int i=0; i<5; i++){
            if(age[i] < min){
                min = age[i];
            }
            if(age[i] > max){
                max = age[i];
            }
        }
        for(int i=0; i<5; i++){
            System.out.println(nom[i] + " a " + age[i] + " ans et appartient à la catégorie " + categorie[i] + ".");
        }
        System.out.println("L'âge minimal est " + min + " et l'âge maximal est " + max + ". La moyenne d'âge des voyageurs est de " + sum + "ans.");
    }

}


Question 2:

package com.company;

import java.util.Scanner;


class Mission8 {
    static void phrase(String x, int y) {
        System.out.println("Vous êtes " + x + " et vous avez " + y + " ans.");
    }
    static void complet(String x, int y) {
            System.out.println(x+": "+y+" ans");
    }


    private static Scanner sc;
    public static void main (String[] args){
        sc = new Scanner(System.in);

        int min;
        int max;
        int sum = 0;
        String nom[] = new String[5];
        int[] age = new int[5];
        String categorie[] = new String[5];
        for(int i=0; i<5; i++) {
            System.out.println("Saisissez votre nom.");
            nom[i] = sc.next();
            System.out.println("Saisissez votre âge.");
            age[i] = sc.nextInt();

            Mission8.phrase(nom[i], age[i]);

            if(age[i]>0 && age[i]<2){
                categorie[i] = "nourisson";
            }
            else if(age[i]>=2 && age[i]<12){
                categorie[i] = "enfant";
            }
            else if(age[i]>=12 && age[i]<55){
                categorie[i] = "adulte";
            }
            else{
                categorie[i] = "senior";
            }
            sum = sum + age[i];
        }
        sum = sum/5;
        min = age[0];
        max = age[0];
        for(int i=0; i<5; i++){
            if(age[i] < min){
                min = age[i];
            }
            if(age[i] > max){
                max = age[i];
            }
        }
        System.out.println("Les voyageurs sont:");
        for(int i=0; i<5; i++) {
            Mission8.complet(nom[i], age[i]);
        }
        System.out.println("L'âge minimal est " + min + " et l'âge maximal est " + max + ". La moyenne d'âge des voyageurs est de " + sum + " ans.");
    }

}


Question 3:

package com.company;

import java.util.Scanner;


class Mission8 {
    public static void phrase(String x, int y) {
        System.out.println("Vous êtes " + x + " et vous avez " + y + " ans.");
    }
    public static void complet(String x, int y) {
        String cat;
        if(y>0 && y<2){
            cat = "nourisson";
        }
        else if(y>=2 && y<12){
            cat = "enfant";
        }
        else if(y>=12 && y<55){
            cat = "adulte";
        }
        else{
            cat = "senior";
        }
        System.out.println(x+": "+y+" ans, il appartient à la catégorie: "+cat);
    }
    public static String categorie(int x){
        String cat;
        if(x>0 && x<2){
            cat = "nourisson";
        }
        else if(x>=2 && x<12){
            cat = "enfant";
        }
        else if(x>=12 && x<55){
            cat = "adulte";
        }
        else{
            cat = "senior";
        }
        return cat;
    }


    private static Scanner sc;
    public static void main (String[] args){
        sc = new Scanner(System.in);

        int min;
        int max;
        int sum = 0;
        String nom[] = new String[5];
        int[] age = new int[5];
        String categorie;

        for(int i=0; i<5; i++) {
            System.out.println("Saisissez votre nom.");
            nom[i] = sc.next();
            System.out.println("Saisissez votre âge.");
            age[i] = sc.nextInt();

            Mission8.phrase(nom[i], age[i]);

            categorie = Mission8.categorie(age[i]);
            System.out.println("Vous êtes dans la catégorie: "+categorie);

            sum = sum + age[i];
        }
        sum = sum/5;
        min = age[0];
        max = age[0];
        for(int i=0; i<5; i++){
            if(age[i] < min){
                min = age[i];
            }
            if(age[i] > max){
                max = age[i];
            }
        }
        System.out.println("Les voyageurs sont:");
        for(int i=0; i<5; i++) {
            Mission8.complet(nom[i], age[i]);
            categorie = Mission8.categorie(age[i]);
        }
        System.out.println("L'âge minimal est " + min + " et l'âge maximal est " + max + ". La moyenne d'âge des voyageurs est de " + sum + " ans.");
    }

}


Question 4:

package com.company;

import java.util.Scanner;


class Mission8 {
    public static void phrase(String x, int y) {
        System.out.println("Vous êtes " + x + " et vous avez " + y + " ans.");
    }
    public static void complet(String x, int y) {
        String cat;
        if(y>0 && y<2){
            cat = "nourisson";
        }
        else if(y>=2 && y<12){
            cat = "enfant";
        }
        else if(y>=12 && y<55){
            cat = "adulte";
        }
        else{
            cat = "senior";
        }
        System.out.println(x+": "+y+" ans, il appartient à la catégorie: "+cat);
    }
    public static String categorie(int x){
        String cat;
        if(x>0 && x<2){
            cat = "nourisson";
        }
        else if(x>=2 && x<12){
            cat = "enfant";
        }
        else if(x>=12 && x<55){
            cat = "adulte";
        }
        else{
            cat = "senior";
        }
        return cat;
    }
    static double moyenne(int[] x){
        double moy=0;
        for (int i=0; i<x.length; i++){
            moy = moy + x[i];
        }
        moy=moy/5;
        return moy;
    }

    static int minimum (int[] x){
        int min=x[0];
        for (int i=0; i<x.length; i++){
            if (min>=x[i]){
                min=x[i];
            }
        }
        return min;
    }

    static int maximum (int[] x){
        int max=x[0];
        for (int i=0; i<x.length; i++){
            if (max<=x[i]){
                max=x[i];
            }
        }
        return max;
    }


    private static Scanner sc;
    public static void main (String[] args) {
        sc = new Scanner(System.in);

        int min;
        int max;
        double moye=0;
        String nom[] = new String[5];
        int[] age = new int[5];
        String categorie;

        for (int i = 0; i < 5; i++) {
            System.out.println("Saisissez votre nom.");
            nom[i] = sc.next();
            System.out.println("Saisissez votre âge.");
            age[i] = sc.nextInt();

            Mission8.phrase(nom[i], age[i]);

            categorie = Mission8.categorie(age[i]);
            System.out.println("Vous êtes dans la catégorie: " + categorie);

        }

        System.out.println("Les voyageurs sont:");
        for (int i = 0; i < 5; i++) {
            Mission8.complet(nom[i], age[i]);
        }
        moye=Mission8.moyenne(age);
        min=Mission8.minimum(age);
        max=Mission8.maximum(age);

        System.out.println("L'âge minimal des voyageurs est de " + min + " ans et l'âge maximal des voyageurs est de " + max + " ans. La moyenne d'âge des voyageurs est de " + moye + " ans.");
    }

}