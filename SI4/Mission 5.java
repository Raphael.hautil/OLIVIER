Question 1:

import java.util.Scanner;
public class mission5 {
    public static void main (String[] args){
     int y=0;
     int i;
     String f="0";
     Scanner X=new Scanner(System.in);

        for(i=1;i<=10;i++){
            int x=X.nextInt();
            System.out.println("Saisissez un nombre");
            f=f+"+"+x;
            y=y+x;
        }
    System.out.print(f+"="+y);
    }
}


/*
Algo:
    variables : nb, somme, indice entier
                add chaine
    Début :
        somme = 0
        Pour indice de 0 à 10 inclus pas 1
            Faire
                Saisir nb
                add = add, " + ", nb
                somme = somme + nb
        FinPour
        Afficher "0", add, " = ", somme
    Fin
 */

Question 2:

package com.company;

import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        int y=0;
        int i;
        String f="0";

        Scanner X=new Scanner(System.in);

        for(i=1;i<=5;i++) {
            Scanner Saisie=new Scanner(System.in);
            System.out.println("Addition (1) ou soustraction (2) ?");
            int saisie = Saisie.nextInt();

            while(saisie <=0 || saisie > 2){
                System.out.println("Faites un choix valide");
                System.out.println("Addition (1) ou soustraction (2) ?");
                saisie=Saisie.nextInt();
            }

            if(saisie==1){
                System.out.println("Vous avez choisi l'addition");
                System.out.println("Saisissez un nombre");
                int x=X.nextInt();
                f=f+"+"+x;
                y=y+x;
            }
            if(saisie==2){
                System.out.println("Vous avez choisi la soustraction");
                System.out.println("Saisissez un nombre");
                int x=X.nextInt();
                f=f+"-"+x;
                y=y-x;
            }
        }
        System.out.print(f+"="+y);
    }

}


/*
Algo:
    variables : nb, rep, choix, indice entier
                res string

    Début
        nb <= 0
        rep <= 0

        Pour indice de 0 a 5 inclus pas 1
            Faire
                Saisir nb
                Saisir choix
                Si choix = 1
                    Alors
                        Si indice = 5
                            res = res + nb
                            rep = rep + nb
                        Sinon
                            res = res + nb + " + "
                            rep = rep + nb
                        FinSi
                Sinon
                    res = res + " - " + nb
                    rep = rep - nb
                FinSi
        FinPour
        Afficher res + " = " + rep
    Fin
 */


Question 3:

import java.util.Scanner;
public class question3 {
    public static void main(String[] args) {
        int random = 0 + (int)(Math.random() * ((100 - 0) + 1));
        int i;
        Scanner X=new Scanner(System.in);

        for(i=1;i<=12;i++){
            System.out.println("Saisissez un nombre");
            int x=X.nextInt();

            if(x==random){
                System.out.println("Bravo vous avez trouvé en "+i+" essais");
            }

            if(x<random){
                System.out.println("C'est plus !");
            }

            if(x>random){
                System.out.println("C'est moins !");
            }
        }
    }

}


/*
Algo:
    variables : rand, essai, indice entier

    Début
        Générer entre 0 et 100
        Afficher "Un nombre entre 0 et 100 à été généré."

        Pour indice de 1 à 12 inclus pas 1
            Faire
                Saisir essai
                    Si essai = rand
                        Alors
                            Afficher "Vous avez trouvé la bonne réponse, bravo !"
                            FinPour
                    Sinon
                        Afficher "Dommage c'est la mauvaise réponse."

                        Si essai > rand
                            Alors
                                Afficher "Le nombre à deviner est plus petit."
                        Sinon
                            Afficher "Le nombre à deviner est plus grand."
                        FinSi
                    FinSi
        FinPour
    Fin
 */
