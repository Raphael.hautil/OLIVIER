Question 1:
Java:
import java.util.Scanner;

public class Mission3 {

    public static void main(String[] args) {

        Scanner Age = new Scanner(System.in);
        System.out.println("Saisissez votre age");
        int age = Age.nextInt();

        if (age <= 3) {
            System.out.println("Vous êtes un nourrisson");
        } else if (age > 3 && age <=12)
        {
            System.out.println("Vous êtes un enfant");
        } else if (age > 12 && age <=18)
        {
            System.out.println("Vous êtes un adolescent");
        } else if (age > 18 && age <=55)
        {
            System.out.println("Vous êtes un adulte");
        } else if (age > 55) {
            System.out.println("Vous êtes un sénior");
        }

    }
}




Question 2:
import java.util.Scanner;

public class Mission3 {

    public static void main(String[] args) {
        String categorie=null;
        double prix=50;

        Scanner Age = new Scanner(System.in);
        System.out.println("Saisissez votre age");
        int age = Age.nextInt();

        if (age <= 3) {
            System.out.println("Vous êtes un nourrisson");
                categorie="nourrisson";
        } else if (age > 3 && age <=12)
        {
            System.out.println("Vous êtes un enfant");
                categorie="enfant";
        } else if (age > 12 && age <=18)
        {
            System.out.println("Vous êtes un adolescent");
                categorie="adolescent";
        } else if (age > 18 && age <=55)
        {
            System.out.println("Vous êtes un adulte");
                categorie="adulte";
        } else if (age > 55) {
            System.out.println("Vous êtes un sénior");
                categorie="senior";
        }


        if (categorie.equals("nourrisson"))
            {
                prix=5;
            }
                else if(categorie.equals("enfant"))
                {
                    prix=prix*0.3;
                }
                    else if(categorie.equals("senior"))
                {
                    prix=prix*0.5;
                }
    System.out.println(prix + "€");

    }
}


Question 3:
import java.util.Scanner;

public class Mission3 {

    public static void main(String[] args) {
        String categorie=null;
        double prix=0;

        Scanner Age = new Scanner(System.in);
        System.out.println("Saisissez votre age");
        int age = Age.nextInt();

        Scanner Spectacle = new Scanner(System.in);
        System.out.println("Choisissez votre spectacle");
        int spectacle = Spectacle.nextInt();

        if (age <= 3) {
            System.out.println("Vous êtes un nourrisson");
                categorie="nourrisson";
        } else if (age > 3 && age <=12)
        {
            System.out.println("Vous êtes un enfant");
                categorie="enfant";
        } else if (age > 12 && age <=18)
        {
            System.out.println("Vous êtes un adolescent");
                categorie="adolescent";
        } else if (age > 18 && age <=55)
        {
            System.out.println("Vous êtes un adulte");
                categorie="adulte";
        } else if (age > 55) {
            System.out.println("Vous êtes un sénior");
                categorie="senior";
        }


        if (spectacle == 1)
        {
            prix = 67;
        }
        else if (spectacle == 2)
        {
            prix=45;
        }
        else if (spectacle == 3)
        {
            prix=110;
        }
        else if (spectacle == 4)
        {
            prix=92;
        }
        else
        {
        System.out.println("Ce spectacle n'existe pas.")

        if (categorie.equals("nourrisson"))
            {
                prix=5;
            }
                else if(categorie.equals("enfant"))
                {
                    prix=prix*0.3;
                }
                    else if(categorie.equals("senior"))
                {
                    prix=prix*0.5;
                }
    System.out.println(prix + "€");

    }
}



Algo complet
/*
            variable :  age, entier
                        categorie chaine de caractères
                        prix float
            Début :
                    Saisir choix
                    Saisir age
                    prixBillet = 0
                    
                    
                        Si age < 3
                            alors afficher "Nourisson"
                            categorie = "Nourisson"
                        Sinon Si age >= 3 ET age < 12
                            alors afficher "Enfant"
                            categorie = "Enfant"
                        Sinon Si age >= 12 ET age < 18
                            alors afficher "Adolescent"
                            categorie = "Adolescent"
                        Sinon Si age >= 18 ET age < 55
                            alors afficher "Adulte"
                            categorie = "Adulte"
                        Sinon
                            afficher "Senior"
                            categorie = "Senior"
                        FinSi
                        
                        Si categorie = 1
                            alors prix = 67
                        Sinon Si categorie = 2
                            alors prix = 45
                        Sinon Si categorie = 3
                            alors prix = 110
                        Sinon Si categorie = 4
                            alors prix = 92
                        Sinon
                            afficher "Ce spectacle n'existe pas."
                            Fin
                        FinSi

                    
                        Si categorie = "Nourisson"
                            prixBillet = 5
                        Sinon Si categorie = "Enfant"
                            prixBillet = prixBillet * 0.3
                        Sinon Si categorie = "Senior"
                            prixBillet = prixBillet * 0.5
                        
                        FinSi
                            afficher prix, "€"
            Fin
         */