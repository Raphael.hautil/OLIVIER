Question 1:

package com.company;
import java.util.Scanner;
public class Mission6 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String[] nom = new String[6];
        int [] age = new int [6];

        System.out.println("Entrer 5 valeurs");
        for (int i=1 ; i < 6 ; i++)
        {
            System.out.println("Entrer le nom du voyageur n°"+ i + " : ");
            nom[i] = sc.next();

            System.out.println("Entrer l'âge du voyageur n°"+ i + " : ");
            age[i] = sc.nextInt();
        }

        for (int i = 1; i < 6 ; i++)
        {
            System.out.println("Le nom du voyageur est "+ nom[i] +" et il a "+ age[i] +" ans.");
        }
    }
}


Question 2:

package com.company;
import java.util.Scanner;
public class Mission6 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String[] nom ={"Michel","Roger","Gérard","Jacqueline","Paulette"};
        int [] age = new int [5];

        System.out.println("Entrer 5 valeurs");
        for (int i=0 ; i < 6 ; i++)
        {
            System.out.println("Entrer l'age de "+ nom[i]);
            age[i] = sc.nextInt();
            System.out.println("Le voyageur "+ nom[i] +" a "+ age[i] +" ans.");
        }
    }
}


Question 3:

package com.company;
import java.util.Scanner;
public class Mission6 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String[] nom ={"Michel","Roger","Gérard","Jacqueline","Paulette"};
        int [] age = new int [5];

        System.out.println("Entrer 5 valeurs");
        for (int i=0 ; i < 6 ; i++)
        {
            System.out.println("Entrer l'age de "+ nom[i]);
            age[i] = sc.nextInt();

            if (age[i]>=0 && age[i]<2) {
                System.out.println("Le voyageur "+ nom[i] +" a "+ age[i] +" ans, c'est un nourrisson.");

            } else if (age[i] >= 2 && age[i] <12){
                System.out.println("Le voyageur "+ nom[i] +" a "+ age[i] +" ans, c'est un enfant.");

            } else if (age[i] >=12 && age[i] <=55){
                System.out.println("Le voyageur "+ nom[i] +" a "+ age[i] +" ans, c'est un adulte.");

            } else if (age[i] > 55){
                System.out.println("Le voyageur "+ nom[i] +" a "+ age[i] +" ans, c'est un sénior.");
            }
        }
    }
}

Question 4:

package com.company;

import java.util.Scanner;
public class Mission6 {
    private static Scanner sc;
    public static void main (String[] args){
        sc = new Scanner(System.in);

        int min;
        int max;
        int sum = 0;
        String nom[] = new String[5];
        int age[] = new int[5];
        String categorie[] = new String[5];
        for(int i=0; i<5; i++) {
            System.out.println("Saisissez votre nom.");
            nom[i] = sc.next();
            System.out.println("Saisissez votre âge.");
            age[i] = sc.nextInt();

            if(age[i]>0 && age[i]<2){
                categorie[i] = "nourisson";
            }
            else if(age[i]>=2 && age[i]<12){
                categorie[i] = "enfant";
            }
            else if(age[i]>=12 && age[i]<55){
                categorie[i] = "adulte";
            }
            else{
                categorie[i] = "senior";
            }
            sum = sum + age[i];
        }
        sum = sum/5;
        min = age[0];
        max = age[0];
        for(int i=0; i<5; i++){
            if(age[i] < min){
                min = age[i];
            }
            if(age[i] > max){
                max = age[i];
            }
        }
        for(int i=0; i<5; i++){
            System.out.println(nom[i] + " a " + age[i] + " ans et appartient à la catégorie " + categorie[i] + ".");
        }
        System.out.println("L'âge minimal est " + min + " et l'âge maximal est " + max + ". La moyenne d'âge des voyageurs est de " + sum + "ans.");
    }

}


Algo général:

/*
variables : min, max, sum, i entiers
            age tableau d'entiers de taille 5
            nom, categorie tableaux de chaines de caractère de taille 5

Debut
    sum <- 0
    Pour i allant de 0 à 5 exclu pas 1
        Faire
            Saisir nom[i]
            Saisir age[i]

            Si age[i] > 0 et age[i] < 2
                alors categorie[i] <- "nourisson"
            Sinon Si age[i] >= 2 et age[i]< 12
                alors categorie[i] <- "enfant"
            Sinon Si age[i] >= 12 et age[i]< 55
                alors categorie[i] <- "adulte"
            Sinon
                categorie[i] <- "senior"
            FinSi
            sum <- sum + age[i]
    FinPour
    sum <- sum/5
    min <- age[0]
    max <- age[0]
    Pour i allant de 0 à 5 exclu pas 1
        Faire
            Si age[i] < min
                Alors min <- age[i]
            FinSi
            Si age[i] > max
                Alors max <- age[i]
            FinSi
    FinPour
    Pour i allant de 0 à 5 exclu pas 1
        Faire
            Afficher nom[i], " a ", age[i], " ans et appartient à la catégorie ", categorie, "."
    FinPour
    Afficher "L'âge minimal est ", min, " et l'âge maximal est ", max, ". La moyenne d'âge des voyageurs est de ", sum, "ans."
 */