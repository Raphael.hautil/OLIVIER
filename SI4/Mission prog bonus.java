Question 1:

import java.util.Scanner;
public class Question1 {
    public static void main(String[] args) {
        Scanner nbre1 = new Scanner(System.in);
        System.out.println("Saisissez un nombre");
        int nombre1 = nbre1.nextInt();

        if (nombre1 % 2 == 0) {
            System.out.println("Le nombre est pair");
        } else {
            System.out.println("Le nombre est impair");
        }
    }
}

Question 2:

import java.util.Scanner;
public class Question1 {
    public static void main(String[] args) {
        Scanner nbre1 = new Scanner(System.in);
        System.out.println("Saisissez votre premier nombre");
        int nombre1 = nbre1.nextInt();

        Scanner nbre2 = new Scanner(System.in);
        System.out.println("Saisissez votre second nombre");
        int nombre2 = nbre2.nextInt();

        if (nombre1 < 0 || nombre2 < 0) {
            System.out.println("Le produit est négatif");
        }
        if (nombre1 < 0 && nombre2 < 0) {
            System.out.println("Le produit est positif");
        }
        if (nombre1 > 0 && nombre2 > 0){
            System.out.println("Le produit est positif");
        }
    }
}