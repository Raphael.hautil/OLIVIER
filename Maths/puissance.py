def puissance(a,n):
    a=a**n
    return a

def puissance2(a,n):
    if n==0:
        return 1
    else:
        return a*puissance2(a,n-1)

def fibonacci(n):
    if n==0 or n==1:
        return 1
    else:
        return fibonacci(n-1) + fibonacci(n-2)

def rapfibo(n):
    return fibonacci(n)/fibonacci(n-1)
        
