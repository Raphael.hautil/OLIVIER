def prodmat(A,B):
    n=len(A)
    p=len(A[0])
    q=len(B[0])
    if p!=len(B):
        return "ERREUR !"
    else:
        C=[[0 for i in range(q)]for i in range(n)]
        for i in range(n):
            for j in range(q):
                C[i][j]=0
                for k in range(p):
                    C[i][j]=C[i][j]+A[i][k]*B[k][j]
    return C
