def fact1(n):
    fact=1
    if n==0:
        fact=1
    for i in range(1,n+1):
        fact=fact*i
    return fact

def fact2(n):
    if n==0:
        return 1
    else:
        return n*fact2(n-1)
    
