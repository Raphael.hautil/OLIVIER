def divisor(n):
    diviseurs=[]
    for i in range(1,n+1):
        if n%i== 0:
            diviseurs.append(i)
    return diviseurs


def estPremier(n):
    if len(divisor(n))== 2:
        return 1
    else:
        return -1


def plusPetitPremier(m):
    while estPremier(m) == -1:
        m=m+1
    return m


def trouvePremier(i):
    k=0 # Nombre courant testé
    j=0 # compteur de nombres premiers
    while j != i:
        k=plusPetitPremier(k)+1
        j=j+1
    return k-1


def main():
    j=int(input("Saisissez un chiffre"))
        
    

