def compression(donnees):
    compr=[]
    n=len(donnees)
    for j in range(n):
        compr.append(0)
        
    premier=donnees[0]
    j=0
    courant=premier

    for i in range(n):
        if donnees[i]==courant:
            compr[j]=compr[j]+1
        else:
            j=j+1
            courant=donnees[i]
            compr[j]=1

    return compr, premier

def decompression(compr, premier):
    n=len(compr)
    donnees=[]
    element=premier
    for k in range(n):
        for i in range(compr[k]):
            donnees.append(element)
        element=1-element      
    return donnees
