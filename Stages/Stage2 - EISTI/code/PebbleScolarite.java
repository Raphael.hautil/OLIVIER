public static void showStudentsNotesSummary(ModelMap model, HttpServletRequest request, UserDao uDao, AdminDao aDao, MarkDao mDao,
                                                ProgramDao pDao, GroupDao gDao , StudentDao sDao, TeachingUnitDao tDao, SecurityUser activeUser,
                                                Long programId, Long groupId, Long studentId){
        PebbleCommon.newTemplate(model, request, uDao, aDao, activeUser);
        model.addAttribute("breadcrumbList", PebbleScolarite.getBreadcrumb().getBreadcrumb());
        model.addAttribute("programList", pDao.getPrograms());
        if (programId != null) {
            model.addAttribute("programId", programId);
            model.addAttribute("groupList", gDao.getGroupFromProgram(programId));
        }
        if (programId != null && groupId != null){
            model.addAttribute("programId", programId);
            model.addAttribute("groupId", groupId);
            model.addAttribute("studentsList", sDao.getStudentsFromGroup(groupId));
            model.addAttribute("teachingUnitList", tDao.getTeachingUnitsFromProgram(groupId, sDao, mDao));
        }
        if (programId != null && groupId != null && studentId != null){
            model.addAttribute("programId", programId);
            model.addAttribute("groupId", groupId);
            model.addAttribute("studentId", studentId);
            model.addAttribute("teachingUnitList", tDao.getTeachingUnitsFromProgram(groupId, sDao, mDao));
        }
    }
