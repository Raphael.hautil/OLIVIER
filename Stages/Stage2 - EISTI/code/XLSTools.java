public static void buildGroupNotesSummaryList(HttpServletResponse response,
                                                  SecurityUser activeUser, long idProgram, long idGroup,
                                                  boolean semestre1, boolean semestre2, List<Boolean> teachingUnitGroupList, List<String> teachingUnitGroupId,
                                                  List<Boolean> teachingUnitStudentList, List<String> teachingUnitStudentId, MarkDao mDao, TeachingUnitDao tDao,
                                                  ProgramDao pDao, GroupDao gDao, StudentDao sDao, ModuleDao moDao)throws IOException{

        try{
            int sem1;

            String titre = "program: " + idProgram + "group: " + idGroup;

            List<BooleanId> listTeachingU = new ArrayList<>();
            List<GroupUser> students;
            List<List<ModuleTeachingUnit>> modules = new ArrayList<>();
            List<Module> modulesReal = new ArrayList<>();
            List<TeachingUnit> teachingUnits = new ArrayList<>();

            students = sDao.getStudentsFromGroup(idGroup);
            Group group = gDao.getGroupById(idGroup);
            Program program = pDao.getProgram(idProgram);

            for (int i = 0; i<teachingUnitGroupList.size(); i++){
                BooleanId bId = new BooleanId();
                bId.setId(Long.valueOf(teachingUnitGroupId.get(i)));
                bId.setaBoolean(teachingUnitGroupList.get(i));
                listTeachingU.add(bId);
            }

            for (BooleanId bId : listTeachingU){
                if (bId.isaBoolean()){
                    teachingUnits.add(tDao.getTeachingUnitById(bId.getId()));
                    modules.add(moDao.getModulesFromTeachingUnit(bId.getId()));
                }
            }

            for (List<ModuleTeachingUnit> module1 : modules) {
                for (ModuleTeachingUnit moduleTeachingUnit : module1) {
                    modulesReal.add(moduleTeachingUnit.getModule());
                }
            }

            List<Long> moduleLength = new ArrayList<>();
            moduleLength.add((long) 0);
            for(TeachingUnit teachingUnit : teachingUnits){
                moduleLength.add(mDao.getModuleSize(teachingUnit.getpId()));
            }

            for (GroupUser student : students) {
                List<ViewAverageModuleUe> moyenne = new ArrayList<>();
                Long studentId = student.getpId().getPfUser();
                for (List<ModuleTeachingUnit> module : modules) {
                    for (ModuleTeachingUnit moduleTeachingUnit : module) {
                        moyenne.add(mDao.getStudentAveragesForModule(studentId, moduleTeachingUnit.getModule().getpId()));
                    }
                }
                student.setMoyenne(moyenne);
            }

            if (semestre1){
                sem1 = 262;
                titre += "semestre: " + sem1;
            }

            titre += ".xls";

            response.setContentType("application/excel; charset=utf-8");
            response.setHeader("Content-disposition", "attachment;filename="+Common.string2url(titre));
            ServletOutputStream out = response.getOutputStream();
            createExcelFile(students, teachingUnits, modulesReal, program, group, moduleLength, semestre1, semestre2, out);
            out.close();
        }
            catch(Exception e){
            LOGGER.fatal("Error while building group notes summary", e);
        }
    }

    private static void createExcelFile(List<GroupUser> groupUser, List<TeachingUnit> listTeachingUnit, List<Module> listModule, Program program, Group group, List<Long> moduleLength, boolean semestre1, boolean semestre2, OutputStream out)throws IOException{
        HSSFWorkbook wb = new HSSFWorkbook();

        final CellStyle grey = wb.createCellStyle();
        grey.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        grey.setFillPattern(CellStyle.SOLID_FOREGROUND);

        CellStyle cellStyleCentered = wb.createCellStyle();
        cellStyleCentered.setAlignment(CellStyle.ALIGN_LEFT);
        cellStyleCentered.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        cellStyleCentered.setBorderTop((short)1);
        cellStyleCentered.setBorderBottom((short)1);
        cellStyleCentered.setBorderLeft((short)1);
        cellStyleCentered.setBorderRight((short)1);

        CellStyle cellCentered = wb.createCellStyle();
        cellCentered.setAlignment(CellStyle.ALIGN_LEFT);
        cellCentered.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

        CellStyle cellFullCentered = wb.createCellStyle();
        cellFullCentered.setAlignment(CellStyle.ALIGN_CENTER);
        cellFullCentered.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        cellFullCentered.setBorderTop((short)1);
        cellFullCentered.setBorderBottom((short)1);
        cellFullCentered.setBorderLeft((short)1);
        cellFullCentered.setBorderRight((short)1);

        CellStyle decimal = wb.createCellStyle();
        decimal.setBorderTop((short)1);
        decimal.setBorderBottom((short)1);
        decimal.setBorderLeft((short)1);
        decimal.setBorderRight((short)1);
        decimal.setDataFormat(wb.createDataFormat().getFormat("0.00"));

        HSSFSheet sheet = wb.createSheet("Conseil de classe");
        HSSFRow row;
        HSSFCell c;

        String title = "Conseil de classe du groupe: " + group.getLabel() + " qui suit le programme " + program.getLabel();

        row = sheet.createRow(0);
        c = row.createCell(0);
        c.setCellStyle(cellCentered);
        c.setCellValue(title);

        int nbRow = 2;
        int nbCell = 2;

        sheet.addMergedRegion(new CellRangeAddress(0,1,0,nbCell));

        nbCell++;
        int nbCell2 = 0;

        c = row.createCell(3);
        c.setCellStyle(cellFullCentered);
        c.setCellValue("UNITES D'ENSEIGNEMENT");
        sheet.addMergedRegion(new CellRangeAddress(0,1,3 + listModule.size() * nbCell2,2 + listModule.size() + listModule.size() * nbCell2));

        row = sheet.createRow(nbRow);

        for (int i = 0; i<moduleLength.size(); i++){
            int k = Math.toIntExact(moduleLength.get(i));
            if (k != 0) {
                int j = Math.toIntExact(moduleLength.get(i-1));
                sheet.addMergedRegion(new CellRangeAddress(nbRow, nbRow, 3 + j, 2 + k + j));
            }
        }

        sheet.addMergedRegion(new CellRangeAddress(2,2,0,2));
        c = row.createCell(0);
        c.setCellStyle(grey);

        for (int i = 0; i<listTeachingUnit.size(); i++) {
            int k = Math.toIntExact(moduleLength.get(i));
            c = row.createCell(nbCell+ i*k);
            c.setCellStyle(cellFullCentered);
            c.setCellValue(listTeachingUnit.get(i).getLabel());
        }

        nbRow++;

        row = sheet.createRow(nbRow);
        c = row.createCell(0);
        c.setCellStyle(cellStyleCentered);
        c.setCellValue("Nom");
        c = row.createCell(1);
        c.setCellStyle(cellStyleCentered);
        c.setCellValue("Prénom");
        c = row.createCell(2);
        c.setCellStyle(cellStyleCentered);
        c.setCellValue("Modules");


        for (Module module : listModule) {
            c = row.createCell(nbCell);
            c.setCellStyle(cellStyleCentered);
            c.setCellValue(module.getLabel());
            nbCell++;
        }

        nbRow++;

        sheet.addMergedRegion(new CellRangeAddress(4,3 + groupUser.size(),2,2));

        for (GroupUser gu : groupUser) {
            row = sheet.createRow(nbRow);
            c = row.createCell(0);
            c.setCellStyle(cellStyleCentered);
            c.setCellValue(gu.getUser().getLastname());

            c = row.createCell(1);
            c.setCellStyle(cellStyleCentered);
            c.setCellValue(gu.getUser().getFirstname());

            c = row.createCell(2);
            c.setCellStyle(cellFullCentered);
            c.setCellValue("Moyenne");

            for (int k = 0; k < gu.getMoyenne().size(); k++) {
                c = row.createCell(3 + k);
                c.setCellValue(gu.getMoyenne().get(k).getAverage());
                c.setCellStyle(decimal);
            }
            nbRow++;
        }

        for(int i=0; i<=nbCell; i++) {
            sheet.autoSizeColumn(i);
        }
        int width = ((int)(8 * 1.14388)) * 256;
        sheet.setColumnWidth(2, width);
        width = ((int)(10 * 1.14388)) * 256;
        sheet.setColumnWidth(3, width);
        wb.write(out);
    }
