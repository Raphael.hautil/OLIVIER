public List<GroupUser>getStudentsFromGroup(Long groupId){
        Query query = getCurrentSession().createQuery("FROM GroupUser " +
                "WHERE group = :groupId " +
                "ORDER BY user.label");

        query.setLong("groupId", groupId);
        return (List<GroupUser>) query.list();
    }

    public List<ModuleTeachingUnit> getModulesFromTeachingUnit(Long teachingUnitId){
        Query query = getCurrentSession().createQuery("FROM ModuleTeachingUnit " +
                "WHERE teachingUnit = :teachingUnitId " +
                "ORDER BY module");
        query.setLong("teachingUnitId", teachingUnitId);
        return (List<ModuleTeachingUnit>) query.list();
    }

    public List<TeachingUnit> getTeachingUnitsFromProgram(Long idGroup, StudentDao sDao, MarkDao mDao){

        List<GroupUser> students;
        List<TeachingUnit> teachingUnits = new ArrayList<>();
        List<ScoMarks> sm;
        List<ScoExam> se = new ArrayList<>();

        students = sDao.getStudentsFromGroup(idGroup);

        sm = mDao.getScoMarksFromStudent(students.get(0).getpId().getPfUser());

        for (ScoMarks scoMarks : sm){
            se.add(mDao.getScoExamFromId(scoMarks.getScoExam().getpId()));
        }

        //TODO getTeachingUnitsFromProgram and getModuleFromTeachingUnit with the real database
        teachingUnits.add(getTeachingUnitById(se.get(0).getScoUnitModule().getScoUnit().getpId()));

        for (int i = 0; i<se.size(); i++){
            if (i+1 < se.size()){
                if (se.get(i).getScoUnitModule().getScoUnit().getpId() != se.get(i+1).getScoUnitModule().getScoUnit().getpId()){
                    teachingUnits.add(getTeachingUnitById(se.get(i+1).getScoUnitModule().getScoUnit().getpId()));
                }
            }
        }
        return teachingUnits;
    }
