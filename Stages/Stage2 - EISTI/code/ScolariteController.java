@Secured({ RoleSecurity.SCOLARITE })
    @RequestMapping(value = "/scolarite/studentsNotesSummary", method = RequestMethod.GET)
    public String showStudentsNotesSummary(HttpServletRequest request, @AuthenticationPrincipal SecurityUser activeUser, ModelMap model,
                                           @RequestParam(value = "programId", required = false) Long programId,
                                           @RequestParam(value = "groupId", required = false) Long groupId,
                                           @RequestParam(value = "studentId", required = false) Long studentId) {

        PebbleScolarite.showStudentsNotesSummary(model, request, uDao, aDao, mDao, pDao, gDao, stDao, tDao, activeUser, programId, groupId, studentId);
        return "scolarite/studentsNotesSummary";
    }

    @RequestMapping(value = {"/scolarite/editGroupesNomenclatures:downloadXLS", "scolarite/studentsNotesSummary"}, method = RequestMethod.POST)
    @ResponseBody
    public void downloadXLS2(HttpServletResponse response, @AuthenticationPrincipal SecurityUser activeUser,
                             @RequestParam long programId,
                             @RequestParam long groupId,
                             @RequestParam (required = false) boolean enableSemestre1,
                             @RequestParam (required = false) boolean enableSemestre2,
                             @RequestParam (required = false) List<Boolean> realTeachingUnitGroup,
                             @RequestParam (required = false) List<String> teachingUnitGroupId,
                             @RequestParam (required = false) List<Boolean> enableTeachingUnitStudent,
                             @RequestParam (required = false) List<String> teachingUnitStudentId){

        try{
            XLSTools.buildGroupNotesSummaryList(response, activeUser, programId, groupId, enableSemestre1, enableSemestre2, realTeachingUnitGroup, teachingUnitGroupId, enableTeachingUnitStudent, teachingUnitStudentId, mDao, tDao, pDao, gDao, stDao, moDao);
        }catch (IOException e){
            LOGGER.fatal("Error while building group notes summary", e);
        }
    }
