$(function(){
    $('select.selectpicker').selectpicker();
    $('#select-year, #select-program ').change(function () {
        if ($('#select-program').val() !== '') {
            window.location.href = WEB_ROOT + 'scolarite/studentsNotesSummary?programId=' + $('#select-program').val();
        }
    });
    $('#select-group').change(function () {
        if ($('#select-program').val() !== '' && $('#select-group').val() !== '') {
            window.location.href = WEB_ROOT + 'scolarite/studentsNotesSummary?programId=' + $('#select-program').val() + '&groupId=' + $('#select-group').val();
        }
    });
    $('#select-student').change(function () {
        if ($('#select-program').val() !== '' && $('#select-group').val() !== '' && $('#select-student').val() !== ''){
            window.location.href = WEB_ROOT + 'scolarite/studentsNotesSummary?programId=' + $('#select-program').val() + '&groupId=' + $('#select-group').val() + '&studentId=' + $('#select-student').val();
        }
    })
});

$(document).ready(function(){
    if ($('#select-program').val() !== '' && $('#select-group').val() !== ''){
        document.getElementById("export-options-group").style.display = 'block';
    }
});

$(document).ready(function(){
    if ($('#select-program').val() !== '' && $('#select-group').val() !== '' && $('#select-student').val() !== ''){
        document.getElementById('export-options-student').style.display = 'block';
    }
});

$(document).ready(function() {
    for (let i = 0; i < document.getElementById('idtu').options.length; i++) {
        let id = document.getElementById('idtu').options[i].value;
        if (document.getElementById('checkbox-teaching-unit-group-' + id).checked) {
            document.getElementById('real-teaching-unit-group-' + id).value = true;
        } else {
            document.getElementById('real-teaching-unit-group-' + id).value = false;
        }
        if (document.getElementById('checkbox-teaching-unit-student-' + id).checked) {
            document.getElementById('real-teaching-unit-student-' + id).value = true;
        } else {
            document.getElementById('real-teaching-unit-student-' + id).value = false;
        }
    }
});

function testGroup() {
    for (let i = 0; i < document.getElementById('idtu').options.length; i++) {
        let id = document.getElementById('idtu').options[i].value;
        if (document.getElementById('checkbox-teaching-unit-group-' + id).checked) {
            document.getElementById('real-teaching-unit-group-' + id).value = true;
        } else {
            document.getElementById('real-teaching-unit-group-' + id).value = false;
        }
    }
    for (let i = 0; i < document.getElementById('idtu').options.length; i++) {
        let id = document.getElementById('idtu').options[i].value;
        let id2 = document.getElementById('idtu').options[i+1].value;
        if (document.getElementById('real-teaching-unit-group-' + id).value == 'false' && document.getElementById('real-teaching-unit-group-' + id2).value == 'false'){
            let nodes = document.getElementById("btn-group").getElementsByTagName('*');
            for(let k = 0; k < nodes.length; k++){
                nodes[k].disabled = true;
            }
            document.getElementById('no-teaching-unit-group').style.display = 'block';
        }else {
            let nodes = document.getElementById("btn-group").getElementsByTagName('*');
            for(let k = 0; k < nodes.length; k++){
                nodes[k].disabled = false;
            }
            document.getElementById('no-teaching-unit-group').style.display = 'none';
        }
    }
}

function testStudent() {
    for (let i = 0; i < document.getElementById('idtu').options.length; i++) {
        let id = document.getElementById('idtu').options[i].value;
        if (document.getElementById('checkbox-teaching-unit-student-' + id).checked) {
            document.getElementById('real-teaching-unit-student-' + id).value = true;
        } else {
            document.getElementById('real-teaching-unit-student-' + id).value = false;
        }
    }
    for (let i = 0; i < document.getElementById('idtu').options.length; i++) {
        let id = document.getElementById('idtu').options[i].value;
        let id2 = document.getElementById('idtu').options[i+1].value;
        if (document.getElementById('real-teaching-unit-student-' + id).value == 'false' && document.getElementById('real-teaching-unit-student-' + id2).value == 'false'){
            let nodes = document.getElementById("btn-student").getElementsByTagName('*');
            for(let k = 0; k < nodes.length; k++){
                nodes[k].disabled = true;
            }
            document.getElementById('no-teaching-unit-student').style.display = 'block';
        }else {
            let nodes = document.getElementById("btn-student").getElementsByTagName('*');
            for(let k = 0; k < nodes.length; k++){
                nodes[k].disabled = false;
            }
            document.getElementById('no-teaching-unit-student').style.display = 'none';
        }
    }
}
