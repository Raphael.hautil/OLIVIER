defmodule Kalert.Slack do
  @endpoint "https://slack.com/api/"
  @token ""


  def user_list do
    {:ok, {_,_,body}} = :httpc.request(:get,{'#{@endpoint}users.list', [{'Authorization','Bearer #{@token}'}]}, [],[])
    res = body |> Poison.decode!(keys: :atoms)
    res.members
  end

  def message(%{channel: channel, text: text}) do
    data = %{channel: channel, text: text} |> Poison.encode!
    {:ok, {_,_,body}} = :httpc.request(:post, {'#{@endpoint}chat.postMessage', [{'Authorization','Bearer #{@token}'}],'application/json','#{data}'}, [],[])
    body |> Poison.decode!(keys: :atoms)
  end

  def rename_channel(%{channel: channel, name: name}) do
    data = %{channel: channel, name: name} |> Poison.encode!
    {:ok, {_,_,body}} = :httpc.request(:post, {'#{@endpoint}channels.rename', [{'Authorization','Bearer #{@token}'}],'application/json','#{data}'}, [],[])
    body |> Poison.decode!(keys: :atoms)
  end

  def change_description(%{channel: channel, topic: topic}) do
    data = %{channel: channel, topic: topic} |> Poison.encode!
    {:ok, {_,_,body}} = :httpc.request(:post, {'#{@endpoint}channels.setTopic', [{'Authorization','Bearer #{@token}'}],'application/json','#{data}'}, [],[])
    body |> Poison.decode!(keys: :atoms)
  end

  def get_phone do
    {:ok, {_,_,body}} = :httpc.request(:get,{'#{@endpoint}users.list', [{'Authorization','Bearer #{@token}'}]}, [],[])
    res = body |> Poison.decode!(keys: :atoms)
    test = Enum.filter(res.members, fn member -> member.deleted == false and member.is_bot == false and member.name != "slackbot" end)
    test2 = Enum.map(test, fn member -> {member.real_name, member.profile.phone} end)
    Enum.into(test2, %{})
  end

end
