# KAlert

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `k_alert` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:k_alert, "~> 0.1.0"}]
    end
    ```

  2. Ensure `k_alert` is started before your application:

    ```elixir
    def application do
      [applications: [:k_alert]]
    end
    ```

Le token a été enlevé pour des raisons de confidentialité.
Rendez vous dans le dossier `lib`

Pour lancer les méthodes:
  - Ouvrir un Shell elixir
  - user_list: Kalert.Slack.user_list
  - message: Kalert.Slack.message%{channel: "CBDFPT2FK", text: "Bonjour l'E4"}
  - rename_channel: Kalert.Slack.rename_channel%{channel: "CBDFPT2FK", name: "Test"}
  - change_description: Kalert.Slack.change_description%{channel: "CBDFPT2FK", topic: "Bonsoir"}
  - get_phone: Kalert.Slack.get_phone
