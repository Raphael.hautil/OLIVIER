let svgContainer = d3.select("body").append("svg").attr("width", "100%").attr("height", window.innerHeight - 20);
let player1shape = svgContainer.append("rect").attr("x", window.innerHeight/2).attr("y", 0).attr("width", "20%").attr("height", "30px");
let player2shape = svgContainer.append("rect").attr("x", window.innerHeight/2).attr("y", window.innerHeight - 50).attr("width", "20%").attr("height", "30px");
let dragger = d3.drag()
    .on("drag", function () {
        d3.select(this).attr("x", d3.event.x - window.innerWidth/10);
    });
player1shape.call(dragger);
player2shape.call(dragger);
let Ball = class {
    constructor() {
        this.x = window.innerHeight/2;
        this.y = window.innerHeight/2;
        this.r = 15;
        this.shape = svgContainer.append("circle").attr("cx", this.x).attr("cy", this.y).attr("r", this.r).attr("fill", "red");
        this.vector = {
            x: d3.randomUniform(-0.9, 0.9)(),
            y: d3.randomUniform(-0.9, 0.9)()
        };
        this.speed = 10;
    }

    remove() {
        this.shape.remove();
    }
};

let ball = new Ball();

d3.timer(function(elapsed) {
    const ballX = ball.x;
    const ballY = ball.y;
    const nextXMove = ballX + ball.speed * ball.vector.x;
    const nextYMove = ballY + ball.speed * ball.vector.y;
    if (nextXMove < 0 || nextXMove > window.innerWidth - 20) {
        ball.vector.x = -ball.vector.x;
    } else if (Number(player1shape.attr("x")) <= nextXMove
        && Number(player1shape.attr("x")) + window.innerWidth/5 >= nextXMove
        && nextYMove < Number(player1shape.attr("y")) + 30) {
        ball.vector.y = -ball.vector.y;
    } else if (Number(player2shape.attr("x")) <= nextXMove
        && Number(player2shape.attr("x")) + window.innerWidth/5 >= nextXMove
        && nextYMove > Number(player2shape.attr("y"))- 30) {
        ball.vector.y = -ball.vector.y;
    } else if (ballY > window.innerHeight || ballY < 0) {
        ball.remove();
        ball = new Ball();
        return ;
    }
    ball.x = ballX + ball.speed * ball.vector.x;
    ball.y = ballY + ball.speed * ball.vector.y;
    ball.shape.attr("cx", ball.x)
        .attr("cy", ball.y);
}, 10);
