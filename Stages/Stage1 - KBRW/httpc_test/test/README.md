# Test

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `test` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:test, "~> 0.1.0"}]
    end
    ```

  2. Ensure `test` is started before your application:

    ```elixir
    def application do
      [applications: [:test]]
    end
    ```
## Ouvrir un shell iex

```elixir
   iex -S mix
```

## Exécuter ces commandes

```elixir
:inets.start
:ssh.start
{:ok, {_,_,body}} = :httpc.request(:get, {'https://google.fr', []}, [], [])
body |> IO.chardata_to_string
```
