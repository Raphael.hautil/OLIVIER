// Create the renderer
var render = new dagreD3.render();

// Add our custom shape (a house)
render.shapes().api = function(parent, bbox, node) {
  var shapeSvg = parent.insert("svg:image", ":first-child")
        .attr("rx", node.rx)
        .attr("ry", node.ry)
        .attr("x", -bbox.width / 2)
        .attr("y", -bbox.height / 2)
        .attr("width", bbox.width)
        .attr("xlink:href", "src/img/browser.png")
        .attr("height", bbox.height);

  node.intersect = function(point) {
      return dagreD3.intersect.rect(node, point);
  };

  return shapeSvg;
};

// Add our custom arrow (a hollow-point)
render.arrows().hollowPoint = function normal(parent, id, edge, type) {
  var marker = parent.append("marker")
    .attr("id", id)
    .attr("viewBox", "0 0 10 10")
    .attr("refX", 9)
    .attr("refY", 5)
    .attr("markerUnits", "strokeWidth")
    .attr("markerWidth", 8)
    .attr("markerHeight", 6)
    .attr("orient", "auto");

  var path = marker.append("path")
    .attr("d", "M 0 0 L 10 5 L 0 10 z")
    .style("stroke-width", 1)
    .style("stroke-dasharray", "1,0")
    .style("fill", "#fff")
    .style("stroke", "#333");
  dagreD3.util.applyStyle(path, edge[type + "Style"]);
};

// Create the renderer
var render = new dagreD3.render();

// Add our custom shape (a house)
render.shapes().man = function(parent, bbox, node) {
  var shapeSvg = parent.insert("svg:image", ":first-child")
        .attr("rx", node.rx)
        .attr("ry", node.ry)
        .attr("x", -bbox.width / 2)
        .attr("y", -bbox.height / 2)
        .attr("width", bbox.width)
        .attr("xlink:href", "src/img/man.png")
        .attr("height", bbox.height);

  node.intersect = function(point) {
      return dagreD3.intersect.rect(node, point);
  };

  return shapeSvg;
};

// Add our custom arrow (a hollow-point)
render.arrows().hollowPoint = function normal(parent, id, edge, type) {
  var marker = parent.append("marker")
    .attr("id", id)
    .attr("viewBox", "0 0 10 10")
    .attr("refX", 9)
    .attr("refY", 5)
    .attr("markerUnits", "strokeWidth")
    .attr("markerWidth", 8)
    .attr("markerHeight", 6)
    .attr("orient", "auto");

  var path = marker.append("path")
    .attr("d", "M 0 0 L 10 5 L 0 10 z")
    .style("stroke-width", 1)
    .style("stroke-dasharray", "1,0")
    .style("fill", "#fff")
    .style("stroke", "#333");
  dagreD3.util.applyStyle(path, edge[type + "Style"]);
};